$(function () {
	$(".showLab").click(function(){
		$(".showLab").hide(500);
		$(".labs").addClass('visible');
	});
	var fixedVideo = {
		$wrapper: $('.video-wrapper'),
		seconds: 500,
		init: function () {
			this.bindEvents();
		},
		bindEvents: function () {
			var self = this;
			// bind scroll event
			$(window).scroll( function() {
				var $video = self.$wrapper.find('.flex-video')
				if (self.isElemUserVisible(self.$wrapper)) { // is visible
					$video.removeClass('fixed-lab');
					self.$wrapper.find('.titulo').css("margin-top", 0);
				} else { // is hidden
					if (!$video.hasClass('fixed-lab')) {
						var h = self.$wrapper.height();
						self.$wrapper.find('.titulo').css("margin-top", h + 'px');
						$video.hide().addClass('fixed-lab').fadeIn(self.seconds);
					}
				}
			});
		},
		isElemUserVisible: function ($elem) {
			return $(window).scrollTop() < $elem.offset().top + $elem.height()- 150;
		}
	}
	fixedVideo.init();
});

//gracias Federico Viarnes