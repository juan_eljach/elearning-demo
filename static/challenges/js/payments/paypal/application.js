$("#id_departamento").change(function(){
    $.get('/tienda/ajax/seleccionar-ciudad',
        {departamento_id: $(this).val()},
        function(data){
            $("#id_ciudad").empty();
            $("#id_ciudad").append("<option value=''>-------</option>");
            $.each(data, function(index, value){
                $("#id_ciudad").append("<option value='" + value['id'] + "'>" + value['nombre'] + "</option>");
            });
        }
    );
});
