$( document ).ready(function() {
    $('.b-nextF').hide();
    var timer = $("#CountDownTimer").TimeCircles({ time: { Days: { color:"#547DBF" }, Hours: { color:"#547DBF"}, Minutes: {color:"#547DBF"}, Seconds: {color:"#547DBF"} },circle_bg_color: "#FAFAFA", count_past_zero: false});
    timer.addListener(function(unit, second, time) { if(time == 0){
        $(".lose_time").fadeIn(500);
    } });

    var updateTime = function(){
        var date = $("#date").val();
        var time = $("#time").val();
        var datetime = date + ' ' + time + ':00';
        $("#DateCountdown").data('date', datetime).TimeCircles().start();
    }
	$('.option').click(function(){
			$(document).find(".check").removeClass('check');
			$( this ).addClass("check");
	});

  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }

  var checkId = 1;
  var level_title = '';
  var level_descripcion = '';
  var question_content = '';
  var question_id = 0;
  var lvl_id = 0;
  var list_answer = new Array();
  var list_count_question = new Array();
  $('.b-nextF').on('click', NewLevel);
      function NewLevel(){
        checkId = 1;
        $('.b-next').show();
        $('.b-nextF').hide();
        $('#level_title').text(level_title);
        $('#level_descripcion').text(level_descripcion);
        $('#question_content').text(question_content);
        $('#question').attr("value", question_id);
        $('#lvl').attr("value", lvl_id);
        $('.count_questions').html('');
        $.each(list_count_question, function(index, value){
            if (index == 0){
                $('.count_questions').append("<li class='circle-nav active' id='circle-" + (index + 1) + "' data-circle='" + (index + 1) + "'></li>");
            }else{
                $('.count_questions').append("<li class='circle-nav' id='circle-" + (index + 1) + "' data-circle='" + (index + 1) + "'></li>");
            }
        });
        $.each(list_answer, function(index, value){
          if((index+1) == 1){
            $('#circle-'+(index+1)).attr("class", "circle-nav active");
          }else{
            $('#circle-'+(index+1)).attr("class", "circle-nav");
          }
          $("#answer-"+(index+1)).attr("class", "option");
          $("#answer-"+(index+1)).attr("data-value", value.id);
          $("#answer-"+(index+1)).text(value.content);
        });
      }
  $('.b-next').on('click', NextQuestion);
      function NextQuestion(){
        var count_circle = checkId;
        $(document).find("[data-circle='" + checkId + "']").removeClass("active");
        var active = $(document).find("[data-nav='" + checkId + "']");
        checkId += 1;
        $(document).find("[data-circle='" + checkId + "']").addClass("active");
      var answer = $('.answer-user').find('.check').attr('data-value');
      var lvl = $('#lvl').val();
      var question = $('#question').val();
      var csrftoken = getCookie('csrftoken');
      $.ajax({
          url : '',
          type: "POST",
          data : {
              csrfmiddlewaretoken: csrftoken,
              level: lvl,
              question: question,
              answer: answer,
          },
          dataType : "json",
          success: function( data ){
            if (data.bloqued == true){
              alert("No hagas trampa, no va a funcionar de nada ;)");
            }else{
                if(data.terminated == true){
                    $('.msg_title').text(data.msg_title);
                    $('.msg_content').text(data.msg_content);
                    if(data.code != false){
                        $('.msg_code').text(data.msg_winner_code);
                        $('.code').text(data.code);
                    }
                    $(".msg_result").fadeIn(500);
                }
              if (data.status == true){
                status = 'good';
              }else{
                status = 'bad';
              }
              $('#circle-'+count_circle).attr("class", "circle-nav "+ status);
            $('.percentage_progress').text(data.progress + "%");
            $('.progress-bar').attr('aria-valuenow', data.progress);
            $('.progress-bar').css( "width", data.progress+"%" );
              if(data.new_level == true){
                $('.b-next').hide();
                $('.b-nextF').show();
                level_title = data.level.title;
                level_descripcion = data.level.description;
                lvl_id = parseInt(data.level.id_level);
                question_content = data.question.content;
                question_id = parseInt(data.question.id_question);
                list_answer = data.answers;
                list_count_question = data.count_questions;
              }else{
                $('#level_title').text(data.level.title);
                $('#level_descripcion').text(data.level.description);
                $('#question_content').text(data.question.content);
                $('#question').attr("value", data.question.id_question);
                $.each(data.answers, function(index, value){
                  $("#answer-"+(index+1)).attr("class", "option");
                  $("#answer-"+(index+1)).attr("data-value", value.id);
                  $("#answer-"+(index+1)).text(value.content);
                });
              }
            }
          }
      });
  }
});
