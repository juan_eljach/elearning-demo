$(function(){
		$("#container").mixItUp();

		$('.carrusel').slick({
 
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

		$('.carrusel2').slick({
 
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
				

		var nav = $("#nav"), no = "nav-open";

			$(".perilla").on("click",function(){

					if(nav.hasClass(no))
					{
						nav.animate({height : 0},300);

						setTimeout(function(){	
							nav.removeClass(no).removeAttr("style");
						},320);

					}
					else
					{
						var newH = nav.css("height","auto").height();

						nav.height(0).animate({height : newH},300);

						setTimeout(function(){
							nav.addClass(no).removeAttr("style");
						},320);

					}
			});

			$("#btn-down").on("click",function(){
			
					$("html,body").animate({
						scrollTop : $(".section1").offset().top - 70
					},800);
				});

      $(".showTem").click(function(){
        /*Variables que me indican donde estoy ubicado*/
        var temarioId = parseInt($(this).attr('data-numTemario'));
        var $section2 = jQuery(document).find(".section2");

        $(document).find(".left").hide('1000');
        $(document).find(".mid").hide('1000');
        $(document).find(".right").hide('1000');

        $(document).find("."+temarioId).show('850');


        var margintopVar = parseInt($(document).find("[data-numTemarioShow='" + temarioId + "']").height());

        if (temarioId == 1 || temarioId == 2 || temarioId ==3) {
          $(".2f").animate({
          marginTop: ( margintopVar - 45) + 'px' 
        }, 600);
          $section2.animate({
          paddingBottom: '40px' 
        }, 600);
        };
        if (temarioId == 4 || temarioId == 5 || temarioId ==6) {
          $(".2f").animate({
          marginTop: '0'
        }, 600);
        };
        if (temarioId == 4 || temarioId == 5 || temarioId ==6) {
          $section2.animate({
          paddingBottom: ( margintopVar - 200) + 'px' 
        }, 100);
        };
      });
      $(".close").click(function(){
        $(document).find(".left").hide('1000');
        $(document).find(".mid").hide('1000');
        $(document).find(".right").hide('1000');
        $(".2f").animate({
          marginTop: '0'
        }, 800);
      });
	});
