var liveSession = function(opts) {
	if(!opts) {
		throw new Error('No se espcificaton las opciones')
	}
	if(!opts.token) {
		throw new Error('No se especificó un token')
	}
	if(!opts.host) {
		console.info('No se especificó un host al cual conectarse, usando el predefinido')
	}

	this.token = opts.token
	this.host = opts.host || ''

}

liveSession.prototype.init = function() {
	var self = this;
	self.io = io.connect(self.host);

	self.io.on('connect', function () {
		console.info('[liveSession] Usuario conectado!')
		self.io.emit('announce', self.token);
	});

	self.io.on('announce_ok', function() {
		console.info('[liveSession] Usuario anunciado!')
	})

	self.io.on('disconnect', function() {
		console.error('[liveSession] Usuario desconectado!')
	})
}