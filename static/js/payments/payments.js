$(document).foundation();
$(document).foundation('joyride', 'start');
$(document).ready(function (){
  $("#form_type_of_payment").hide();
  $("#id_datos_del_comprador").hide();
  $("#id_error_username_paid").hide();
  $("#id_error_email_paid").hide();
  $("#id_error_password").hide();
  });
function value_type_de_payment(value){
  $("#type_of_payment").val(value);
  if ($("#type_of_payment").val() == 'paypal' ){
    $("form").attr("action", "https://www.paypal.com/cgi-bin/webscr");
    $("#credit_card_number").removeAttr("required");
    $("#cvv").removeAttr("required");
    $("#expiry_month").removeAttr("required");
    $("#expiry_year").removeAttr("required");
    Stripe.publishableKey = '';
    $("#id_datos_del_comprador").show();
    $("#user_submit_paid").attr("type", 'button');
  }
  if($("#type_of_payment").val() == 'tarjeta' ){
    Stripe.publishableKey = '{{ publishable }}';
    $("#credit_card_number").attr("required");
    $("#cvv").attr("required");
    $("#expiry_month").attr("required");
    $("#expiry_year").attr("required");
    $("#id_datos_del_comprador").hide();
    $("#user_submit_paid").attr("type", 'submit');
    $("form").attr("action", "");
  }
};
function send_form_paypal(event){
  if ($("#type_of_payment").val() == 'paypal' ){
    $("#user_submit_paid").attr("disabled");
    $("#user_submit_paid").removeAttr("onclick");
    $("#user_submit_paid").attr("value", "Procesando...");
    $.ajax({
        url : '/payments/ajax/form_paypal',
        type: "POST",
        data : {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
          first_name: $('#id_first_name_paid').val(),
          last_name: $('#id_last_name_paid').val(),
          email: $('#id_email_paid').val(),
          username: $('#id_username_paid').val(),
          password1: $('#id_password1_paid').val(),
          password2: $('#id_password2_paid').val(),
          email_of_paypal: $('#id_email_of_paypal').val(),
        },
        dataType : "json",
        success: function( data ){
            if (data['completed']){
              $("#user_submit_paid").attr("type", 'submit');
              $("#user_submit_paid").click();
            }
            if (data['username']){
              $("#id_error_username_paid").text(data['username']);
              $("#id_error_username_paid").show();
            }
            if (data['email']){
              $("#id_error_email_paid").text(data['email']);
              $("#id_error_email_paid").show();
            }
            if (data && ~data['username'] && ~data['email'] && ~data['completed']){
              $("#id_error_password").text('Los dos campos de contraseña no coinciden.');
              $("#id_error_password").show();
            }
        }
    });
  }
}
