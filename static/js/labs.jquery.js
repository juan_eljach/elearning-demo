function responder_comentario(data, image_user, id_comment){
  var $content = $(document).find("[data-restDiv='"+ data +"']");
  var $div = "<form id='commit-sumit' onsubmit='updateAnswer(" + data + ")' ><div style='margin-top:15px;' class='row'><div class='large-10 large-centered columns'><div class='large-9 columns'><label class='input-d' ><input type='text' placeholder='Escribe tu comentario' id='comentario-" + id_comment + "'/><img src='" + image_user + "' ></label></div><div class='large-3 columns'><input type='hidden' id='id_comment-" + id_comment + "' value='" + id_comment + "'><a onclick='updateAnswer(" + data + ")' class='button-blue' >Enviar</a></div></div></div></form>";
  if (!$content.hasClass("disabled")){
    $($content).append($div).fadeIn(1000);
    $($content).addClass('disabled');
  }
};

$(function(){
    var heightMax = $("#h-c-large").height();
    $(window).scroll(function(){
         if ($(window).scrollTop() == $(document).height() - $(window).height()){
             if( $(".div-comentarios").height() <= heightMax ){
                $(".div-comentarios").css( "max-height", "+=100" );
            }
         }
        });
});

$(function(){
    $('form').on("keyup keypress", function(e) {
  var code = e.keyCode || e.which;
  if (code  == 13) {
    e.preventDefault();
    return false;
  }
});
});

$(function () {
	$(".showLab").click(function(){
		$(".showLab").hide(500);
		$(".labs").addClass('visible');
	});
	var fixedVideo = {
		$wrapper: $('.video-wrapper'),
		seconds: 500,
		init: function () {
			this.bindEvents();
		},
		bindEvents: function () {
			var self = this;
			// bind scroll event
			$(window).scroll( function() {
				var $video = self.$wrapper.find('.flex-video')
				if (self.isElemUserVisible(self.$wrapper)) { // is visible
					$video.removeClass('fixed-lab');
					self.$wrapper.find('.titulo').css("margin-top", 0);
				} else { // is hidden
					if (!$video.hasClass('fixed-lab')) {
						var h = self.$wrapper.height();
						self.$wrapper.find('.titulo').css("margin-top", h + 'px');
						$video.hide().addClass('fixed-lab').fadeIn(self.seconds);
					}
				}
			});
		},
		isElemUserVisible: function ($elem) {
			return $(window).scrollTop() < $elem.offset().top + $elem.height()- 150;
		}
	}
	fixedVideo.init();
});
//gracias Federico Viarnes
