from django.contrib.auth.models import User
from django.conf import settings
from django.db import models
from django.utils.timezone import timedelta
from teams.models import TeamsUser
import json
import urllib
import urllib2
from django.utils.timesince import  timesince
from discussions.models import AnswerVideo


def send_event(event_type, event_data):
    to_send = {
        'event': event_type,
        'data': event_data
    }
    urllib2.urlopen(settings.ASYNC_BACKEND_URL, urllib.urlencode(to_send))

class NotificationTeam(models.Model):
    team = models.ForeignKey(TeamsUser)
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now=True)
    updated = models.BooleanField(default=False)
    viewed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username

    def as_dict(self):
        data = {
            'leader': self.team.teams.name,
            'user': self.user.username,
            'date': str(timesince(self.date).encode('UTF-8')),
        }
        return json.dumps(data)

    def save(self, *args, **kwargs):
        is_new = False
        if not self.pk:
            is_new = True
        super(NotificationTeam, self).save(*args, **kwargs)
        if is_new:
            send_event('notification-team-create', self.as_dict())


class NotificationComment(models.Model):
    answer = models.ForeignKey(AnswerVideo)
    date = models.DateTimeField(auto_now=True)
    viewed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username

    def as_dict(self):
        slug = '/cursos/'
        slug += str(self.answer.comment.video.course_class.course.slug)
        slug += '/clases/'
        slug += str(self.answer.comment.video.course_class.slug)
        slug += '/video/'
        slug += str(self.answer.comment.video.slug)
        data = {
            'user_comment': str(self.answer.comment.user_comment.username),
            'user': str(self.answer.user_answer.username),
            'url_comment': str(slug),
            'date': str(timesince(self.date).encode('UTF-8')),
        }
        return json.dumps(data)

    def save(self, *args, **kwargs):
        is_new = False
        if not self.pk:
            is_new = True
        super(NotificationComment, self).save(*args, **kwargs)
        if is_new:
            user_comment = str(self.answer.comment.user_comment.username)
            user = str(self.answer.user_answer.username)
            if user_comment != user:
                send_event('notification-comments', self.as_dict())
