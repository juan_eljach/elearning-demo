from django.db import models
from classes.models import CourseClass
from django.template.defaultfilters import slugify
from materials.models import Material
import threading


class Article(models.Model):
    title = models.CharField(max_length=140)
    content = models.TextField()
    course_class = models.ManyToManyField(CourseClass)
    slug = models.SlugField(max_length=300)

    def delete_of_material(self):
        for course_class in self.course_class.all():
            try:
                material = Material.objects.get(
                    course_class=course_class,
                    id_data_material=self.pk,
                    type_material='article'
                )
                material.delete()
            except:
                pass
        return 0

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Article, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Article, self).save(*args, **kwargs)
        task = threading.Thread(
            target=self.delete_of_material
        )
        task.setDaemon(True)
        task.start()

    def __unicode__(self):
        return self.title
