from django.views.generic import ListView, DetailView
from .models import Article


class ArticlesListView(ListView):
    model = Article
    template_name = "articles/articles.html"
    context_object_name = "articles"


class ArticleDetailView(DetailView):
    model = Article
    template_name = "articles/article_detail.html"
    context_object_name = "article"

    def get_object(self):
        slug = self.kwargs['article_slug']
        obj = self.model.objects.get(slug__iexact=slug)
        return obj

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        context["course_url"] = self.kwargs['course_slug']
        context["class_url"] = self.kwargs['class_slug']
        return context
