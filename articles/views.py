from django.views.generic import ListView, DetailView
from .models import Article
from users.models import UserProgressMaterial
from users.previous_next_classes import url_next_classes
from users.previous_next_classes import url_previous_classes
from users.previous_next_classes import if_last_classes
from users.previous_next_classes import if_first_classes
from django.utils.decorators import method_decorator
from exploiter.decorators import user_can_access
from courses.models import Course
from classes.models import CourseClass
from django.shortcuts import redirect
from additional.can_access_material.access_material import can_access_material
from django.conf import settings
if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
    production = False
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
    production = True


class ArticlesListView(ListView):
    model = Article
    template_name = "articles/articles.html"
    context_object_name = "articles"


class ArticleDetailView(DetailView):
    model = Article
    template_name = "articles/article_detail.html"
    context_object_name = "article"

    def get_object_course_class(self):
        obj_course = Course.objects.get(slug=self.kwargs['course_slug'])
        obj_course_class = CourseClass.objects.get(
            course=obj_course,
            slug=self.kwargs['class_slug']
        )
        return obj_course_class

    def get_object(self):
        slug = self.kwargs['article_slug']
        obj = self.model.objects.get(
            slug__iexact=slug,
            course_class=self.get_object_course_class()
        )
        return obj

    def get_context_data(self, **kwargs):
        try:
            UserProgressMaterial.objects.get(
                user=self.request.user,
                id_data_material=self.get_object().id,
                type_material='articles'
            )
        except:
            UserProgressMaterial.objects.create(
                user=self.request.user,
                id_data_material=self.get_object().id,
                type_material='articles'
            )
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        context['production'] = production
        context["course_url"] = self.kwargs['course_slug']
        context["class_url"] = self.kwargs['class_slug']
        context['url_next_class'] = url_next_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'article',
            self.request.user
        )
        context['url_previous_class'] = url_previous_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'article'
        )
        context['if_first_classes'] = if_first_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'article'
        )
        context['if_last_classes'] = if_last_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'article',
            self.request.user,
        )
        context['course_icon'] = True
        return context

    @method_decorator(user_can_access)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        can_access = can_access_material(
            self.request.user, self.get_object_course_class())
        if can_access:
            return redirect("course", course_slug=kwargs["course_slug"])

        return self.render_to_response(context)
