from django.conf.urls import patterns, url
from .views import ArticleDetailView

urlpatterns = patterns('',
    #url(r'articulos/$', ArticlesListView.as_view(), name='clase'),
    url(r'cursos/(?P<course_slug>[\w\d\-]+)/articulos/(?P<article_slug>[\w\d\-]+)/$', ArticleDetailView.as_view(), name='articulo'),
)
