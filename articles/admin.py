from django.contrib import admin
from materials.models import Material
from .models import Article
from materials.notifications import register_of_notification


class ArticleAdmin(admin.ModelAdmin):
    exclude = ("slug",)
    filter_horizontal = ("course_class",)
    search_fields = ('title', )

    class Media:
        js = ('/static/tiny_mce/tiny_mce.js',
              '/static/tiny_mce/textarea.js',)

    def save_model(self, request, form, formset, change):
        super(ArticleAdmin, self).save_model(request, form, formset, change)
        formset.save_m2m()
        form.save()
        for course_class in form.course_class.all():
            try:
                Material.objects.get(
                    course_class=course_class,
                    id_data_material=form.id,
                    type_material='article'
                )
            except:
                register_material = Material.objects.create(
                    course_class=course_class,
                    id_data_material=form.id,
                    type_material='article'
                )
                register_material.save()
                obj_material = Material.objects.get(id=register_material.id)
                register_of_notification(
                    obj_material,
                    obj_material.course_class,
                    obj_material.id_data_material,
                    obj_material.type_material
                )

admin.site.register(Article, ArticleAdmin)
