from django.contrib import admin
from .models import DiscountUser
from .models import DiscountUserNew
from .models import DiscountPercentage
from .models import DiscountCodeURL


class DiscountUserAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )


class DiscountUserNewAdmin(admin.ModelAdmin):
    search_fields = ('email', )


class DiscountPercentageAdmin(admin.ModelAdmin):
    search_fields = ('name', )

admin.site.register(DiscountUser, DiscountUserAdmin)
admin.site.register(DiscountUserNew, DiscountUserNewAdmin)
admin.site.register(DiscountPercentage, DiscountPercentageAdmin)
admin.site.register(DiscountCodeURL)