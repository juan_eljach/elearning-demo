from django.db import models
from django.contrib.auth.models import User

from friends.models import FriendsInvented


class DiscountPercentage(models.Model):
    choices_apply = (
        ('discount_user', 'discount_user'),
        ('discount_user_new', 'discount_user_new'),
        ('discount_friends_invented', 'discount_friends_invented'),
        ('discount_mail_tracking', 'discount_mail_tracking')
    )
    name = models.CharField(max_length=254)
    apply_discount = models.CharField(
        max_length=254,
        choices=choices_apply,
        unique=True
    )
    percentage = models.IntegerField()

    def __unicode__(self):
        return self.name


class DiscountUser(models.Model):
    code = models.CharField(max_length=254)
    user = models.ForeignKey(User)
    used = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(DiscountUser, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.user.username


class DiscountUserFriends(models.Model):
    friends_invented = models.ForeignKey(FriendsInvented)
    discount_user = models.ForeignKey(DiscountUser)

    def __unicode__(self):
        return 0


class DiscountFriendsInvented(models.Model):
    friends_invented = models.ForeignKey(FriendsInvented)
    is_user = models.BooleanField(default=False)

    def __unicode__(self):
        return self.is_user


class DiscountUserNew(models.Model):
    code = models.CharField(max_length=254)
    email = models.EmailField()
    used = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(DiscountUserNew, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.email


class DiscountCodeURL(models.Model):
    code = models.CharField(max_length=254)
    discount = models.CharField(max_length=254)

    def __unicode__(self):
        return self.code