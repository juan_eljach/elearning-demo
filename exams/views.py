from django.shortcuts import render
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator

class TakeUserExam(TemplateView):
    model = UserExam
    template_name = 'take_exam.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TakeUserExam, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        user_profile = get_object_or_404(UserProfile, user=request.user)

        exam = get_object_or_404(Exam, shash=kwargs['exam_hash'], **exam_params)

        if exam.course and not user_profile.is_organizer():
            if not user_profile.course.filter(pk=exam.course.pk).exists():
                return redirect('join', course_slug=exam.course.slug)

            if 'unsubscribed' in user_profile.user_type or 'pay' in user_profile.user_type:
                time_left = user_profile.get_time_left()
                if time_left <= 0:
                    return redirect('time_expired')

        try:
            user_exam = UserExam.objects.get(approved=True, exam=exam, user=user_profile)
        except UserExam.DoesNotExist:
            try:
                user_exam = UserExam.objects.get(active=True, exam=exam, user=user_profile)
            except UserExam.DoesNotExist:
                user_exam = UserExam.objects.filter(active=False, exam=exam, user=user_profile).order_by('-id')
                if user_exam.count() > 0:
                    user_exam = user_exam[0]

                    if user_exam.is_after_day():
                        return self.generate(request, *args, **kwargs)
                else:
                    return self.generate(request, *args, **kwargs)
            else:
                if not user_exam.is_in_time():
                    finish = user_exam.finish()
                    return redirect(finish['redirect'])

                else:
                    return self.show_exam(user_exam=user_exam)

        return redirect('exam_result', exam_hash=exam.shash)

    def generate(self, request, *args, **kwargs):
        exam = Exam.objects.get(shash=kwargs['exam_hash'])
        user_profile = UserProfile.objects.get(user=request.user, organization__slug=request.subdomain)

        questions_all = exam.question_set.all()
        question_basics = questions_all.filter(question_type='ba')
        question_intermediates = questions_all.filter(question_type='in')
        question_advances = questions_all.filter(question_type='ad')

        basics_percent = round((question_basics.count() * 100)/questions_all.count())
        intermediates_percent = round((question_intermediates.count() * 100)/questions_all.count())

        basics = sample(question_basics, int(basics_percent * exam.questions_number / 100))
        intermediates = sample(question_intermediates, int(intermediates_percent * exam.questions_number / 100))
        advances = sample(question_advances, exam.questions_number - len(basics) - len(intermediates))

        questions = map(lambda x: x.id, (basics + intermediates + advances))

        user_exam = UserExam.objects.create(
            questions=','.join(str(q) for q in questions),
            exam=exam,
            user=user_profile,
            question_active_id=questions[0],
            user_type=user_profile.user_type)

        return self.show_exam(user_exam=user_exam)

    def show_exam(self, **kwargs):
        user_exam = kwargs['user_exam']

        question = Question.objects.get(id=user_exam.question_active.id)
        answers = question.answer_set.all()

        context = self.get_context_data(**kwargs)
        context['question'] = question
        context['answers'] = answers
        context['userexam_id'] = user_exam.id
        context['exam_hash'] = user_exam.exam.shash
        context['time_left'] = user_exam.time_left()
        context['questions_number'] = user_exam.exam.questions_number
        context['user_exam'] = user_exam.exam.course.slug
        context['current'] = user_exam.answers.count() + 1
        return self.render_to_response(context)
