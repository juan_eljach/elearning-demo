from django.db import models
from django.contrib.auth.models import User
from courses.models import Course


class Question(models.Model):
    question = models.TextField(max_length=500)

    def __unicode__(self):
            return self.question[:40]

    def get_correct_answer(self):
        return self.answers.filter(correct=True).first()


class Answer(models.Model):
    answer = models.TextField(max_length=500)
    correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question)

    def __unicode__(self):
        return self.answer[:40]

    def get_question(self):
        return self.question_set.all()[0]

    def get_course(self):
        question = self.get_question()
        return question.exam_set.all()[0]


class Exam(models.Model):
    course = models.ForeignKey(Course)
    questions = models.ManyToManyField(Question)

    def __unicode__(self):
            return self.course.name

    def get_questions_count(self):
        return self.questions.count()


class UserExam(models.Model):
    approved = models.BooleanField(default=False)
    questions = models.ManyToManyField(Question)
    answers = models.ManyToManyField(Answer)
    user = models.ForeignKey(User)
    exam = models.ForeignKey(Exam)
