from django.contrib import admin
from .models import Exam, Question, Answer


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 4


class ExamAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "get_questions_count")


class QuestionAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "get_correct_answer")
    inlines = [AnswerInline]


class AnswerAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "get_question", "get_course", "correct")

admin.site.register(Exam, ExamAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
