# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from .views import DisconnectUserContainer

urlpatterns = patterns('',
    url(r'^user/disconnect/(?P<username>\w+)/$',
        DisconnectUserContainer.as_view(), name='disconnect_user'),
)