from django.contrib import admin
from .models import ContainerImage
from .models import ContainerUser
from .models import ApiExploiterInstance
from .models import ApiExploiterUser
from .models import ApiExploiterPrivateBetaUser


class ContainerImageAdmin(admin.ModelAdmin):
    search_fields = ('course__name', )


class ContainerUserAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )


class ApiExploiterInstanceAdmin(admin.ModelAdmin):
    search_fields = ('course__name', )


class ApiExploiterUserAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )


class ApiExploiterPrivateBetaUserAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )

admin.site.register(ContainerImage, ContainerImageAdmin)
admin.site.register(ContainerUser, ContainerUserAdmin)
admin.site.register(ApiExploiterInstance, ApiExploiterInstanceAdmin)
admin.site.register(ApiExploiterUser, ApiExploiterUserAdmin)
admin.site.register(ApiExploiterPrivateBetaUser,
    ApiExploiterPrivateBetaUserAdmin)
