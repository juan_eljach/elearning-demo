from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from labs_containers.models import ApiExploiterUser
from django.views.generic import TemplateView
from .models import ContainerImage
from .models import ContainerUser
import requests
import httplib
import base64
import urllib
import requests.auth


CONSUMER_KEY = '18b3ccb5f5b544ebd4265f7adca1c5cae44ef3fc661da4524f2a260699f52c25'
CONSUMER_SECRET = 'cf2b9c82831056244ac126b88905531427990321972a4067063f6b4bb51263ba'
host = 'codepicnic.com'


def generate_token():
    encoded_CONSUMER_KEY = urllib.quote(CONSUMER_KEY)
    encoded_CONSUMER_SECRET = urllib.quote(CONSUMER_SECRET)
    concat_consumer_url = encoded_CONSUMER_KEY + ":" + encoded_CONSUMER_SECRET
    url_token = '/oauth/token/'
    params = urllib.urlencode({'grant_type': 'client_credentials'})
    request = httplib.HTTPSConnection(host)
    request.putrequest("POST", url_token)
    request.putheader("Host", host)
    request.putheader("User-Agent: Exploiter.co")
    request.putheader("Authorization", "Basic %s" % base64.b64encode(concat_consumer_url))
    request.putheader("Content-Type" ,"application/x-www-form-urlencoded;charset=UTF-8")
    request.putheader("Content-Length", "29")
    request.putheader("Accept-Encoding", "gzip")
    request.endheaders()
    request.send(params)
    response = request.getresponse()
    token = eval(response.read())['access_token']
    return token


def create_container(user, obj_course):
    obj_container_image = ContainerImage.objects.get(course=obj_course)
    url_console = '/api/consoles/'
    headers = {"Authorization": "bearer " + generate_token()}
    data = 'bite[custom_image]=%s&bite[container_size]=%s&bite[container_type]=%s&bite[hostname]=%s'%(
        obj_container_image.custom_image,
        obj_container_image.size,
        obj_container_image.container_type,
        obj_container_image.host_name
    )
    response_request = requests.post(
        'https://'+host+url_console,
        headers=headers,
        data=data
    )
    response = response_request.json()
    create_container_user = ContainerUser.objects.create(
        user=user,
        image=obj_container_image,
        name=response['container_name']
    )
    create_container_user.save()
    return (response['url'] + '?hide=options')


def start_container(user, obj_course):
    obj_container_image = ContainerImage.objects.get(course=obj_course)
    obj_container_user = ContainerUser.objects.filter(
        user=user,
        image=obj_container_image
    ).last()
    url_console = 'https://codepicnic.com/api/consoles/%s/start' % (
        obj_container_user.name
    )
    headers = {"Authorization": "bearer " + generate_token()}
    response_request = requests.post(
        url_console,
        headers=headers
    )
    response = response_request.json()
    return (response['url'] + '?hide=options')


def stop_container(user, obj_course):
    obj_container_image = ContainerImage.objects.get(course=obj_course)
    obj_container_user = ContainerUser.objects.filter(
        user=user,
        image=obj_container_image
    ).last()
    url_console = 'https://codepicnic.com/api/consoles/%s/stop' % (
        obj_container_user.name
    )
    headers = {"Authorization": "bearer " + generate_token()}
    response_request = requests.post(
        url_console,
        headers=headers
    )
    response = response_request.json()
    return (response['url'] + '?hide=options')


def restart_container(user, obj_course):
    obj_container_image = ContainerImage.objects.get(course=obj_course)
    obj_container_user = ContainerUser.objects.last(
        user=user,
        image=obj_container_image
    ).last()
    url_console = 'https://codepicnic.com/api/consoles/%s/restart' % (
        obj_container_user.name
    )
    headers = {"Authorization": "bearer " + generate_token()}
    response_request = requests.post(
        url_console,
        headers=headers
    )
    response = response_request.json()
    return (response['url'] + '?hide=options')


class DisconnectUserContainer(TemplateView):
    template_name = 'base.html'

    def get_object(self):
        obj = get_object_or_404(User, username=self.kwargs['username'])
        return obj

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        obj_api_user = get_object_or_404(ApiExploiterUser, user=self.object,
            status=True)
        obj_api_user.status = False
        obj_api_user.save()
        try:
            host = "labs.api.exploiter.co"
            url_create = "/v1/aws/terminateInstance/{0}".format(
                obj_api_user.instance_id)
            response_request = requests.get(
                'http://' + host + url_create,
            )
            response_request.json()
        except:
            pass
        return HttpResponse(status=200)