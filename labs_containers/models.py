from django.db import models
from django.contrib.auth.models import User
from courses.models import Course

container_size = (
    ("medium", "256MB"),
    ("large", "512MB"),
    ("xlarge", "1GB"),
    ("xxlarge", "2GB")
)

container_type = (
    ("python", "Python"),
    ("ruby", "Ruby"),
    ("bash", "Bash"),
    ("php", "Php"),
    ("nodejs", "NodeJS"),
    ("js", "JS"),
    ("rails", "Rails")
)


class ContainerImage(models.Model):
    custom_image = models.CharField(max_length=254, blank=True, null=True)
    host_name = models.CharField(max_length=254)
    size = models.CharField(max_length=7, choices=container_size)
    container_type = models.CharField(max_length=6, choices=container_type)
    course = models.ForeignKey(Course)

    def __unicode__(self):
        return self.host_name


class ContainerUser(models.Model):
    user = models.ForeignKey(User)
    image = models.ForeignKey(ContainerImage)
    name = models.CharField(max_length=254)

    def __unicode__(self):
        return self.user.username


class ApiExploiterInstance(models.Model):
    course = models.ForeignKey(Course)
    flavor = models.CharField(max_length=80)
    ami = models.CharField(max_length=80)
    sg = models.CharField(max_length=80)

    def __unicode__(self):
        return self.flavor


class ApiExploiterUser(models.Model):
    api_instance = models.ForeignKey(ApiExploiterInstance)
    user = models.ForeignKey(User)
    ip_publica = models.IPAddressField()
    instance_id = models.CharField(max_length=50)
    status = models.BooleanField(default=True)

    def __unicode__(self):
        return self.user.username


class ApiExploiterPrivateBetaUser(models.Model):
    user = models.ForeignKey(User)
    status = models.BooleanField(default=True)

    def __unicode__(self):
        return self.user.username
