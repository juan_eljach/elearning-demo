from django.contrib import admin
from .models import LoggingError

admin.site.register(LoggingError)
