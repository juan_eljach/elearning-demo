from django.contrib.auth.models import User
from django.utils import timezone
from django.db import models


class LoggingError(models.Model):
    choices_name = (
        ('videos', 'videos'),
        ('articles', 'articles'),
        ('container', 'container'),
        ('courses', 'courses'),
        ('classes', 'classes'),
        ('paypal', 'paypal'),
        ('quiz', 'quiz'),
        ('email', 'email'),
    )

    user = models.ForeignKey(User)
    name = models.CharField(max_length=140, choices=choices_name)
    error = models.CharField(max_length=254)
    date = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.name
