from django.conf.urls import patterns, include, url
from .views import HomeView, LiveView, TermsOfUseView, PrivacyPolicyView, EmpezarRedirectView, CourseLandingView, PricingView, NosotrosView, Error404View, PurchaseView, PurchaseComplete
from .views import RankingOfPoints, Welcome, Congratulations
from django.views.decorators.cache import cache_page

urlpatterns = patterns('',
    url(r'^$', cache_page(60)(HomeView.as_view()), name='home'),
    url(r'^live/$', LiveView.as_view(), name='live'),
    url(r'^nosotros/$', NosotrosView.as_view(), name='nosotros'),
    url(r'^404/$', Error404View.as_view(), name='error404'),
    url(r'^terminos/$', TermsOfUseView.as_view(), name='terms'),
    url(r'^welcome/$', Welcome.as_view(), name='welcome'),
    url(r'^politicas/$', PrivacyPolicyView.as_view(), name='privacy'),
    url(r'^pricing/$', PricingView.as_view(), name='pricing'),
    url(r'^suscribirme/$', PurchaseView.as_view(), name='suscribirme'),
    url(r'^suscribirme/(?P<code>\w+)$', PurchaseView.as_view(),
        name='suscribirme_code'),
    url(r'^ranking/$', RankingOfPoints.as_view(), name='ranking'),
    url(r'^empezar/$', EmpezarRedirectView.as_view(), name='empezar'),
    url(r'^congratulations/$', Congratulations.as_view(),
        name='congratulations'),
    url(r'^curso/(?P<course_landing_slug>[\w\d\-]+)/$',
        CourseLandingView.as_view(), name='course_landing'),
)
