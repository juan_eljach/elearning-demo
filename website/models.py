from django.db import models
from django.template.defaultfilters import slugify

class Video(models.Model):
    title = models.CharField(max_length=140)
    description = models.TextField(max_length=500)
    thumb = models.ImageField(upload_to="uploads")
    slug = models.SlugField(max_length=300)
    video = models.URLField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Video, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title
