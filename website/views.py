#-*-coding:UTF-8 -*-
from django.views.generic import DetailView, TemplateView, View, RedirectView
#from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect
#from django.views.decorators.csrf import csrf_protect
#from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.gzip import gzip_page
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.utils.translation import ugettext as _
#from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
#from django.shortcuts import redirect
#from django.http import HttpResponse
#from django.utils import simplejson
#from django.shortcuts import render
from django.utils import timezone
from django.db import connection
from django.conf import settings

from userena.models import UserenaSignup
from userena.forms import SignupForm

#from urllib2 import urlopen, Request
from datetime import timedelta
#from mixpanel import Mixpanel
#from urllib import urlencode
from discounts.models import DiscountCodeURL
import stripe
#import json
#import uuid

from payments.forms import CreditCardForm
from payments.models import Payment#, PaymentOfPaypal
from discounts.models import DiscountUserNew
from discounts.models import DiscountUser
from accounts.models import UserProfile
from courses.models import Course
#from additional.create_payments.create_payments_stripe import payments_stripe

if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
    production = False
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
    production = True

mp = settings.MIXPANEL


class HomeView(TemplateView):
    template_name = "website/index.html"

    @method_decorator(gzip_page)
    def dispatch(self, *args, **kwargs):
        return super(HomeView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['production'] = production
        return context


class TermsOfUseView(TemplateView):
    template_name = "website/terms.html"


class PrivacyPolicyView(TemplateView):
    template_name = "website/privacy.html"


class LiveView(TemplateView):
    template_name = "website/live.html"


class Congratulations(TemplateView):
    template_name = "website/congratulations.html"

    def get(self, request, *args, **kwargs):
        return super(Congratulations, self).get(request, *args, **kwargs)


class NosotrosView(TemplateView):
    template_name = "website/nosotros.html"


class Error404View(TemplateView):
    template_name = "404.html"


class EmpezarRedirectView(RedirectView):
    url = "/signup"


class CourseLandingView(DetailView):
    model = Course
    template_name = "website/base_landing_courses.html"
    context_object_name = "course_landing"

    def get_object(self, **kwargs):
        slug = self.kwargs["course_landing_slug"]
        obj = get_object_or_404(Course, slug__iexact=slug)
        return obj

    def get_context_data(self, **kwargs):
        context = super(CourseLandingView, self).get_context_data(**kwargs)
        course = self.get_object()
        course_classes = course.courseclass_set.all().order_by("id")
        context["classes"] = course_classes
        context['production'] = production
        return context


class PurchaseView(TemplateView):
    form_class = CreditCardForm
    form_user = SignupForm
    template_name = "website/purchase.html"
    initial = {'key': 'value'}
    content_type = None

    def get(self, request, *args, **kwargs):
        try:
            code = self.kwargs['code']
            code = DiscountCodeURL.objects.get(code=code)
            if code:
                code_url = code.discount
            else:
                code_url = ""
        except:
            code_url = ""
        if request.user.is_authenticated():
            try:
                identify = request.user.id
                mp.track(identify, "Pricing viewed")
            except:
                pass
            try:
                Payment.objects.get(user=request.user)
                Payment.objects.get(user=request.user, status='Completed')
                return HttpResponseRedirect("/accounts/%s" % request.user)
            except:
                form = self.form_class(initial=self.initial)
                return render(request, self.template_name, {
                    "form": form,
                    "code": code_url,
                    "years": range(2015, 2036),
                    'months': range(1, 13),
                    "publishable": STRIPE_PUBLISHABLE
                })
        else:
            form = self.form_class(initial=self.initial)
            form_user = self.form_user(initial=self.initial)
            return render(request, self.template_name, {
                "form": form,
                "form_user": form_user,
                "code": code_url,
                "years": range(2015, 2036),
                'months': range(1, 13),
                "publishable": STRIPE_PUBLISHABLE
            })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST or None)
        form_user = self.form_user(request.POST or None)
        if form.is_valid():
            if request.user.is_authenticated():
                is_new_user = False
                if not request.user.email:
                    user_obj = User.objects.get(id=request.user.id)
                    user_obj.email = request.POST["email"]
                    user_obj.save()
                    email = request.POST["email"]
                else:
                    email = request.user.email
                user_obj = request.user
            else:
                is_new_user = True
                if form_user.is_valid():
                    new_user = form_user.save()
                    email = form_user.cleaned_data["email"]
                    user_obj = User.objects.get(
                        username=form_user.cleaned_data["username"]
                    )
                    user_obj.first_name = request.POST["first_name"]
                    user_obj.last_name = request.POST["last_name"]
                    user_obj.is_active = True
                    user_obj.save()
                    user_activate = UserenaSignup.objects.get(
                        user_id=user_obj.id
                    )
                    user_activate.activation_key = 'ALREADY_ACTIVATED'
                    user_activate.save()
                    new_user = authenticate(
                        username=user_obj.username,
                        password=form_user.cleaned_data["password1"]
                    )
                    login(request, new_user)
                    identify = user_obj.id
                    mp.people_set(identify, {
                        '$first_name': user_obj.first_name,
                        '$last_name': user_obj.last_name,
                        '$email': user_obj.email,
                        'type': 'free',
                    })
                    mp.track(identify, "Account activated")
                else:
                    return render(
                        request,
                        self.template_name,
                        {
                            'form_user': form_user,
                            "years": range(2015, 2036),
                            'months': range(1, 13)
                        }
                    )
            if request.POST["type_of_payment"] == 'tarjeta':
                try:
                    cupon_of_discount = request.POST['cupon']
                    if cupon_of_discount:
                        if is_new_user:
                            try:
                                obj_discount = DiscountUserNew.objects.get(
                                    code=cupon_of_discount,
                                    email=user_obj.email,
                                    used=False
                                )
                            except:
                                obj_discount = False
                        else:
                            try:
                                obj_discount = DiscountUser.objects.get(
                                    code=cupon_of_discount,
                                    user=user_obj,
                                    used=False
                                )
                            except:
                                obj_discount = False
                    else:
                        obj_discount = False
                    if obj_discount:
                        coupon = stripe.Coupon.retrieve(obj_discount.code)
                        try:
                            customer = stripe.Customer.create(
                                email=email,
                                card=form.cleaned_data["stripe_token"],
                                coupon=coupon.id,
                            )
                            obj_discount.used = True
                            obj_discount.save()
                        except Exception:
                            return render(
                                request,
                                self.template_name,
                                {
                                    'form': form,
                                    "error": "El Cupón Expiró",
                                    "years": range(2015, 2036),
                                    'months': range(1, 13)
                                }
                            )
                    else:
                        customer = stripe.Customer.create(
                            email=email,
                            card=form.cleaned_data["stripe_token"]
                        )
                    customer.save()
                    customer_with_suscription = stripe.Customer.retrieve(
                        customer.id
                    )
                    customer_with_suscription.subscriptions.create(
                        plan='month-suscription'
                    )
                    customer_with_suscription.save()
                    user_profile = UserProfile.objects.get(user=user_obj)
                    user_profile.paid_user = True
                    user_profile.type_of_payment = "credit_card"
                    user_profile.save()
                    payment_form = Payment.objects.create(
                        user=user_obj,
                        date_of_subscription=timezone.datetime.today().date(),
                        date_of_last_payment=timezone.datetime.today().date(),
                        date_end_of_subscription=timezone.datetime.today().date() + timedelta(days=30),
                        sub_id=customer.id,
                        status='Completed',
                        token=form.cleaned_data["stripe_token"],
                        receiver_id=customer.id
                        )
                    payment_form.save()
                    #MIXPANEL TRACKING
                    try:
                        identify = user_obj.id
                        mp.people_set(identify, {'type': 'paid'})
                        mp.people_track_charge(identify, '25.00', {
                            '$time': payment_form.date_of_subscription})
                        mp.track(identify, "Successful Stripe payment")
                    except:
                        pass
                    #END OF MIXPANEL TRACKING
                    return HttpResponseRedirect("/congratulations/")
                except stripe.error.CardError, card_error:
                    return render(
                        request,
                        self.template_name,
                        {
                            'form': form,
                            "error": _(card_error),
                            "years": range(2015, 2036),
                            'months': range(1, 13)
                        }
                    )
        else:
            return render(
                request,
                self.template_name,
                {
                    'form': form,
                    "years": range(2015, 2036),
                    'months': range(1, 13)
                }
            )
        return render(
            request,
            self.template_name,
            {
                "form": form,
                "years": range(2015, 2036),
                'months': range(1, 13)
            }
        )


class PricingView(View):
    form_class = CreditCardForm
    form_user = SignupForm
    template_name = "website/pricing.html"
    initial = {'key': 'value'}
    content_type = None

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            try:
                identify = request.user.id
                mp.track(identify, "Pricing viewed")
            except:
                pass
            try:
                Payment.objects.get(user=request.user)
                Payment.objects.get(user=request.user, status='Completed')
                return HttpResponseRedirect("/accounts/%s" % request.user)
            except:
                form = self.form_class(initial=self.initial)
                return render(request, self.template_name, {
                    "form": form,
                    "years": range(2015, 2036),
                    'months': range(1, 13),
                    "publishable": STRIPE_PUBLISHABLE
                })
        else:
            form = self.form_class(initial=self.initial)
            form_user = self.form_user(initial=self.initial)
            return render(request, self.template_name, {
                "form": form,
                "form_user": form_user,
                "years": range(2015, 2036),
                'months': range(1, 13),
                "publishable": STRIPE_PUBLISHABLE
            })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST or None)
        form_user = self.form_user(request.POST or None)
        if form.is_valid():
            if request.user.is_authenticated():
                is_new_user = False
                if not request.user.email:
                    user_obj = User.objects.get(id=request.user.id)
                    user_obj.email = request.POST["email"]
                    user_obj.save()
                    email = request.POST["email"]
                else:
                    email = request.user.email
                user_obj = request.user
            else:
                is_new_user = True
                if form_user.is_valid():
                    new_user = form_user.save()
                    email = form_user.cleaned_data["email"]
                    user_obj = User.objects.get(
                        username=form_user.cleaned_data["username"]
                    )
                    user_obj.first_name = request.POST["first_name"]
                    user_obj.last_name = request.POST["last_name"]
                    user_obj.is_active = True
                    user_obj.save()
                    user_activate = UserenaSignup.objects.get(
                        user_id=user_obj.id
                    )
                    user_activate.activation_key = 'ALREADY_ACTIVATED'
                    user_activate.save()
                    new_user = authenticate(
                        username=user_obj.username,
                        password=form_user.cleaned_data["password1"]
                    )
                    login(request, new_user)
                    identify = user_obj.id
                    mp.people_set(identify, {
                        '$first_name': user_obj.first_name,
                        '$last_name': user_obj.last_name,
                        '$email': user_obj.email,
                        'type': 'free',
                    })
                    mp.track(identify, "Account activated")
                else:
                    return render(
                        request,
                        self.template_name,
                        {
                            'form_user': form_user,
                            "years": range(2015, 2036),
                            'months': range(1, 13)
                        }
                    )
            if request.POST["type_of_payment"] == 'tarjeta':
                try:
                    cupon_of_discount = request.POST['cupon']
                    if cupon_of_discount:
                        if is_new_user:
                            try:
                                obj_discount = DiscountUserNew.objects.get(
                                    code=cupon_of_discount,
                                    email=user_obj.email,
                                    used=False
                                )
                            except:
                                obj_discount = False
                        else:
                            try:
                                obj_discount = DiscountUser.objects.get(
                                    code=cupon_of_discount,
                                    user=user_obj,
                                    used=False
                                )
                            except:
                                obj_discount = False
                    else:
                        obj_discount = False
                    if obj_discount:
                        coupon = stripe.Coupon.retrieve(obj_discount.code)
                        try:
                            customer = stripe.Customer.create(
                                email=email,
                                card=form.cleaned_data["stripe_token"],
                                coupon=coupon.id,
                            )
                            obj_discount.used = True
                            obj_discount.save()
                        except Exception:
                            return render(
                                request,
                                self.template_name,
                                {
                                    'form': form,
                                    "error": "El Cupón Expiró",
                                    "years": range(2015, 2036),
                                    'months': range(1, 13)
                                }
                            )
                    else:
                        customer = stripe.Customer.create(
                            email=email,
                            card=form.cleaned_data["stripe_token"]
                        )
                    customer.save()
                    customer_with_suscription = stripe.Customer.retrieve(
                        customer.id
                    )
                    customer_with_suscription.subscriptions.create(
                        plan='month-suscription'
                    )
                    customer_with_suscription.save()
                    user_profile = UserProfile.objects.get(user=user_obj)
                    user_profile.paid_user = True
                    user_profile.type_of_payment = "credit_card"
                    user_profile.save()
                    payment_form = Payment.objects.create(
                        user=user_obj,
                        date_of_subscription=timezone.datetime.today().date(),
                        date_of_last_payment=timezone.datetime.today().date(),
                        date_end_of_subscription=timezone.datetime.today().date() + timedelta(days=30),
                        sub_id=customer.id,
                        status='Completed',
                        token=form.cleaned_data["stripe_token"],
                        receiver_id=customer.id
                        )
                    payment_form.save()
                    #MIXPANEL TRACKING
                    try:
                        identify = user_obj.id
                        mp.people_set(identify, {'type': 'paid'})
                        mp.people_track_charge(identify, '25.00', {
                            '$time': payment_form.date_of_subscription})
                        mp.track(identify, "Successful Stripe payment")
                    except:
                        pass
                    #END OF MIXPANEL TRACKING
                    return HttpResponseRedirect("/cursos/")
                except stripe.error.CardError, card_error:
                    return render(
                        request,
                        self.template_name,
                        {
                            'form': form,
                            "error": card_error,
                            "years": range(2015, 2036),
                            'months': range(1, 13)
                        }
                    )
        else:
            return render(
                request,
                self.template_name,
                {
                    'form': form,
                    "years": range(2015, 2036),
                    'months': range(1, 13)
                }
            )
        return render(
            request,
            self.template_name,
            {
                "form": form,
                "years": range(2015, 2036),
                'months': range(1, 13)
            }
        )


class PurchaseComplete(View):
    template_name = "payments/purchase_complete.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class RankingOfPoints(View):
    template_name = 'ranking/ranking_of_points.html'

    def get(self, request, *args, **kwargs):
        cursor = connection.cursor()
        cursor.execute(
            '''SELECT award_registeraward.user_id,
            sum(public.award_award.point) FROM public.award_registeraward,
            public.award_award WHERE public.award_registeraward.award_id =
            public.award_award.id GROUP BY award_registeraward.user_id ORDER BY
             sum desc;''')
        data_of_ranking = cursor.fetchall()[:10]
        ranking_of_users = {}
        counter = 1
        for data in data_of_ranking:
            user = User.objects.get(id=data[0])
            ranking_of_users[counter] = {
                'user': user.username, 'point': int(data[1])
            }
            counter += 1
        return render(request, self.template_name, {
            'ranking_of_users': ranking_of_users}
        )


class Welcome(TemplateView):
    template_name = 'website/welcome.html'

    def get(self, request, *args, **kwargs):
        courses = Course.objects.filter(available=True)
        return render(request, self.template_name, {'courses': courses})


def google_analytcs(request):
    return {'google_analytcs': GOOGLE_ANALYTICS}
