from django.contrib.auth.models import User
from django.db import models


class FriendsInvented(models.Model):
    user = models.ForeignKey(User)
    email = models.EmailField()
    status = models.BooleanField(default=False)
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.email
