#-*- coding:UTF-8 -*-
from django.http.response import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import CreateView
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings

from accounts.models import UserProfile
from payments.models import Payment, PaymentOfPaypal
from payments.forms import CreditCardForm
from userena.forms import SignupForm
from .models import FriendsInvented
from django.utils import timezone
from datetime import timedelta

from additional.discounts.create_cupon import CreateCuponInvented
from additional.create_payments.create_payments_stripe import payments_stripe
import stripe
from emails.utils import SendEmail

if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
from discounts.models import DiscountPercentage

mp = settings.MIXPANEL

class ReferralView(TemplateView):
    template_name = "friends/referral.html"
    template_signup = "friends/referral-signup.html"

    def get_porcentage_discount(self):
        obj_discount_percentage = DiscountPercentage.objects.filter(
            apply_discount='discount_friends_invented'
        ).first()
        return obj_discount_percentage.percentage

    def get(self, request, *args, **kwargs):
        obj_user = request.user
        if obj_user.is_authenticated():
            porcentage_discount = self.get_porcentage_discount()
            return render(
                request,
                self.template_name,
                {'porcentage_discount': int(porcentage_discount)}
            )
        else:
            return render(request, self.template_signup)


class ReferralPlan(TemplateView):
    template_name = "friends/referral-plan.html"
    form_class = CreditCardForm
    form_user = SignupForm
    initial = {'key': 'value'}
    content_type = None

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            try:
                identify = request.user.id
                mp.track(identify, "Pricing viewed")
            except:
                pass
            try:
                Payment.objects.get(user=request.user)
                Payment.objects.get(user=request.user, status='Completed')
                return HttpResponseRedirect("/accounts/%s" % request.user)
            except:
                form = self.form_class(initial=self.initial)
                return render(request, self.template_name, {
                    "form": form,
                    "years": range(2015, 2036),
                    'months': range(1, 13),
                    "publishable": STRIPE_PUBLISHABLE
                })
        else:
            form = self.form_class(initial=self.initial)
            form_user = self.form_user(initial=self.initial)
            return render(request, self.template_name, {
                "form": form,
                "form_user": form_user,
                "years": range(2015, 2036),
                'months': range(1, 13),
                "publishable": STRIPE_PUBLISHABLE
            })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST or None)
        form_user = self.form_user(request.POST or None)
        if form.is_valid():
            if request.user.is_authenticated():
                is_new_user = False
                if not request.user.email:
                    user_obj = User.objects.get(id=request.user.id)
                    user_obj.email = request.POST["email"]
                    user_obj.save()
                    email = request.POST["email"]
                else:
                    email = request.user.email
                user_obj = request.user
            else:
                is_new_user = True
                if form_user.is_valid():
                    new_user = form_user.save()
                    email = form_user.cleaned_data["email"]
                    user_obj = User.objects.get(
                        username=form_user.cleaned_data["username"]
                    )
                    user_obj.first_name = request.POST["first_name"]
                    user_obj.last_name = request.POST["last_name"]
                    user_obj.is_active = True
                    user_obj.save()
                    user_activate = UserenaSignup.objects.get(
                        user_id=user_obj.id
                    )
                    user_activate.activation_key = 'ALREADY_ACTIVATED'
                    user_activate.save()
                    new_user = authenticate(
                        username=user_obj.username,
                        password=form_user.cleaned_data["password1"]
                    )
                    login(request, new_user)
                    identify = user_obj.id
                    mp.people_set(identify, {
                        '$first_name': user_obj.first_name,
                        '$last_name': user_obj.last_name,
                        '$email': user_obj.email,
                        'type': 'free',
                    })
                    mp.track(identify, "Account activated")
                else:
                    return render(
                        request,
                        self.template_name,
                        {
                            'form_user': form_user,
                            "years": range(2015, 2036),
                            'months': range(1, 13)
                        }
                    )
            if request.POST["type_of_payment"] == 'tarjeta':
                payments_stripe(
                    self,
                    request,
                    form,
                    request.POST['cupon'],
                    is_new_user,
                    user_obj,
                    form.cleaned_data["stripe_token"],
                    email
                )
                return redirect("/cursos/")
        else:
            return render(
                request,
                self.template_name,
                {
                    'form': form,
                    "years": range(2015, 2036),
                    'months': range(1, 13)
                }
            )
        return render(
            request,
            self.template_name,
            {
                "form": form,
                "years": range(2015, 2036),
                'months': range(1, 13)
            }
        )


class InventedView(CreateView):
    model = FriendsInvented

    def text_body(self, host, message, cupon, porcentage_discount):
        porcentage_discount = str(porcentage_discount) + str("%")
        body = """
            <p>%s te ha invitado para que hagas parte de la&nbsp;cominudad Exploiter.co,</p>
            <p><em><span style="color: rgb(119, 119, 119); font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.3999996185303px; background-color: rgb(255, 255, 255);">&quot; %s &quot;</span></em></p>
            <p>Gracias a tu amigo tendras %s de descuento en tu suscripci&oacute;n, solo tienes que hacer clic en el boton de abajo y utilizar el siguiente cup&oacute;n: <b>%s</b></p>
        """ % (host, message, porcentage_discount, cupon)
        return body

    def post(self, request, *args, **kwargs):
        datos = dict(request.POST)
        emails = datos['email']
        message = datos['message']
        is_paid = False
        for email in emails:
            try:
                obj_user_invented = User.objects.get(
                    email=email
                )
                paid_user = obj_user_invented.my_profile.paid_user
                if paid_user:
                    is_paid = True
            except:
                pass
            if not is_paid:
                try:
                    FriendsInvented.objects.get(
                        email=email
                    )
                except Exception, msg_error:
                    obj_friends_invented = FriendsInvented.objects.create(
                        user=request.user,
                        email=email
                    )
                    obj_friends_invented.save()

                    obj_create_cupon = CreateCuponInvented(
                        request.user,
                        email,
                        obj_friends_invented
                    )
                    obj_create_cupon.create_discount_user_invented()
                    cupon = obj_create_cupon.get_cupon()
                    porcentage_discount = obj_create_cupon.get_porcentage_discount()
                    to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                        email,
                        False,
                        'e@exploiter.co',
                        'Cursos de Exploiter.co',
                        "Un amigo tuyo acaba de regalarte un descuento del " + str(porcentage_discount) + str("%"),
                        'b0a0d7b4-b9d3-4fc7-b371-4b028fa06592',
                        self.text_body(
                                str(request.user.get_short_name()),
                                message[0],
                                cupon,
                                porcentage_discount
                            )
                        )
                    SendEmail(to_email, to_name, from_email, from_name, subject, template_name, html_text)
        return redirect('/amigos/plan/')
