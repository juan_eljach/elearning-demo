from django.contrib import admin
from .models import FriendsInvented

class AdminFriendsInveted(admin.ModelAdmin):
    list_display = ('user', 'email', 'status', 'active')
    search_fields = ('user__username', )

admin.site.register(FriendsInvented, AdminFriendsInveted)
