from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from .views import InventedView
from .views import ReferralView
from .views import ReferralPlan

urlpatterns = patterns('',
    url(r'^enviar-invitacion/$', InventedView.as_view(), name='send-invented'),
    url(r'^amigos/$', ReferralView.as_view(), name='referral'),
    url(r'^amigos/plan/$', ReferralPlan.as_view(), name='referral-plan'),
)
