from django import forms
from .models import Stripe

choices_price = (
    ("exploiter_un_mes", "1 MES"),
)

class CreditCardForm(forms.Form):
    last_4_digits = forms.CharField(
        required = True,
        min_length = 4,
        max_length = 4,
        widget = forms.HiddenInput()
    )
    stripe_token = forms.CharField(
        required = True,
        widget = forms.HiddenInput()
    )
    
    def addError(self, message):
        self._error[NON_FIELD_ERRORS] = self.error_class([message])

class StripeForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Stripe


class PaymentOfPaypalForm(forms.Form):
    email_of_user_of_exploiter = forms.EmailField()
    email_of_user_whoever_payment = forms.EmailField()
