from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from django.contrib.auth.models import User
from .forms import PaymentOfPaypalForm
from .models import PaymentOfPaypal

@dajaxice_register
def send_form(request, type_of_payment_of_paypal, email_of_user_whoever_payment, email_of_user_of_exploiter):
    dajax = Dajax()

    if type_of_payment_of_paypal == 'otro_quien_paga':
        if not email_of_user_whoever_payment or not email_of_user_of_exploiter:
            dajax.alert("Completa los Campos !!")
            dajax.redirect("/payments/paypal")
        else:
            try:
                user = User.objects.get(email=email_of_user_of_exploiter) 
                register_payment_of_paypal = PaymentOfPaypal.objects.create(user=user, email_whoever_payment=email_of_user_whoever_payment)
                register_payment_of_paypal.save()
            except:
                dajax.alert("Ocurrio un Error !!");
                dajax.redirect("/payments/paypal");
    return dajax.json()
