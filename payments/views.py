from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.http.response import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.views.generic import DeleteView
from django.shortcuts import redirect
from django.views.generic import View
from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings
from django.http import Http404
from userena.models import UserenaSignup
from userena.forms import SignupForm
import logging
from urllib2 import urlopen, Request
from urllib import urlencode
import stripe
import json
from mixpanel import Mixpanel
from django.utils import timezone
from datetime import timedelta

from additional.subscriptions.cancel_subscription import get_cancel_subscription
from payments.models import Payment, PaymentOfPaypal
from .forms import StripeForm, CreditCardForm
from discounts.models import DiscountUserNew
from .models import Stripe, PaymentOfPaypal
from discounts.models import DiscountUser
from payments.forms import CreditCardForm
from accounts.models import UserProfile
from users.forms import UserCreation
from courses.models import Course
from payments.models import UserTracking
from loggings.models import LoggingError

if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS


mp = settings.MIXPANEL


class Unsubscribe(DeleteView):
    template_name = 'payments/unsubscribe.html'
    success_url = '/cursos'

    def delete(self, request, *args, **kwargs):
        get_cancel_subscription(request.user)
        return HttpResponseRedirect(self.success_url)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)


class VerifyIpnOfStripe(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(VerifyIpnOfStripe, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        try:
            receiver_id = data['data']['object']['customer']
        except:
            receiver_id = 0
        try:
            payment_update = Payment.objects.get(receiver_id=receiver_id)
            user = UserProfile.objects.get(user=payment_update.user)
            identify = user.id
            if not data['data']['object']['refunded']:
                if data['data']['object']['status'] == 'succeeded' or data['data']['object']['status'] == 'paid':
                    payment_update.status = 'Completed'
                    payment_update.date_end_of_subscription = timezone.datetime.today().date() + timedelta(days=30)
                    payment_update.receiver_id = receiver_id
                    user.paid_user = True
                    user.type_of_payment = "stripe"
                    mp.people_set(identify, {'type': 'paid'})
                    mp.track(identify, "Successful Stripe payment")
                else:
                    user.paid_user = False
                    user.type_of_payment = "stripe"
                    payment_update.status = data['data']['object']['status']
                    payment_update.receiver_id = receiver_id
                    mp.people_set(identify, {'type': 'free'})
                    mp.track(identify, "Fail Stripe payment")
            else:
                user.paid_user = False
                user.type_of_payment = "stripe"
                payment_update.status = data['data']['object']['status']
                payment_update.receiver_id = receiver_id
                mp.people_set(identify, {'type': 'free'})
                mp.track(identify, "Refund Stripe payment")
            payment_update.save()
            user.save()
            return HttpResponse(status=200)
        except:
            return HttpResponse(status=200)


class VerifyIpnOfPaypal(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(VerifyIpnOfPaypal, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            user_paypal = PaymentOfPaypal.objects.get(
                email_whoever_payment=request.POST["payer_email"]
            )
            user = User.objects.get(username=user_paypal.user)
        except:
            user = User.objects.get(email=request.POST["payer_email"])
        try:
            receiver_id = request.POST["receiver_id"]
        except:
            receiver_id = 0
        try:
            txn_type = request.POST['txn_type']
            if txn_type == 'subscr_signup':
                mc_currency = request.POST['mc_currency']
                amount3 = request.POST['amount3']
                try:
                    sub_id = request.POST["subscr_id"]
                except:
                    sub_id = 0
                if str(amount3) == '25.00' and mc_currency == 'USD':
                    try:
                        payment_status = request.POST["payment_status"]
                    except:
                        payment_status = 'Completed'
                    try:
                        txn_id = request.POST["txn_id"]
                    except:
                        txn_id = 1
                    try:
                        payment_update = Payment.objects.get(user=user)
                        if request.POST["payment_status"] == 'Completed':
                            payment_update.date_end_of_subscription = timezone.datetime.today().date() + timedelta(days=30)
                            payment_update.token = request.POST["txn_id"]
                            payment_update.receiver_id = receiver_id
                        payment_update.save()
                    except:
                        try:
                            payment_form = Payment.objects.create(
                                user=user,
                                date_of_subscription=timezone.datetime.today().date(),
                                date_of_last_payment=timezone.datetime.today().date(),
                                date_end_of_subscription=timezone.datetime.today().date() + timedelta(days=30),
                                sub_id=sub_id,
                                status=payment_status,
                                token=txn_id,
                                receiver_id=receiver_id
                            )
                            payment_form.save()
                        except Exception as msg_error:
                            obj_logging = LoggingError.objects.create(
                                user=user,
                                name='paypal',
                                error=msg_error
                            )
                            obj_logging.save()
                    finally:
                        user = UserProfile.objects.get(user=user)
                        user.paid_user = True
                        user.type_of_payment = "paypal"
                        user.save()
                        identify = user.id
                        mp.people_set(identify, {'type': 'paid'})
                        mp.track(identify, "Successful PayPal payment")
            elif txn_type == 'subscr_cancel':
                payment_update = Payment.objects.get(user=user)
                user = UserProfile.objects.get(user=user)
                user.paid_user = True
                user.type_of_payment = "paypal"
                user.save()
                identify = user.id
                mp.people_set(identify, {'type': 'free'})
                mp.track(identify, "Canceled PayPal payment")
            elif txn_type == 'subscr_payment':
                try:
                    payment_update = Payment.objects.get(user=user)
                    payment_update.status = request.POST["payment_status"]
                    mc_currency = request.POST['mc_currency']
                    if mc_currency == 'USD':
                        if request.POST["payment_status"] == 'Completed':
                            payment_update.date_end_of_subscription = timezone.datetime.today().date() + timedelta(days=30)
                            payment_update.token = request.POST["txn_id"]
                            payment_update.receiver_id = receiver_id
                        payment_update.save()
                        user = UserProfile.objects.get(user=user)
                        user.paid_user = True
                        user.type_of_payment = "paypal"
                        user.save()
                        identify = user.id
                        mp.people_set(identify, {'type': 'paid'})
                        mp.track(identify, "Successful PayPal payment")
                except Exception as msg_error:
                    obj_logging = LoggingError.objects.create(
                        user=user,
                        name='paypal',
                        error=msg_error
                    )
                    obj_logging.save()
            elif txn_type == 'subscr_failed':
                user = UserProfile.objects.get(user=user)
                user.paid_user = False
                user.type_of_payment = "paypal"
                user.save()
                identify = user.id
                mp.people_set(identify, {'type': 'free'})
                mp.track(identify, "Failed PayPal payment")
        except Exception as msg_error:
            obj_logging = LoggingError.objects.create(
                user=user,
                name='paypal',
                error=msg_error
            )
            obj_logging.save()
        return HttpResponse(status=200)


class Traking(View):
    model = UserTracking
    success_url = '/suscribirme'

    def get_object(self):
        token = self.kwargs['token']
        obj = self.model.objects.get(token=token)
        return obj

    def get(self, request, *args, **kwargs):
        try:
            obj_traking = self.get_object()
            obj_traking.used = True
            obj_traking.save()
        except:
            pass
        return HttpResponseRedirect(self.success_url)


class SendForm(View):
    template_name = "website/pricing.html"
    form_user = SignupForm

    def post(self, request, *args, **kwargs):
        form_user = self.form_user(request.POST or None)
        if not request.user.is_authenticated():
            if form_user.is_valid():
                form_user.save()
                #email=form_user.cleaned_data["email"]
                user_obj = User.objects.get(
                    username=form_user.cleaned_data["username"]
                )
                user_obj.first_name = request.POST["first_name"]
                user_obj.last_name = request.POST["last_name"]
                user_obj.is_active = True
                user_obj.save()
                user_activate = UserenaSignup.objects.get(user_id=user_obj.id)
                user_activate.activation_key = 'ALREADY_ACTIVATED'
                user_activate.save()
                identify = user_obj.id
                mp.people_set(identify, {
                    '$first_name': user_obj.first_name,
                    '$last_name': user_obj.last_name,
                    '$email': user_obj.email,
                    'type': 'free',
                })
                mp.track(identify, "Account activated")
            else:
                return HttpResponse(
                    json.dumps(form_user.errors),
                    mimetype='application/javascript'
                )
        else:
            user_obj = request.user
        if request.POST["email_of_paypal"]:
            try:
                register_payment_of_paypal = PaymentOfPaypal.objects.create(
                    user=user_obj,
                    email_whoever_payment=request.POST["email_of_paypal"]
                )
                register_payment_of_paypal.save()
            except Exception as msg_error:
                obj_logging = LoggingError.objects.create(
                    user=user_obj,
                    name='paypal',
                    error=msg_error
                )
                obj_logging.save()
        return HttpResponse(json.dumps({'completed': 'completed'}),
            mimetype='application/javascript')
