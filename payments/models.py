from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


method_payment = (
    ('western_onion', 'Western Onion'),
    ('consignacion', 'consginacion'),
)


class Stripe(models.Model):
    user = models.OneToOneField(User)
    last_4_digits = models.CharField(max_length=255)
    stripe_id = models.CharField(max_length=255)
    sub_id = models.CharField(max_length=255)
    date_of_subscription = models.DateTimeField(default=timezone.now)
    date_of_last_payment = models.DateTimeField(default=timezone.now)


class Payment(models.Model):
    user = models.OneToOneField(User)
    date_of_subscription = models.DateField()
    date_of_last_payment = models.DateField()
    date_end_of_subscription = models.DateField()
    sub_id = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    receiver_id = models.CharField(max_length=255)

    def __unicode__(self):
        return self.user.username

class PaymentOfPaypal(models.Model):
    user = models.OneToOneField(User)
    email_whoever_payment = models.EmailField()

    def __unicode__(self):
        return self.user.username

class PaymentOther(models.Model):
    user = models.OneToOneField(User)
    date_of_subscription = models.DateField()
    date_of_last_payment = models.DateField()
    date_end_of_subscription = models.DateField()
    method_payment = models.CharField(max_length=254, choices=method_payment)

    def __unicode__(self):
        return self.user.username


class PaymentOxxo(models.Model):
    user = models.OneToOneField(User)
    type_identification = models.CharField(max_length=140, blank=True, null=True)
    identification = models.CharField(max_length=50, blank=True, null=True)
    date_of_subscription = models.DateField()
    date_of_last_payment = models.DateField()
    date_end_of_subscription = models.DateField()
    cell_phone = models.CharField(max_length=20, blank=True, null=True)

    def __unicode__(self):
        return self.user.username


class UserTracking(models.Model):
    user = models.OneToOneField(User)
    token = models.CharField(max_length=254)
    used = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username
