from django.contrib.auth.models import User
from django.contrib import admin
from .models import Payment
from .models import PaymentOther
from .models import PaymentOxxo
from .models import PaymentOfPaypal
from .models import UserTracking
from accounts.models import UserProfile


class PaymentOfPaypalAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )


class PaymentAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )
    list_filter = ('date_of_last_payment', )


class PaymentOtherAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )


class PaymentOxxoAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )

    def save_model(self, request, form, formset, change):
        instances = formset.save(commit=False)
        instances.save()
        obj_profile = UserProfile.objects.get(user=instances.user)
        obj_profile.paid_user = True
        obj_profile.save()

admin.site.register(PaymentOfPaypal, PaymentOfPaypalAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(PaymentOther, PaymentOtherAdmin)
admin.site.register(PaymentOxxo, PaymentOxxoAdmin)
admin.site.register(UserTracking)
