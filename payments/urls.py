from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import Unsubscribe
from .views import VerifyIpnOfStripe
from .views import VerifyIpnOfPaypal
from .views import SendForm
from .views import Traking

urlpatterns = patterns('',
    #url(regex=r'^payments/card$', view=FormStripe.as_view()),
    #url(regex=r'^payments/paypal$', view=login_required(FormPaypal.as_view())),
    url(r'^tracking/(?P<token>[\w\d\-]+)/$', Traking.as_view(), name='traking'),
    url(regex=r'^payments/verify_ipn$', view=VerifyIpnOfPaypal.as_view()),
    url(
        regex=r'^payments/verify_ipn_stripe$', view=VerifyIpnOfStripe.as_view()
    ),
    #url(regex=r'^payments/gracias-por-la-compra$', view=PurchaseComplete.as_view()),
    url(regex=r'^payments/unsubscribe$', view=Unsubscribe.as_view()),
    url(regex=r'^payments/ajax/form_paypal$', view=SendForm.as_view()),
)
