from django.db import models
from classes.models import CourseClass
from django.template.defaultfilters import slugify


class Lab(models.Model):
    name = models.CharField(max_length=140)
    description = models.TextField()
    labfile = models.FileField(upload_to="uploads", blank=True)
    course_class = models.ForeignKey(CourseClass)
    slug = models.SlugField(max_length=300)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Lab, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name