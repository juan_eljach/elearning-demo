from django.contrib import admin
from .models import Lab


class LabAdmin(admin.ModelAdmin):
    exclude = ("slug",)

admin.site.register(Lab, LabAdmin)
