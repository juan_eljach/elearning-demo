from django.contrib.auth.models import User
from django.conf import settings

if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
import stripe
from payments.models import Payment
from payments.models import PaymentOther
from payments.models import PaymentOxxo
from datetime import date
from loggings.models import LoggingError

def get_cancel_stripe(obj_user):
    try:
        obj_payment = Payment.objects.get(user=obj_user)
        obj_payment.status = 'Canceled'
        obj_payment.save()
        customer = stripe.Customer.retrieve(obj_payment.sub_id)
        sub_id = customer['subscriptions']['data'][0]['id']
        customer.subscriptions.retrieve(sub_id).delete()
    except Exception as msg_error:
        obj_logging = LoggingError.objects.create(
            user=obj_user,
            name='stripe',
            error=msg_error
        )
        obj_logging.save()
        return {}
    return {}

def get_cancel_subscription(obj_user):
    if obj_user.is_authenticated():
        if obj_user.my_profile.paid_user:
            if obj_user.my_profile.type_of_payment == 'paypal':
                pass
            elif obj_user.my_profile.type_of_payment == 'credit_card':
                get_cancel_stripe(obj_user)
    return {}


def get_status(request):
    try:
        obj_user = request.user
        if obj_user.is_authenticated():
            if obj_user.my_profile.paid_user:
                if obj_user.my_profile.type_of_payment == 'credit_card':
                    obj_payment = Payment.objects.get(user=obj_user)
                    if obj_payment.status != 'Canceled':
                        return {'status_subscribe': True}
    except:
        return {}
    return {}
