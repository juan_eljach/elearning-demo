from django.contrib.auth.models import User


def user_last_login():
    first_week = open('first_week.txt', 'w')
    second_week = open('second_week.txt', 'w')
    third_week = open('third_week.txt', 'w')

    first_month = open('first_month.txt', 'w')
    second_month = open('second_month.txt', 'w')
    third_month = open('third_month.txt', 'w')
    four_month = open('four_month.txt', 'w')
    five_month = open('five_month.txt', 'w')
    six_month = open('six_month.txt', 'w')
    seven_month = open('seven_month', 'w')
    eight_month = open('eight_month', 'w')

    users_first_week = User.objects.filter(
        last_login__range=('2015-04-13', '2015-04-21')
    )
    for user in users_first_week:
        first_week.writelines(user.email)
        first_week.writelines('\n')

    users_second_week = User.objects.filter(
            last_login__range=('2015-04-06', '2015-04-13')
    )
    for user in users_second_week:
        second_week.writelines(user.email)
        second_week.writelines('\n')

    users_third_week = User.objects.filter(
        last_login__range=('2015-03-30', '2015-04-06')
    )
    for user in users_third_week:
        third_week.writelines(user.email)
        third_week.writelines('\n')

    users_first_month = User.objects.filter(
        last_login__range=('2015-03-01', '2015-03-30')
    )
    for user in users_first_month:
        first_month.writelines(user.email)
        first_month.writelines('\n')

    users_second_month = User.objects.filter(
        last_login__range=('2015-02-01', '2015-03-01')
    )
    for user in users_second_month:
        second_month.writelines(user.email)
        second_month.writelines('\n')

    users_third_month = User.objects.filter(
        last_login__range=('2015-01-01', '2015-02-01')
    )
    for user in users_third_month:
        third_month.writelines(user.email)
        third_month.writelines('\n')

    users_four_month = User.objects.filter(
        last_login__range=('2014-12-01', '2015-01-01')
    )
    for user in users_four_month:
        four_month.writelines(user.email)
        four_month.writelines('\n')

    users_five_month = User.objects.filter(
        last_login__range=('2014-11-01', '2014-12-01')
    )
    for user in users_five_month:
        five_month.writelines(user.email)
        five_month.writelines('\n')

    users_six_month = User.objects.filter(
        last_login__range=('2014-10-01', '2014-11-01')
    )
    for user in users_six_month:
        six_month.writelines(user.email)
        six_month.writelines('\n')

    users_seven_month = User.objects.filter(
        last_login__range=('2014-09-01', '2013-10-01')
    )
    for user in users_seven_month:
        seven_month.writelines(user.email)
        seven_month.writelines('\n')

    users_eight_month = User.objects.filter(
        last_login__range=('2014-08-01', '2014-09-01')
    )
    for user in users_eight_month:
        eight_month.writelines(user.email)
        eight_month.writelines('\n')
