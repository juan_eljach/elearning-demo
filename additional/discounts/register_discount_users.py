from django.contrib.auth.models import User
from discounts.models import DiscountUser

email_users = open('email_users.txt', 'r')
code_users = open('code_users.txt', 'r')
code = code_users.readlines()[0].replace("\n", "")
print "Code: ", code
for email_user in email_users.readlines():
    email_user = email_user.replace("\n", "")
    print email_user
    try:
        obj_user = User.objects.get(email=email_user)
        try:
            DiscountUser.objects.get(
                code=code,
                user=obj_user
            )
        except:
            obj_discount = DiscountUser.objects.create(
                code=code,
                user=obj_user
            )
            obj_discount.save()
    except:
        pass
