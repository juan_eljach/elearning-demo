#-*- encoding: UTF-8 -*-
from django.conf import settings
from django.contrib.auth.models import User

from friends.models import FriendsInvented
from discounts.models import DiscountUser
from discounts.models import DiscountUserNew
from discounts.models import DiscountUserFriends
from discounts.models import DiscountPercentage
from discounts.models import DiscountFriendsInvented
import stripe
from django.utils.crypto import get_random_string
if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE


class CreateCuponInvented:

    def __init__(self, obj_user, email_friend_invented, obj_friends_invented, code=False):
        self.obj_user = obj_user
        self.email_friend_invented = email_friend_invented
        self.obj_friends_invented = obj_friends_invented
        self.id_discount_user = 0
        self.code = code

    def set_cupon(self):
        obj_discount_percentage = DiscountPercentage.objects.filter(
            apply_discount='discount_friends_invented'
        ).first()
        self.code = get_random_string(20)
        stripe.Coupon.create(
            percent_off=obj_discount_percentage.percentage,
            duration='once',
            id=self.code
        )

    def get_cupon(self):
        return self.code

    def get_type_of_user(self):
        try:
            obj_user = User.objects.get(
                email=self.email_friend_invented
            )
            return obj_user
        except:
            return False

    def get_porcentage_discount(self):
        obj_discount_percentage = DiscountPercentage.objects.filter(
            apply_discount='discount_friends_invented'
        ).first()
        return obj_discount_percentage.percentage

    def create_discount_user_invented(self):
        self.set_cupon()
        type_of_user = self.get_type_of_user()
        if type_of_user:
            obj_discount_user = DiscountUser.objects.create(
                code=self.get_cupon(),
                user=type_of_user
            )
            obj_discount_user.save()
            obj_discount_friends = DiscountFriendsInvented.objects.create(
                friends_invented=self.obj_friends_invented,
                is_user=True
            )
            obj_discount_friends.save()
        else:
            obj_discount_user_new = DiscountUserNew.objects.create(
                code=self.get_cupon(),
                email=self.email_friend_invented
            )
            obj_discount_user_new.save()
            obj_discount_friends = DiscountFriendsInvented.objects.create(
                friends_invented=self.obj_friends_invented
            )
            obj_discount_friends.save()



class CreateCuponUserFriend:

    def __init__(self, obj_user, obj_friends_invented, code=False):
        self.obj_user = obj_user
        self.obj_friends_invented = obj_friends_invented
        self.code = code

    def set_cupon(self):
        obj_discount_percentage = DiscountPercentage.objects.filter(
            apply_discount='discount_friends_invented'
        ).first()
        self.code = get_random_string(20)
        stripe.Coupon.create(
            percent_off=obj_discount_percentage.percentage,
            duration='once',
            id=self.code
        )

    def get_porcentage_discount(self):
        obj_discount_percentage = DiscountPercentage.objects.filter(
            apply_discount='discount_friends_invented'
        ).first()
        return obj_discount_percentage.percentage

    def get_cupon(self):
        return self.code


    def create_discount_user_friend(self):
        self.set_cupon()
        type_of_user = self.obj_user
        if type_of_user:
            obj_discount = DiscountUser.objects.create(
                code=self.get_cupon(),
                user=type_of_user
            )
            obj_discount.save()
            obj_discount_friends = DiscountUserFriends.objects.create(
                friends_invented=self.obj_friends_invented,
                discount_user=obj_discount
            )
            obj_discount_friends.save()
            return obj_discount
        else:
            return False
