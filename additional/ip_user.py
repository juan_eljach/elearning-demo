#-*- coding:utf-8 -*-
try:
    from django.contrib.gis.geoip import GeoIP
except:
    from django.contrib.gis.geoip import HAS_GEOIP as GeoIP
from users.models import IPUser


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_ip_user(request):
    if request.user.is_authenticated():
        try:
            geo = GeoIP()
            ip = get_client_ip(request)
            data_geo = geo.city(ip)
            city = False
            country = False
            if data_geo:
                city = data_geo['city']
                country = data_geo['country_name']
            IPUser.objects.get_or_create(
                user=request.user,
                ip=ip,
                city=city,
                country=country
            )
        except:
            pass
    return ''
