#-*- coding: UTF-8 -*-
from discounts.models import DiscountUserNew
from discounts.models import DiscountUser
from accounts.models import UserProfile
import stripe
from payments.models import Payment
from django.utils import timezone
from datetime import timedelta
from mixpanel import Mixpanel
from django.conf import settings
from django.shortcuts import render
from friends.models import FriendsInvented
from additional.discounts.create_cupon import CreateCuponUserFriend
from django.shortcuts import redirect
from emails.utils import SendEmail

mp = Mixpanel("92623cef2230cef5fb3c7e0748877016")

if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE


def text_body_free(short_name, cupon, porcentage_discount):
    porcentage_discount = str(porcentage_discount) + "%"
    body = """
        <p><span style="font-size:18px;">&iexcl;Hola %s !</span></p>
        <p style="text-align: start;"><span style="font-size: 16px; line-height: 25.6000003814697px;">Tu confiaste en nosotros y nos referenciastes a un amigo tuyo para que se uniera al plan Profesional y ahora hace&nbsp;parte de nuestra comunidad profesional, como quer&iacute;amos agradecerte por confiar en nosotros tu tambi&eacute;n tienes descuento&nbsp;del %s solo tienes que hacer clic en el bot&oacute;n de abajo y utilizar el siguiente cup&oacute;n: <b>%s</b></span></p>
    """ % (short_name, porcentage_discount, cupon)
    return body


def text_body_paid(short_name, porcentage_discount):
    porcentage_discount = str(porcentage_discount) + "%"
    body = """
        <p><span style="font-size:18px;">&iexcl;Hola %s !</span></p>
        <p style="text-align: start;"><span style="font-size: 16px; line-height: 25.6000003814697px;">Tu confiaste en nosotros y nos referenciastes a un amigo tuyo para que se uniera al plan Profesional y ahora hace&nbsp;parte de nuestra comunidad profesional, como quer&iacute;amos agradecerte por confiar en nosotros tu tambi&eacute;n tienes descuento&nbsp;del %s para tu pr&oacute;ximo mes</span></p>
    """ % (short_name, porcentage_discount)
    return body


def payments_stripe(self, request, form, cupon_of_discount, is_new_user,
    user_obj, stripe_token, email):
    try:
        cupon_of_discount = cupon_of_discount
        if cupon_of_discount:
            if is_new_user:
                try:
                    obj_discount = DiscountUserNew.objects.get(
                        code=cupon_of_discount,
                        email=user_obj.email,
                        used=False
                    )
                except:
                    obj_discount = False
            else:
                try:
                    obj_discount = DiscountUser.objects.get(
                        code=cupon_of_discount,
                        user=user_obj,
                        used=False
                    )
                except:
                    obj_discount = False
        else:
            obj_discount = False
        if obj_discount:
            coupon = stripe.Coupon.retrieve(obj_discount.code)
            try:
                customer = stripe.Customer.create(
                    email=email,
                    card=stripe_token,
                    coupon=coupon.id,
                )
                obj_discount.used = True
                obj_discount.save()
            except Exception:
                return render(
                    request,
                    self.template_name,
                    {
                        'form': form,
                        "error": "El Cupón Expiró",
                        "years": range(2015, 2036),
                        'months': range(1, 13)
                    }
                )
        else:
            customer = stripe.Customer.create(
                email=email,
                card=stripe_token
            )
        customer.save()
        customer_with_suscription = stripe.Customer.retrieve(
            customer.id
        )
        customer_with_suscription.subscriptions.create(
            plan='month-suscription'
        )
        customer_with_suscription.save()
        user_profile = UserProfile.objects.get(user=user_obj)
        user_profile.paid_user = True
        user_profile.type_of_payment = "credit_card"
        user_profile.save()
        payment_form = Payment.objects.create(
            user=user_obj,
            date_of_subscription=timezone.datetime.today().date(),
            date_of_last_payment=timezone.datetime.today().date(),
            date_end_of_subscription=(
                timezone.datetime.today().date() + timedelta(days=30)),
            sub_id=customer.id,
            status='Completed',
            token=stripe_token,
            receiver_id=customer.id
            )
        payment_form.save()
        #MIXPANEL TRACKING
        try:
            identify = user_obj.id
            mp.people_set(identify, {'type': 'paid'})
            mp.people_track_charge(
                identify, '25.00', {'$time': payment_form.date_of_subscription})
            mp.track(identify, "Successful Stripe payment")
        except:
            pass
        try:
            obj_friends = FriendsInvented.objects.get(
                email=email
            )
            obj_create_cupon = CreateCuponUserFriend(
                obj_friends.user,
                obj_friends
            )
            discount_user = obj_create_cupon.create_discount_user_friend()
            if discount_user:
                cupon = obj_create_cupon.get_cupon()
                porcentage_discount = obj_create_cupon.get_porcentage_discount()
                obj_user_friends_profile = UserProfile.objects.filter(
                    user=obj_friends.user
                ).last()
                if obj_user_friends_profile.paid_user:
                    obj_payment = Payment.objects.filter(
                        user=obj_friends.user
                    ).last()
                    customer = stripe.Customer.retrieve(obj_payment.sub_id)
                    sub_id = customer['subscriptions']['data'][0]['id']
                    subscription = customer.subscriptions.retrieve(sub_id)
                    subscription.coupon = discount_user.code
                    subscription.save()
                    obj_discount_user = DiscountUser.objects.filter(
                        user=obj_friends.user
                    ).last()
                    obj_discount_user.used = True
                    obj_discount_user.save()
                    to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                        obj_friends.user.email,
                        False,
                        'e@exploiter.co',
                        'Cursos de Exploiter.co',
                        "Un amigo tuyo acaba de regalarte un descuento del " + str(porcentage_discount) + str("%"),
                        '0e5fca5b-1137-4c49-ae45-cdfd75ec403c',
                        text_body_paid(
                                obj_friends.user.get_short_name(),
                                porcentage_discount
                            )
                        )
                else:
                    to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                        obj_friends.user.email,
                        False,
                        'e@exploiter.co',
                        'Cursos de Exploiter.co',
                        "Un amigo tuyo acaba de regalarte un descuento del " + str(porcentage_discount) + str("%"),
                        '6ae26347-35e3-467f-be30-a68bd53651f5',
                        text_body_free(
                                obj_friends.user.get_short_name(),
                                cupon,
                                porcentage_discount
                            )
                        )
                SendEmail(to_email, to_name, from_email, from_name, subject, template_name, html_text)
            return redirect("/cursos/")
        except:
            pass
        return redirect("/cursos/")
    except stripe.error.CardError, card_error:
        if is_new_user:
            user_obj.delete()
        return render(
            request,
            self.template_name,
            {
                'form': form,
                "error": card_error,
                "years": range(2015, 2036),
                'months': range(1, 13)
            }
        )
