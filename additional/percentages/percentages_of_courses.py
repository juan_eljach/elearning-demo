from datetime import date
from quiz.models import Sitting, Quiz
#from classes.models import CourseClass
from courses.models import Course


def percentage_by_course(obj_user, course):
    all_courseclass_approved = Sitting.objects.filter(
        user=obj_user.id, complete=True).order_by("-id")
    approved = []
    validate_sitting = []
    for classes in all_courseclass_approved:
        if not classes.quiz.id in validate_sitting:
            if classes.check_if_passed:
                approved.append(classes.quiz.id)
            validate_sitting.append(classes.quiz.id)
    all_quiz_of_course_of_user = Quiz.objects.filter(
        id__in=approved)
    classes = course.courseclass_set.all().only(
        "name", "image", "slug").order_by("id")
    available_classes = course.courseclass_set.filter(
        course=course,
        quiz__in=all_quiz_of_course_of_user,
        start_date__lte=date.today()
    ).order_by("id")
    try:
        percentage_by_course = (100.0 / float(classes.count()))
        percentage_by_course = percentage_by_course * (
            float(available_classes.count())
        )
    except:
        percentage_by_course = 1
    if percentage_by_course == 0:
        percentage_by_course = 1
    return int(percentage_by_course)
