# -*- coding: utf-8 -*-
from labs_containers.models import ApiExploiterPrivateBetaUser
from classes.models import CourseClass
from courses.models import Course
from quiz.models import Sitting
from quiz.models import Quiz


def can_access_material(obj_user, get_object):
    obj_beta_user = ApiExploiterPrivateBetaUser.objects.filter(
        user=obj_user,
        status=True
    )
    if obj_beta_user:
        pass
    else:
        all_sitting_by_user = Sitting.objects.filter(
            user=obj_user, complete=True).order_by("-id")
        approved = []
        validate_sitting = []
        course_class_id = []
        for classes in all_sitting_by_user:
            if not classes.quiz.id in validate_sitting:
                if classes.check_if_passed:
                    approved.append(classes.quiz.id)
                    course_class_id.append(classes.quiz.course_class.id)
                validate_sitting.append(classes.quiz.id)
        all_quiz_of_course_of_user = Quiz.objects.filter(
                                                    id__in=approved)
        obj_course_class = get_object
        course_id = get_object.course_id
        obj_course = Course.objects.get(id=course_id)
        obj_course_class = CourseClass.objects.filter(
            course=obj_course
        ).first()
        if all_quiz_of_course_of_user:
            obj_course_class_user = CourseClass.objects.filter(
                id__in=course_class_id,
                course=obj_course
            )
            if obj_course_class_user:
                obj_course_class_next = CourseClass.objects.filter(
                    id__gt=obj_course_class_user.last().id,
                    course=obj_course
                ).first()
                if get_object in obj_course_class_user or get_object == obj_course_class_next:
                    pass
                else:
                    return True
            else:
                obj_course_class_next = CourseClass.objects.filter(
                    course=obj_course
                ).first()
                if obj_course_class_next == get_object:
                    pass
                else:
                    return True
        else:
            if obj_course_class == get_object:
                pass
            else:
                return True
