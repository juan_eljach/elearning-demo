# -*- coding: utf-8 -*-
from accounts.models import UserProfile
from award.models import Award, RegisterAward

obj_award = Award.objects.get(name_reference="recoleccion-de-informacion")
obj_profiles = UserProfile.objects.filter(paid_user=False)
obj_users = []
for profile in obj_profiles:
    obj_users.append(profile.user)
obj_register_user = RegisterAward.objects.filter(
    award=obj_award,
    user__in=obj_users
)
email = open("emails_intro_hacking.txt", "w")
for register_user in obj_register_user:
    email.write(register_user.user.email)
    email.write("\n")
email.close()
print "Ok"