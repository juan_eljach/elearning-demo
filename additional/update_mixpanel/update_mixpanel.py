from accounts.models import UserProfile
from mixpanel import Mixpanel
import json
from django.utils.timesince import  timesince

mp = Mixpanel("92623cef2230cef5fb3c7e0748877016")
all_user_profile = UserProfile.objects.all()
for user_profile in all_user_profile:
    identify = user_profile.user.id
    last_login = str(timesince(user_profile.user.last_login).encode('UTF-8'))
    if user_profile.paid_user:
        mp.people_set(identify, {'type': 'paid', '$last_seen': last_login})
        mp.track(identify, "Successful")
    else:
        mp.people_set(identify, {'type': 'free', '$last_seen': last_login})
        mp.track(identify, "Free")
