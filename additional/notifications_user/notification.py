from django.conf import settings
from materials.models import Notification
from videos.models import Video
from articles.models import Article
from notifications_user.models import NotificationTeam
from notifications_user.models import NotificationComment
from django.utils.timesince import  timesince
from discussions.models import CommentVideo
from discussions.models import AnswerVideo

def get_notifications(request):
    if request.user.is_authenticated():
        notifications = []
        objs_notifications = Notification.objects.filter(
            user=request.user
        )[:5]

        for obj_notification in objs_notifications:
            slug_course = obj_notification.material.course_class.course.slug
            slug_course_class = obj_notification.material.course_class.slug
            url_material = "/cursos/" + slug_course
            slug_course_class = obj_notification.material.course_class.slug
            url_material += "/clases/" + slug_course_class
            name_material = False
            if obj_notification.material.type_material == 'video':
                obj_video = Video.objects.get(
                    id=obj_notification.material.id_data_material)
                name_material = obj_video.title
                slug_material = obj_video.slug
                url_material += "/video/" + obj_video.slug
            elif obj_notification.material.type_material == 'article':
                obj_article = Article.objects.get(
                    id=obj_notification.material.id_data_material)
                name_material = obj_article.title
                slug_material = obj_article.slug
                url_material += "/" + obj_article.slug
            notifications.append({
                'id': obj_notification.id,
                'user': obj_notification.user.username,
                'name_course': obj_notification.material.course_class.course.name,
                'name_classes': obj_notification.material.course_class.name,
                'name_material': name_material,
                'slug_course': slug_course,
                'slug_course_class': slug_course_class,
                'slug_material': slug_material,
                'url_material': url_material,
                'date': obj_notification.date,
                'type': obj_notification.material.type_material
            })
        return {
            'notifications': notifications,
            'async_url': settings.ASYNC_BACKEND_URL
        }
    else:
        return {}

def get_num_notifications(request):
    if request.user.is_authenticated():
        num_notifications = len(Notification.objects.filter(
            user=request.user,
            viewed=False
        )[:5])
        return {'num_notifications': num_notifications}
    return {}


def get_notifications_team(request):
    if request.user.is_authenticated():
        notifications = []
        obj_notification_team = NotificationTeam.objects.filter(
            user=request.user,
            viewed=False
        ).last()
        if obj_notification_team:
            notifications.append({
                'leader': obj_notification_team.team.teams.name,
                'user': obj_notification_team.user.username,
                'date': str(timesince(obj_notification_team.date).encode('UTF-8')),
            })
            return {
                'notifications_team': notifications,
                'async_url': settings.ASYNC_BACKEND_URL
            }
        else:
            return {}
    else:
        return {}

def get_notifications_comments(request):
    if request.user.is_authenticated():
        notifications = []
        obj_comment = CommentVideo.objects.filter(
            user_comment=request.user
        )
        if obj_comment:
            obj_answer = AnswerVideo.objects.filter(
                comment=obj_comment
            )

            obj_notification_comment = NotificationComment.objects.filter(
                answer=obj_answer,
                viewed=False
            )
            for notificaction_comment in obj_notification_comment:
                user_comment = str(notificaction_comment.answer.comment.user_comment.username)
                user = notificaction_comment.answer.user_answer.username
                slug = '/cursos/'
                slug += str(notificaction_comment.answer.comment.video.course_class.course.slug)
                slug += '/clases/'
                slug += str(notificaction_comment.answer.comment.video.course_class.slug)
                slug += '/video/'
                slug += str(notificaction_comment.answer.comment.video.slug)
                if user_comment != user:
                    notifications.append({
                        'user_comment': user_comment,
                        'user': user,
                        'url_comment': slug,
                        'date': str(timesince(notificaction_comment.date).encode('UTF-8')),
                    })
            return {
                'notifications_comments': notifications,
                'async_url': settings.ASYNC_BACKEND_URL
            }
        else:
            return {}
    else:
        return {}
