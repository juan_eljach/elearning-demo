from django.views.generic import ListView, DetailView
from .models import Course
from quiz.models import Sitting, Quiz
from classes.models import CourseClass

from django.utils.decorators import method_decorator
from exploiter.decorators import user_can_access

from mixpanel import Mixpanel

from datetime import date
from datetime import timedelta
from users.models import UserInitiationCourse
from django.shortcuts import render

mp = Mixpanel("92623cef2230cef5fb3c7e0748877016")


class CoursesList(ListView):
    model = Course
    template_name = "courses/courses.html"
    context_object_name = "courses"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            initiation_course = UserInitiationCourse.objects.filter(
                user=request.user
            )
            initiation_courses = []
            for initiation in initiation_course:
                initiation_courses.append(initiation.obj_course.id)
            initiation_course = self.model.objects.filter(
                id__in=initiation_courses
            )
            exclude_course_of_inititation = self.model.objects.exclude(
                id__in=initiation_courses
            )
            courses = exclude_course_of_inititation.filter(available=True)
        else:
            initiation_course = False
            courses = self.model.objects.filter(available=True)
        return render(request, self.template_name, {
            'courses': courses,
            'initiation_course': initiation_course
        })


class CourseDetailView(DetailView):
    model = Course
    model_sitting = Sitting
    model_courseclass = CourseClass
    model_quiz = Quiz
    template_name = "courses/course_detail.html"
    context_object_name = "course"

    def get_object(self):
        slug = self.kwargs['course_slug']
        obj = self.model.objects.get(slug__iexact=slug)
        return obj

    @method_decorator(user_can_access)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        try:
            identify = self.request.user.id
            course = str(context["course"])
            mp.track(identify, "Course: " + course + " viewed")
        except:
            pass
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(CourseDetailView, self).get_context_data(**kwargs)
        course = context["course"]
        try:
            UserInitiationCourse.objects.get(
                user=self.request.user,
                obj_course=course
            )
            print self.request.user.id
        except:
            initiation_of_course = UserInitiationCourse.objects.create(
                user=self.request.user,
                obj_course=course
            )
            initiation_of_course.save()
        context["classes"] = course.courseclass_set.all().only(
            "name", "image", "slug").order_by("id")
        context["all_classes"] = course.courseclass_set.filter(
            course=self.get_object()).order_by("id")
        try:
            all_courseclass_approved = self.model_sitting.objects.filter(
                user=self.request.user.id, complete=True).order_by("-id")
            approved = []
            validate_sitting = []
            context["active_course"] = True
            for classes in all_courseclass_approved:
                if not classes.quiz.id in validate_sitting:
                    if classes.check_if_passed:
                        approved.append(classes.quiz.id)
                    validate_sitting.append(classes.quiz.id)
            all_quiz_of_course_of_user = self.model_quiz.objects.filter(
                id__in=approved)
            context["available_classes"] = course.courseclass_set.filter(
                course=self.get_object(),
                quiz__in=all_quiz_of_course_of_user,
                start_date__lte=date.today()
            ).order_by("id")
            context["not_available_yet"] = course.courseclass_set.filter(
                course=self.get_object(),
                start_date__gt=date.today() + timedelta(days=1)
            ).order_by("id")
            context["disabled_classes"] = course.courseclass_set.exclude(
                course=self.get_object(),
                start_date__gt=date.today() + timedelta(days=1)
            ).exclude(
                id__in=context["available_classes"]
            ).order_by("id")
        except:
            context["active_course"] = False
            first_class = course.courseclass_set.filter(
                course=self.get_object())[0]
            context["available_classes"] = course.courseclass_set.filter(
                id=first_class.id).order_by("id")
            context["disabled_classes"] = course.courseclass_set.filter(
                id__gt=first_class.id).order_by("id")
        return context