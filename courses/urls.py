from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from .views import CoursesList, CourseDetailView

urlpatterns = patterns('',
    url(r'^cursos/$', CoursesList.as_view(), name='courses'),
    url(r'^cursos/(?P<course_slug>[\w\d\-]+)/$', CourseDetailView.as_view(), name='course'),

)
