# -*- coding: utf-8 -*-
from django.views.generic import ListView, DetailView
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.conf import settings
from datetime import date
from datetime import datetime
from datetime import timedelta
from users.models import UserInitiationCourse
from quiz.models import Sitting, Quiz
from classes.models import CourseClass
from exploiter.decorators import user_can_access_course
from .models import Course
from accounts.models import UserProfile
from trials.models import TrialOfCourses
from django.shortcuts import redirect
from labs_containers.models import ApiExploiterInstance
from labs_containers.models import ApiExploiterUser
from labs_containers.models import ApiExploiterPrivateBetaUser
from additional.api_labs_exploiter.ApiExploiter import createLabs
mp = settings.MIXPANEL
if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
    production = False
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
    production = True


def get_days_trial(user):
    days = False
    try:
        if user.is_authenticated():
            obj_trial = TrialOfCourses.objects.get(user=user)
            date_today = datetime.today()
            date_today = str(date_today.month) + '/' + str(date_today.day) + '/' + str(date_today.year)
            date_end = obj_trial.date_end
            date_end = str(date_end.month) + '/' + str(date_end.day) + '/' + str(date_end.year)
            date_today = datetime.strptime(date_today, '%m/%d/%Y')
            date_end = datetime.strptime(date_end, '%m/%d/%Y')
            if date_end >= date_today:
                days = abs((date_today - date_end).days)
                if days <= 0:
                    days = -1
            else:
                days = -1
    except:
        pass
    return days


def percentage_by_course(self, course):
    all_courseclass_approved = Sitting.objects.filter(
        user=self.request.user.id, complete=True).order_by("-id")
    approved = []
    validate_sitting = []
    for classes in all_courseclass_approved:
        if not classes.quiz.id in validate_sitting:
            if classes.check_if_passed:
                approved.append(classes.quiz.id)
            validate_sitting.append(classes.quiz.id)
    all_quiz_of_course_of_user = Quiz.objects.filter(
        id__in=approved)
    classes = course.courseclass_set.all().only(
        "name", "image", "slug").order_by("id")
    available_classes = course.courseclass_set.filter(
        course=course,
        quiz__in=all_quiz_of_course_of_user,
        start_date__lte=date.today()
    ).order_by("id")
    try:
        percentage_by_course = (100.0 / float(classes.count()))
        percentage_by_course = percentage_by_course * (
            float(available_classes.count())
        )
    except:
        percentage_by_course = 1
    if percentage_by_course == 0:
        percentage_by_course = 1
    return int(percentage_by_course)


class CoursesList(ListView):
    model = Course
    template_name = "courses/courses.html"
    context_object_name = "courses"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            initiation_course = UserInitiationCourse.objects.filter(
                user=request.user
            )
            initiation_courses = []
            for initiation in initiation_course:
                initiation_courses.append(initiation.obj_course.id)
            initiation_course = self.model.objects.filter(
                id__in=initiation_courses
            )
            exclude_course_of_inititation = self.model.objects.exclude(
                id__in=initiation_courses
            )
            courses = exclude_course_of_inititation.filter(available=True)
            percetage = {}
            for course in initiation_course:
                percetage[course] = percentage_by_course(self, course)
        else:
            initiation_course = False
            courses = self.model.objects.filter(available=True)
            percetage = False
        permission_to_view_course = False
        obj_profile = False
        try:
            obj_profile = UserProfile.objects.get(user=request.user)
            obj_trial = TrialOfCourses.objects.filter(
                user=request.user,
                date_end__gt=datetime.today().date()
            ).first()
            if obj_profile.paid_user or obj_trial:
                permission_to_view_course = True
        except:
            pass
        obj_trial_days = False
        if obj_profile:
            if not obj_profile.paid_user:
                obj_trial_days = get_days_trial(request.user)
        return render(request, self.template_name, {
            'permission_to_view_course': permission_to_view_course,
            'course_icon': True,
            'courses': courses,
            'initiation_course': initiation_course,
            'obj_trial_days': obj_trial_days,
            'percetage': percetage,
            'production': production,
        })


class CourseDetailView(DetailView):
    model = Course
    model_sitting = Sitting
    model_courseclass = CourseClass
    model_quiz = Quiz
    template_name = "courses/course_detail.html"
    context_object_name = "course"

    def get_object(self):
        slug = self.kwargs['course_slug']
        obj = self.model.objects.get(slug__iexact=slug)
        return obj

    @method_decorator(user_can_access_course)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if request.user.is_authenticated():
            obj_api = ApiExploiterPrivateBetaUser.objects.filter(user=request.user,
                status=True)
        else:
            obj_api = False
        obj_course = context['course']
        if obj_api and obj_course.course_type == 'free':
            self.template_name = 'courses/courses_new_template.html'
            try:
                obj_lab = ApiExploiterInstance.objects.get(course=obj_course)
                try:
                    ApiExploiterUser.objects.get(
                        api_instance=obj_lab,
                        user=request.user,
                        status=True
                    )
                except Exception as msg_error:
                    try:
                        createLabs(obj_lab, request.user)
                    except Exception as msg_error:
                        print 'Error: ', msg_error
                        pass
            except:
                pass
        try:
            identify = self.request.user.id
            course = str(context["course"])
            mp.track(identify, "Course: " + course + " viewed")
        except:
            pass
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(CourseDetailView, self).get_context_data(**kwargs)
        course = context["course"]
        context['course_icon'] = True
        context['production'] = production
        if self.request.user.is_authenticated():
            context['permission_to_view_course'] = False
            obj_profile = UserProfile.objects.get(user=self.request.user)
            try:
                obj_trial = TrialOfCourses.objects.filter(
                    user=self.request.user,
                    date_end__gt=datetime.today().date()
                ).first()
                if obj_profile.paid_user or obj_trial:
                    context['permission_to_view_course'] = True
            except:
                return redirect("courses")
            if not obj_profile.paid_user:
                context['obj_trial_days'] = get_days_trial(self.request.user)
            else:
                context['obj_trial_days'] = False
            print self.request.user
            if self.request.user.my_profile.paid_user or context['permission_to_view_course']:
                try:
                    UserInitiationCourse.objects.get(
                        user=self.request.user,
                        obj_course=course
                    )
                except:
                    initiation_of_course = UserInitiationCourse.objects.create(
                        user=self.request.user,
                        obj_course=course
                    )
                    initiation_of_course.save()
            else:
                pass
            context["classes"] = course.courseclass_set.all().only(
                "name", "image", "slug").order_by("id")
            context["all_classes"] = course.courseclass_set.filter(
                course=self.get_object()).order_by("id")
            try:
                all_courseclass_approved = self.model_sitting.objects.filter(
                    user=self.request.user.id, complete=True).order_by("-id")
                approved = []
                validate_sitting = []
                context["active_course"] = True
                for classes in all_courseclass_approved:
                    if not classes.quiz.id in validate_sitting:
                        if classes.check_if_passed:
                            approved.append(classes.quiz.id)
                        validate_sitting.append(classes.quiz.id)
                all_quiz_of_course_of_user = self.model_quiz.objects.filter(
                    id__in=approved)
                context["available_classes"] = course.courseclass_set.filter(
                    course=self.get_object(),
                    quiz__in=all_quiz_of_course_of_user,
                    start_date__lte=date.today()
                ).order_by("id")
                context["not_available_yet"] = course.courseclass_set.filter(
                    course=self.get_object(),
                    start_date__gt=date.today() + timedelta(days=1)
                ).order_by("id")
                context["disabled_classes"] = course.courseclass_set.exclude(
                    course=self.get_object(),
                    start_date__gt=date.today() + timedelta(days=1)
                ).exclude(
                    id__in=context["available_classes"]
                ).order_by("id")
                context['percentage_by_course'] = percentage_by_course(
                    self, course)
            except:
                context['percentage_by_course'] = 1
                context["active_course"] = False
                first_class = course.courseclass_set.filter(
                    course=self.get_object())[0]
                context["available_classes"] = course.courseclass_set.filter(
                    id=first_class.id).order_by("id")
                context["disabled_classes"] = course.courseclass_set.filter(
                    id__gt=first_class.id).order_by("id")
        else:
            context["all_classes"] = course.courseclass_set.filter(
                course=self.get_object()).order_by("id")
        return context
