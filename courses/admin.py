from django.contrib import admin
from .models import Course
from .models import TeacherCourses


class CourseAdmin(admin.ModelAdmin):
    ordering = ["id"]
    exclude = ("slug",)
    filter_horizontal = ("teacher_courses",)
    search_fields = ('name', )

    class Media:
        js = ('/static/tiny_mce/tiny_mce.js',
              '/static/tiny_mce/textarea.js',)

    def save_model(self, request, form, formset, change):
        super(CourseAdmin, self).save_model(request, form, formset, change)
        formset.save_m2m()
        form.save()


class TeacherCoursesAdmin(admin.ModelAdmin):
    search_fields = ('name', )
    list_display = ('name', 'teacher_image', 'teacher_description')

admin.site.register(Course, CourseAdmin)
admin.site.register(TeacherCourses, TeacherCoursesAdmin)
