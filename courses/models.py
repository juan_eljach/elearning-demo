from django.db import models
from django.template.defaultfilters import slugify

class TeacherCourses(models.Model):
    name = models.CharField(max_length=140)
    teacher_image = models.ImageField(upload_to="uploads")
    teacher_description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name


class Course(models.Model):
    TYPES = (
        ('paid', 'Pago'),
        ('free', 'Gratis'),
    )

    name = models.CharField(max_length=140)
    description = models.TextField()
    image = models.ImageField(upload_to="uploads")
    slug = models.SlugField(max_length=300)
    teacher_courses = models.ManyToManyField(TeacherCourses, null=True, blank=True)
    available = models.BooleanField(default=True)
    course_type = models.CharField(max_length=20, choices=TYPES, default="paid")
    image_landing = models.ImageField(upload_to="uploads", null=True, blank=True)
    short_description = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Course, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
