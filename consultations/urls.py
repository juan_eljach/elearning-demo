from django.conf.urls import include, url, patterns
from .views import WrongQuestions

urlpatterns = patterns('',
    url(r'^adminkakeador/consulta/questions/$', WrongQuestions.as_view()),
)
