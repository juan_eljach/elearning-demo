from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.shortcuts import render
from courses.models import Course
from quiz.models import Quiz
from quiz.models import Sitting
from quiz.models import Question
import operator

class WrongQuestions(TemplateView):
    template_name = 'consultations/WrongQuestions.html'

    @method_decorator(permission_required('is_superuser'))
    def dispatch(self, *args, **kwargs):
        return super(WrongQuestions, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        obj_settings = Sitting.objects.filter(complete=True)
        list_incorrect_questions = {}
        for object_sitting in obj_settings:
            for incorrect_questions in object_sitting.get_incorrect_questions:
                try:
                    list_incorrect_questions[incorrect_questions] = 1 + list_incorrect_questions[incorrect_questions]
                except:
                    list_incorrect_questions[incorrect_questions] = 1
        ordered = sorted(list_incorrect_questions.iteritems(), key=operator.itemgetter(1))
        ordered.reverse()
        list_id_questions = []
        list_value_questions = []
        for id_question in ordered:
            try:
                query = Question.objects.get(id=id_question[0])
                list_id_questions.append(query)
                list_value_questions.append({'id': id_question[0], 'value': id_question[1]})
            except:
                pass
        obj_courses = Course.objects.filter(available=True)
        context = {
            'WrongQuestions': list_id_questions,
            'courses': obj_courses,
            'list_value_questions': list_value_questions
        }
        return render(request, self.template_name, context)
