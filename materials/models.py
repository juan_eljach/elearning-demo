#coding: utf-8
from django.utils.timesince import  timesince
from django.contrib.auth.models import User
from django.db import models
from classes.models import CourseClass
from django.conf import settings
import json
import urllib
import urllib2
import threading
from celery.task.base import periodic_task
from django.utils.timezone import timedelta


def send_event(event_type, event_data):
    to_send = {
        'event': event_type,
        'data': event_data
    }
    urllib2.urlopen(settings.ASYNC_BACKEND_URL, urllib.urlencode(to_send))


class Material(models.Model):
    course_class = models.ForeignKey(CourseClass)
    id_data_material = models.IntegerField()
    type_material = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        super(Material, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.course_class.name


class Notification(models.Model):
    user = models.ForeignKey(User)
    material = models.ForeignKey(Material)
    date = models.DateTimeField(auto_now=True)
    viewed = models.BooleanField(default=False)

    class Meta:
        ordering = ('-id', )

    def __unicode__(self):
        return self.user.username

    def as_dict(self):
        from videos.models import Video
        from articles.models import Article
        slug_course = self.material.course_class.course.slug
        url_material = "/cursos/" + slug_course
        slug_course_class = self.material.course_class.slug
        url_material += "/clases/" + slug_course_class
        name_material = False
        if self.material.type_material == 'video':
            obj_video = Video.objects.get(id=self.material.id_data_material)
            name_material = obj_video.title
            url_material += "/video/" + obj_video.slug
        elif self.material.type_material == 'article':
            obj_article = Article.objects.get(id=self.material.id_data_material)
            name_material = obj_article.title
            url_material += "/" + obj_article.slug
        data = {
            'id': self.pk,
            'user': self.user.username,
            'name_course': self.material.course_class.course.name,
            'name_classes': self.material.course_class.name,
            'name_material': name_material,
            'url_material': url_material,
            'date': str(timesince(self.date).encode('UTF-8')),
            'type': self.material.type_material,
        }
        return json.dumps(data)

    def save(self, *args, **kwargs):
        is_new = False
        if not self.pk:
            is_new = True
        super(Notification, self).save(*args, **kwargs)
        if is_new:
            send_event('message-create', self.as_dict())
        else:
            send_event('message', self.as_dict())
