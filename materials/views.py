# coding: utf-8
from django.http import HttpResponse
from django.views import generic
from .models import Notification
from notifications_user.models import NotificationComment
from discussions.models import CommentVideo
from discussions.models import AnswerVideo
import json


class NotificationViewed(generic.View):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            obj_notifications = Notification.objects.filter(
                user=request.user,
                viewed=False
            )
            obj_comment_video = CommentVideo.objects.filter(
                user_comment=request.user,

            )
            obj_answer = AnswerVideo.objects.filter(
                comment=obj_comment_video
            )
            obj_notifications_comments = NotificationComment.objects.filter(
                answer=obj_answer
            )
            for notifications_comment in obj_notifications_comments:
                obj_comment = NotificationComment.objects.get(
                    id=notifications_comment.id
                )
                obj_comment.viewed = True
                obj_comment.save()
            for notification in obj_notifications:
                obj_notification = Notification.objects.get(
                    id=notification.id
                )
                obj_notification.viewed = True
                obj_notification.date = notification.date
                obj_notification.save()
        return HttpResponse(json.dumps({'status': 'ok'}),
            mimetype='application/javascript')
