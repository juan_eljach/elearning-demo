from django.contrib import admin
from .models import Notification
from .models import Material

class NotificationAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )

class MaterialAdmin(admin.ModelAdmin):
    search_fields = ('type_material', )

admin.site.register(Notification, NotificationAdmin)
admin.site.register(Material, MaterialAdmin)
