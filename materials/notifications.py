#coding: utf-8
from django.contrib.auth.models import User
from accounts.models import UserProfile
from celery import task
from users.models import UserInitiationCourse
from materials.models import Notification
from materials.models import Material
from payments.models import Payment
from payments.models import PaymentOther
from payments.models import PaymentOxxo
from datetime import date


def get_date_end_of_subscription(user):
    try:
        obj_payment = Payment.objects.get(user=user)
        date_end = obj_payment.date_end_of_subscription
    except:
        try:
            obj_payment = PaymentOther.objects.get(user=user)
            date_end = obj_payment.date_end_of_subscription
        except:
            try:
                obj_payment = PaymentOxxo.objects.get(user=user)
                date_end = obj_payment.date_end_of_subscription
            except:
                date_end = False
    return date_end


@task(ignore_result=True)
def register_of_notification(pk_material, course_class, id_data_material, type_material):
    initiation_courses = UserInitiationCourse.objects.filter(
        obj_course=course_class.course
    )
    for initiation in initiation_courses:
        if course_class.course.course_type == 'paid':
            date_end_of_subscription = get_date_end_of_subscription(initiation.user)
            if date_end_of_subscription:
                if date.today() <= get_date_end_of_subscription(initiation.user):
                    obj_notification = Notification(
                        user=initiation.user,
                        material=pk_material
                    )
                    obj_notification.save()
        else:
            obj_notification = Notification(
                user=initiation.user,
                material=pk_material
            )
            obj_notification.save()
