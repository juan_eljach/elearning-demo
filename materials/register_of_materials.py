# -*- coding: utf-8 -*-
from videos.models import Video
from materials.models import Material
from articles.models import Article

#usage:
    #python manage.py shell
    #from materials import register_of_materials
#registro de material de videos
videos = Video.objects.all()
for video in videos:
    register_material = Material.objects.create(
        course_class=video.course_class,
        id_data_material=video.id,
        type_material='video'
    )
    register_material.save()

#registro de material de articulo
articles = Article.objects.all()
for article in articles:
    for course_class in article.course_class.all():
        register_material = Material.objects.create(
            course_class=course_class,
            id_data_material=article.id,
            type_material='article'
        )
        register_material.save()