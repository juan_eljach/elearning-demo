from django.contrib.auth.models import User
from award.models import RegisterAward, Award
from quiz.models import Sitting
from celery import task
from users.models import UserProgressMaterial
from materials.models import Material
from users.models import UserInitiationCourse


@task
def register_of_award():
    all_user = User.objects.all().order_by("id")
    for obj_user in all_user:
        print obj_user.id
        sitting_of_user = Sitting.objects.filter(user=obj_user)
        approved_of_user = []
        for sitting in sitting_of_user:
            if sitting.check_if_passed:
                approved_of_user.append(sitting)
        count = 0
        for approved in approved_of_user:
            try:
                award_id = Award.objects.get(
                    name_reference=approved.quiz.course_class.slug
                )
                try:
                    RegisterAward.objects.get(
                        award=award_id, user=obj_user
                    )
                except:
                    register_award = RegisterAward(
                        award=award_id,
                        user=obj_user, relation=approved
                    )
                    register_award.save()
                    print "Guardo"
                    try:
                        register_of_progress_material(
                            approved.quiz.course_class,
                            obj_user
                        )
                    except Exception, error:
                        print "Error: ", error
                    if count == 0:
                        try:
                            register_viewed_course(
                                approved.quiz.course_class.course,
                                obj_user
                            )
                            count = 1
                        except Exception, error:
                            print "Error: ", error
            except:
                pass


@task
def register_of_progress_material(course_class, obj_user):
    all_material = Material.objects.filter(
        course_class=course_class
    )
    for material in all_material:
        try:
            UserProgressMaterial.objects.get(
                user=obj_user,
                id_data_material=material.id_data_material,
                type_material=material.type_material
            )
        except:
            user_material = UserProgressMaterial.objects.create(
                user=obj_user,
                id_data_material=material.id_data_material,
                type_material=material.type_material,
            )
            user_material.save()
            print "Guardando Material"


@task
def register_viewed_course(course, user):
    print "Entro"
    try:
        UserInitiationCourse.objects.get(
            user=user,
            obj_course=course
        )
    except:
        initiation_of_course = UserInitiationCourse.objects.create(
            user=user,
            obj_course=course
        )
        initiation_of_course.save()
        print "Guardado Course"