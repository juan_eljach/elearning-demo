from .models import Award, RegisterAward
from quiz.models import Quiz, Sitting
from classes.models import CourseClass
from courses.models import Course


class CourseClassAward():
    """Class what return object of Award for CourseClass Complete"""
    def __init__(self, datos):
        try:
            quiz = Quiz.objects.get(id=datos.quiz.id)
            course_class = CourseClass.objects.get(id=quiz.course_class_id)
            sitting = Sitting.objects.filter(
                user=datos.request.user,
                quiz=quiz
            ).order_by("id").last()
            if sitting.check_if_passed:
                award_id = Award.objects.get(name_reference=course_class.slug)
                register_award = RegisterAward(
                    award=award_id, user=datos.request.user, relation=datos)
                register_award.save()
                CourseAward(course_class, datos)
        except:
            pass


class CourseAward():
    """Class what return object of Arward for Course Complete"""
    def __init__(self, course_class, datos):
        try:
            course = Course.objects.get(id=course_class.course_id)
            all_course_class_of_course = CourseClass.objects.filter(
                course=course)
            all_slug_of_course = []
            for course in all_course_class_of_course:
                all_slug_of_course.append(course.slug)
            all_award_of_course = Award.objects.filter(
                name_reference__in=all_slug_of_course)
            all_register_award_by_user = RegisterAward.objects.filter(
                award__in=all_award_of_course, user=datos.request.user)
            if len(all_course_class_of_course) == len(
                all_register_award_by_user):
                award_id = Award.objects.get(name_reference=course.slug)
                register_award = RegisterAward(
                    award=award_id, user=datos.request.user, relation=datos)
                register_award.save()
        except:
            pass
