from django.contrib.auth.models import User
from django.db import models


class Award(models.Model):
    name_show = models.CharField(max_length=140)
    name_reference = models.CharField(max_length=140)
    image = models.ImageField(upload_to="uploads")
    point = models.IntegerField()
    description = models.TextField()

    def __unicode__(self):
        return self.name_show


class RegisterAward(models.Model):
    award = models.ForeignKey(Award)
    user = models.ForeignKey(User)
    date = models.DateField(auto_now=True)
    relation = models.CharField(max_length=254)

    def __unicode__(self):
        return self.award.name_show
