from django import forms
from django.contrib import admin
from .models import Award, RegisterAward


class AwardAdminForm(forms.ModelForm):

    class Meta:
        model = Award
        fields = ["name_show", "name_reference", "image", "point", "description"]

    def save(self, commit=True):
        award = super(AwardAdminForm, self).save(commit=False)
        award.save()
        return award


class AwardAdmin(admin.ModelAdmin):
    form = AwardAdminForm
    search_fields = ('name_show', )


class RegisterAwardAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )

admin.site.register(Award, AwardAdmin)
admin.site.register(RegisterAward, RegisterAwardAdmin)
