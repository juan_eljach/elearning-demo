from django.contrib.auth.models import User
from django.db import models
from courses.models import Course
from django.template.defaultfilters import slugify

##Nuevo
import re
import json
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator
from django.utils.timezone import now
from model_utils.managers import InheritanceManager
from celery import shared_task
from videos.models import Video


class DifficultyOfChallenges(models.Model):
    name = models.CharField(max_length=140)

    def __unicode__(self):
        return self.name

class Challenges(models.Model):
    name = models.CharField(max_length=140)
    description = models.TextField()
    image = models.ImageField(upload_to="uploads")
    slug = models.SlugField(max_length=300)
    available = models.BooleanField(default=True)
    difficulty = models.ForeignKey(DifficultyOfChallenges)
    pass_mark = models.SmallIntegerField(blank=True,
                    default=0,
                    help_text="Percentage required to pass challenge.",
                    validators=[MaxValueValidator(100)])
    title_lost = models.CharField(max_length=140)
    content_lost = models.CharField(max_length=140)
    title_winner = models.CharField(max_length=140)
    content_winner = models.CharField(max_length=140)
    content_discount = models.CharField(max_length=140)
    start_date = models.DateTimeField()
    date_completed = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        if self.pass_mark > 100:
            raise ValidationError(u'%s is above 100' % self.pass_mark)
        super(Challenges, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

class DiscountsChallenge(models.Model):
    code = models.CharField(max_length=254)
    challenge = models.ForeignKey(Challenges)
    used = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(DiscountsChallenge, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.challenge.name

class LevelOfChallenge(models.Model):
    title = models.CharField(max_length=60,blank=False)
    description = models.TextField(blank=True,
                                   help_text="a description of the Level")
    challenge = models.ForeignKey(Challenges, null=True, blank=True)

    def save(self,*args, **kwargs):
        super(LevelOfChallenge, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(self.title)


class QuestionChallenge(models.Model):
    level_challenge = models.ForeignKey(LevelOfChallenge,blank=True)
    content = models.CharField(max_length=1000,
                               blank=False,
                               help_text="Enter the question text that "
                                         "you want displayed",
                               verbose_name='Question')
    objects = InheritanceManager()

    def __unicode__(self):
        return unicode(self.content)


class AnswerChallenge(models.Model):
    question = models.ForeignKey(QuestionChallenge)

    content = models.CharField(max_length=1000,
                               blank=False,
                               help_text="Enter the answer text that "
                                         "you want displayed")

    correct = models.BooleanField(blank=False,
                                  default=False,
                                  help_text="Is this a correct answer?")

    def __unicode__(self):
        return unicode(self.content)

    class Meta:
        ordering = ['id']

class AnswerChallengeUser(models.Model):
    user = models.ForeignKey(User)
    answer_challenge = models.ForeignKey(AnswerChallenge)

    def save(self, *args, **kwargs):
        super(AnswerChallengeUser, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.user.username

class ProgressUser(models.Model):
    user = models.ForeignKey(User)
    challenge = models.ForeignKey(Challenges)
    terminated = models.BooleanField(default=False)
    date_initiation = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        super(ProgressUser, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(ProgressUser, self).save(*args, **kwargs)

class WinnersChallenge(models.Model):
    user = models.ForeignKey(User)
    discount = models.ForeignKey(DiscountsChallenge)

    def __unicode__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        super(WinnersChallenge, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(WinnersChallenge, self).save(*args, **kwargs)


class VideoChallenge(models.Model):
    challenge = models.ForeignKey(Challenges)
    video = models.ForeignKey(Video)

    def __unicode__(self):
        return self.challenge.name
