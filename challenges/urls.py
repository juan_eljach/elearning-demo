from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from .views import ChallengeDetailView
from .views import ChallengesView

urlpatterns = patterns('',
    url(r'^retos/$', login_required(ChallengesView.as_view()), name='challenges'),
    url(r'cursos/(?P<course_slug>[\w\d\-]+)/clases/(?P<class_slug>[\w\d\-]+)/lab/(?P<reto_slug>[\w\d\-]+)/$',   login_required(ChallengeDetailView.as_view()), name="challenge"),
)
