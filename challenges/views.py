# -*- coding: utf-8 -*-
import datetime
import json
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import View
from django.http import Http404
from .models import Challenges
from classes.models import CourseClass
from .models import LevelOfChallenge
from .models import QuestionChallenge
from .models import AnswerChallenge
from quiz.models import Quiz, Sitting
from exploiter.decorators import user_can_access
from datetime import date
from datetime import timedelta
from mixpanel import Mixpanel
from users.models import UserProgressMaterial
from materials.models import Material
from .models import AnswerChallengeUser
from .models import AnswerChallenge
from courses.models import Course
from labs_containers.models import ApiExploiterUser
from django.conf import settings
from users.previous_next_classes import url_next_classes
from .models import ProgressUser
from .models import DiscountsChallenge
from django.core.urlresolvers import reverse
from .models import WinnersChallenge
from .models import VideoChallenge
from award.models import Award
from award.models import RegisterAward
from django.shortcuts import get_object_or_404

#Container
from containers_challenge.models import ContainerImageChallenge
from containers_challenge.models import ContainerUserChallenge
from containers_challenge.views import create_container
from containers_challenge.views import start_container
from containers_challenge.views import stop_container
from containers_challenge.views import restart_container
from additional.api_labs_exploiter.ApiExploiter import updateLabs

mp = Mixpanel("92623cef2230cef5fb3c7e0748877016")

class ChallengesView(TemplateView):
    template_name = 'challenges/all_challenge.html'

    def get_context_data(self, **kwargs):
        context = super(ChallengesView, self).get_context_data(**kwargs)
        context['challenges'] = Challenges.objects.filter(
            available=True,
            start_date__lt=datetime.datetime.now()
        )
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class ChallengeDetailView(DetailView):
    model = Challenges
    model_level = LevelOfChallenge
    model_question = QuestionChallenge
    model_answer = AnswerChallenge
    template_name = "challenges/challenge.html"
    context_object_name = "challenges"

    def get_object(self):
        try:
            slug = self.kwargs['reto_slug']
            obj = self.model.objects.get(
                slug=slug,
                start_date__lt=datetime.datetime.now()
            )
        except:
            raise Http404
        return obj

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_answer_user(self):
        answer = AnswerChallengeUser.objects.filter(
            user=self.request.user
        ).last()
        return answer

    def get_course(self):
        obj_course = get_object_or_404(Course,
            slug=self.kwargs['course_slug'])
        return obj_course

    def get_user_last_question(self):
        answer_user = self.get_answer_user()
        if answer_user is None:
            return False
        else:
            obj_user_question = answer_user.answer_challenge.question
            obj_last_question = self.model_question.objects.filter(
                level_challenge=answer_user.answer_challenge.question.level_challenge
            ).last()
            if obj_user_question == obj_last_question:
                return True
            else:
                return False

    def get_last_level_challenge(self):
        last_level_challenge = LevelOfChallenge.objects.filter(
            challenge=self.get_object()
        ).last()
        return last_level_challenge

    def get_last_question(self):
        last_question = QuestionChallenge.objects.filter(
            level_challenge=self.get_last_level_challenge()
        ).last()
        return last_question

    def get_level_challenge(self):
        answer_user = self.get_answer_user()
        if answer_user is None:
            level = self.model_level.objects.filter(
                challenge=self.get_object()).first()
        else:
            if self.get_user_last_question() is True:
                level=self.model_level.objects.filter(
                    id__gt=answer_user.answer_challenge.question.level_challenge.id
                ).first()
            else:
                level=answer_user.answer_challenge.question.level_challenge
        return level

    def get_len_question_of_level(self):
        all_question_of_level = self.model_question.objects.filter(
            level_challenge=self.get_level_challenge()
        )
        print len(all_question_of_level)
        if len(all_question_of_level) == 1:
            list = [1]
        else:
            list = range(0, len(all_question_of_level))
        return list

    def get_time_progress(self):
        answer_user = self.get_answer_user()
        if answer_user is None:
            pass
        return 0

    def get_questions(self):
        answer_user = self.get_answer_user()
        if answer_user is None or self.get_user_last_question() is True:
            question = self.model_question.objects.filter(
                level_challenge=self.get_level_challenge()
            ).first()
        else:
            question = self.model_question.objects.filter(
                id__gt=answer_user.answer_challenge.question.id
            ).first()
        return question

    def get_answers(self):
        answers = self.model_answer.objects.filter(
            question=self.get_questions()
        )
        return answers

    def get_progress_user(self):
        answer = AnswerChallengeUser.objects.filter(
            user=self.request.user
        )
        return len(answer)

    def get_value_progress(self):
        objs_level_challenge = LevelOfChallenge.objects.filter(
            challenge=self.get_object()
        )
        progress = QuestionChallenge.objects.filter(
            level_challenge__in=objs_level_challenge
        )
        try:
            value_progress = 100 // len(progress)
        except:
            value_progress = 1
        return value_progress

    def get_time(self, challenges):
        date_server = datetime.date.today()
        date_reto = challenges.date_completed.date()
        date_remaining = date_reto - date_server
        date_remaining = int(date_remaining.total_seconds())
        time_now = datetime.datetime.now()
        time_server = datetime.timedelta(
            hours=time_now.hour,
            minutes=time_now.minute,
            seconds=time_now.second
        )
        time_reto = datetime.timedelta(
            hours=challenges.date_completed.hour,
            minutes=challenges.date_completed.minute,
            seconds=challenges.date_completed.second
        )
        total_time = time_reto - time_server
        time = date_remaining
        time += int(total_time.total_seconds())
        return time

    def get_ids_answers(self):
        ids_answers = []
        for answer in self.get_answers():
            ids_answers.append(answer.id)
        return ids_answers

    def get_len_answer_user(self):
        len_answer_user = AnswerChallengeUser.objects.filter(
            user=self.request.user)
        return len(len_answer_user)

    def get_discount_available(self):
        avaialble_discount = DiscountsChallenge.objects.filter(
            challenge=self.get_object(),
            used=False
        ).first()
        if avaialble_discount == None:
            avaialble_discount = False
        return avaialble_discount

    def get_all_discount_available(self):
        all_discount = DiscountsChallenge.objects.filter(
            challenge=self.get_object()
        )
        if all_discount == None:
            all_discount = False
        return all_discount

    def get_percentage_user(self):
        percentage_challenge = self.get_object().pass_mark
        obj_level_challenge = LevelOfChallenge.objects.filter(
            challenge=self.get_object()
        )
        obj_question_challenge= QuestionChallenge.objects.filter(
            level_challenge__in=obj_level_challenge
        )
        obj_answers_challenge = AnswerChallenge.objects.filter(
            question__in=obj_question_challenge
        )
        obj_answer_user = AnswerChallengeUser.objects.filter(
            user=self.request.user,
            answer_challenge__in=obj_answers_challenge
        )
        correct = 0
        for answer_user in obj_answer_user:
            if answer_user.answer_challenge.correct:
                correct += 1
        len_question_challenge = len(obj_question_challenge)
        try:
            value_percentage = 100 // len_question_challenge
        except:
            value_percentage = 1
        total = value_percentage * correct
        if total >= percentage_challenge:
            return True
        else:
            return False

    def set_winner_challenge(self):
        if self.get_discount_available():
            try:
                user_winner = WinnersChallenge.objects.get(
                    user=self.request.user,
                    discount__in=self.get_all_discount_available()
                )
            except:
                user_winner = WinnersChallenge.objects.create(
                    user=self.request.user,
                    discount=self.get_discount_available()
                )
                user_winner.save()
                discount_used = DiscountsChallenge.objects.get(
                    id=self.get_discount_available().id
                )
                discount_used.used = True
                discount_used.save()
            return user_winner.discount.code
        else:
            return False

    def get_object_course_class(self):
        obj_course = Course.objects.get(slug=self.kwargs['course_slug'])
        obj_course_class = CourseClass.objects.get(
            course=obj_course,
            slug=self.kwargs['class_slug']
        )
        return obj_course_class

    def set_user_terminated(self):
        progress_user = ProgressUser.objects.get(
            user=self.request.user,
            challenge=self.get_object()
        )
        if progress_user.terminated is False:
            progress_user.terminated = True
            progress_user.save()
        return 0

    def get_object_video(self):
        obj_challenge = get_object_or_404(Challenges,
            slug=self.kwargs['reto_slug'])
        obj = VideoChallenge.objects.get(challenge=obj_challenge)
        return obj.video

    def get_context_data(self, **kwargs):
        context = super(ChallengeDetailView, self).get_context_data(**kwargs)
        try:
            ProgressUser.objects.get(
                user=self.request.user,
                challenge=self.get_object()
            )
        except:
            progress_user = ProgressUser.objects.create(
                user=self.request.user,
                challenge=self.get_object()
            )
            progress_user.save()
        #context['time'] = self.get_time(context['challenges'])
        #if context['time'] <= 0:
        #    context['time'] = 1
        context['time'] = datetime.datetime.now() + timedelta(days=7)
        level = self.get_level_challenge()
        if level is None:
            terminated = True
            self.set_user_terminated()
            if self.get_percentage_user():
                winner_challenge=self.set_winner_challenge()
                if winner_challenge:
                    code = winner_challenge
                    msg_winner_code = str(self.get_object().content_discount)
                else:
                    code = False
                    msg_winner_code = False
                msg_title = str(self.get_object().title_winner)
                msg_content = str(self.get_object().content_winner)
                try:
                    obj_adward = Award.objects.get(
                        name_reference='challenges-' + str(self.get_object().slug)
                    )
                    try:
                        obj_adward_user = RegisterAward.objects.get(
                            award=obj_adward,
                            user=self.request.user
                        )
                    except:
                        obj_adward_user = RegisterAward.objects.create(
                            award=obj_adward,
                            user=self.request.user,
                            relation='challenges'
                        )
                        obj_award_user.save()
                except:
                    pass
            else:
                code = False
                msg_winner_code = False
                msg_title = self.get_object().title_lost
                msg_content = self.get_object().content_lost
        else:
            terminated = False
            msg_content = False
            code = False
            msg_title = False
            msg_winner_code = False
        dict_msg = {
            'msg_winner_code': msg_winner_code,
            'msg_title': msg_title,
            'msg_content': msg_content,
        }
        try:
            api_user = ApiExploiterUser.objects.get(user=self.request.user,
                status=True)
            if api_user.ip_publica == '127.0.0.1':
                updateLabs(api_user)
                api_user = ApiExploiterUser.objects.get(user=self.request.user,
                    status=True)
            iframe_container = "http://{0}:8080".format(
                api_user.ip_publica
            )
        except:
            iframe_container = False
        if iframe_container:
            context['iframe_container'] = iframe_container
        else:
            context['iframe_container'] = ''
        context['terminated'] = terminated,
        context['msgs']= dict_msg,
        context['level'] = level
        context['question'] = self.get_questions()
        context['count_questions'] = self.get_len_question_of_level()
        context['answers'] = self.get_answers()
        context['url'] = self.kwargs['reto_slug']
        context['len'] = self.get_len_answer_user()
        context['course'] = self.get_course()
        context['url_next_class'] = url_next_classes(
            self.get_object_course_class(),
            self.get_object_video().id,
            'video',
            self.request.user,
            True
        )
        context['new_level'] = self.get_user_last_question(),
        context['initiation_progress'] = (self.get_value_progress() * self.get_progress_user()) + 1
        return context

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated() and request.is_ajax():
            if int(self.get_level_challenge().id) == int(request.POST['level']):
                if int(self.get_questions().id) == int(request.POST['question']):
                    if int(request.POST['answer']) in self.get_ids_answers():
                        obj_question = QuestionChallenge.objects.get(
                            id=request.POST['level']
                        )
                        obj_answer_challenge = AnswerChallenge.objects.get(
                            id=request.POST['answer']
                        )
                        answer_user = AnswerChallengeUser.objects.create(
                            user=request.user,
                            answer_challenge=obj_answer_challenge
                        )
                        answer_user.save()
                        obj_answer_user = AnswerChallengeUser.objects.get(
                            id=answer_user.id
                        )
                        status = obj_answer_user.answer_challenge.correct
                        level = self.get_level_challenge()
                        if level is None:
                            dict_level = {}
                            dict_question = {}
                            list_answers = {}
                            terminated = True
                            self.set_user_terminated()
                            if self.get_percentage_user():
                                winner_challenge=self.set_winner_challenge()
                                if winner_challenge:
                                    code = winner_challenge
                                    msg_winner_code = self.get_object().content_discount
                                else:
                                    code = False
                                    msg_winner_code = False
                                msg_title = self.get_object().title_winner
                                msg_content = self.get_object().content_winner
                                try:
                                    obj_adward = Award.objects.get(
                                        name_reference='challenges-' + str(self.get_object().slug)
                                    )
                                    try:
                                        obj_adward_user = RegisterAward.objects.get(
                                            award=obj_adward,
                                            user=self.request.user
                                        )
                                    except:
                                        obj_adward_user = RegisterAward.objects.create(
                                            award=obj_adward,
                                            user=self.request.user,
                                            relation='challenges'
                                        )
                                        obj_award_user.save()
                                except:
                                    pass
                            else:
                                code = False
                                msg_winner_code = False
                                msg_title = self.get_object().title_lost
                                msg_content = self.get_object().content_lost
                        else:
                            code = False
                            msg_title = False
                            msg_winner_code = False
                            dict_level = {
                                'id_level': level.id,
                                'title':level.title,
                                'description': level.description
                            }
                            question = self.get_questions()
                            dict_question = {
                                'id_question': question.id,
                                'content': question.content
                            }
                            answers = self.get_answers()
                            list_answers = []
                            for answer in answers:
                                dict_answers = {
                                    'id': answer.id,
                                    'content': answer.content,
                                }
                                list_answers.append(dict_answers)
                            terminated = False
                            msg_content = False
                        return HttpResponse(json.dumps({
                            'terminated': terminated,
                            'level': dict_level,
                            'question': dict_question,
                            'answers': list_answers,
                            'status': status,
                            'code': code,
                            'len': self.get_len_answer_user(),
                            'count_questions': self.get_len_question_of_level(),
                            'msg_winner_code': msg_winner_code,
                            'msg_title': msg_title,
                            'msg_content': msg_content,
                            'new_level': self.get_user_last_question(),
                            'progress': (self.get_value_progress() * self.get_progress_user())
                            }),
                            mimetype='application/javascript')
                    else:
                        return HttpResponse(json.dumps({
                            'bloqued': True
                        }),
                        mimetype='application/javascript')
                else:
                    return HttpResponse(json.dumps({
                        'bloqued': True
                    }),
                    mimetype='application/javascript')
            else:
                return HttpResponse(json.dumps({
                    'bloqued': True
                }),
                mimetype='application/javascript')
        else:
            return HttpResponseRedirect("/")
