from django.contrib import admin
from .models import Challenges
from .models import LevelOfChallenge
from .models import QuestionChallenge
from .models import AnswerChallenge
from .models import AnswerChallengeUser
from .models import ProgressUser
from .models import DiscountsChallenge
from .models import WinnersChallenge
from .models import DiscountsChallenge
from .models import DifficultyOfChallenges
from .models import VideoChallenge
from materials.models import Material
from containers_challenge.models import ContainerImageChallenge


class ContainerImagenInline(admin.TabularInline):
    model = ContainerImageChallenge


class AnswerChallengeInline(admin.TabularInline):
    model = AnswerChallenge


class DiscountsChallengeInline(admin.TabularInline):
    model = DiscountsChallenge


class QuestionChallengeAdmin(admin.ModelAdmin):
    inlines = [AnswerChallengeInline]


class ChallengesAdmin(admin.ModelAdmin):
    exclude = ("slug",)
    inlines = [DiscountsChallengeInline, ContainerImagenInline]


class VideoChallengeAdmin(admin.ModelAdmin):
    search_fields = ('challenge__name', )

    def save_model(self, request, form, formset, change):
        instances = formset.save(commit=False)
        instances.save()
        try:
            Material.objects.get(
                course_class=instances.video.course_class,
                id_data_material=instances.id,
                type_material='lab'
            )
        except:
            register_material = Material.objects.create(
                course_class=instances.video.course_class,
                id_data_material=instances.id,
                type_material='lab'
            )
            register_material.save()

admin.site.register(ProgressUser)
admin.site.register(Challenges, ChallengesAdmin)
admin.site.register(LevelOfChallenge)
admin.site.register(QuestionChallenge, QuestionChallengeAdmin)
admin.site.register(AnswerChallengeUser)
admin.site.register(WinnersChallenge)
admin.site.register(DiscountsChallenge)
admin.site.register(DifficultyOfChallenges)
admin.site.register(VideoChallenge, VideoChallengeAdmin)
