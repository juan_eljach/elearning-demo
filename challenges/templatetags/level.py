from django import template
from challenges.models import Challenges
from challenges.models import LevelOfChallenge

register = template.Library()


@register.tag
def get_level_of_challenge(parser, token):
    return level_of_challenge()

class level_of_challenge(template.Node):

    def render(self, context):
        obj_levels = LevelOfChallenge.objects.filter(
            challenge=context['challenge']
        )
        context['obj_levels'] = obj_levels
        return ''
