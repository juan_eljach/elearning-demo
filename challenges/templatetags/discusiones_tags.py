from django import template

from classes.models import Comment

register = template.Library()

@register.inclusion_tag('classes/discusiones.html', takes_context=True)
def show_discusiones(context):
	discusiones = Comment.objects.all().order_by('-id')

	return {'discusiones': discusiones}
