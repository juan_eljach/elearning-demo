#-*- coding: UTF-8 -*-
from django.contrib.auth.models import User
from loggings.models import LoggingError
from .models import Email
from celery import task
import sendgrid


@task
def SendEmail(to_email, to_name, from_email, from_name, subject, template_name,
    html_text, text=None):
    sg = sendgrid.SendGridClient('juaneljach', 'imperionazza100%')
    message = sendgrid.Mail()
    message.add_to(to_email)
    if to_name:
        message.add_to_name(to_name)
    message.set_from(from_email)
    message.set_from_name(from_name)
    message.set_subject(subject)
    if template_name:
        message.add_filter('templates', 'enabled', '1')
        message.add_filter('templates', 'template_id', template_name)
    if html_text:
        message.set_html(html_text)
    if text:
        message.set_text(text)
    try:
        sg.send(message)
        obj_email = Email.objects.create(
            subject=subject,
            to_email=to_email,
            to_name=to_name,
            from_email=from_email,
            from_name=from_name,
            sent=True,
            template_name=template_name,
            text=text,
            html_text=html_text
        )
        obj_email.save()
    except Exception, msg_error:
        try:
            obj_user = User.objects.get(email=to_email)
            obj_logging = LoggingError.objects.create(
                user=obj_user,
                name='email',
                error=msg_error
            )
            obj_logging.save()
        except:
            pass
