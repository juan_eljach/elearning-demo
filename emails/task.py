from .models import Email
from celery import task


@task()
def send_all_emails():
    emails = Email.objects.filter(sent=False)[:50]
    for email in emails:
        email.send_email()
