from django.db import models


class Email(models.Model):
    subject = models.CharField(max_length=255)
    to_email = models.EmailField(max_length=255, blank=False)
    to_name = models.CharField(max_length=255, blank=True)
    from_email = models.EmailField(max_length=255, blank=False, default='Cursos de Exploiter.co <e@exploiter.co>')
    from_name = models.CharField(max_length=255)
    sent = models.BooleanField(default=False)
    template_name = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(max_length=5000, blank=True, null=True)
    html_text = models.TextField(max_length=5000, blank=True, null=True)
    date_send = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s' % self.to_email
