from django.contrib.auth.models import User
from django.contrib import admin
from .models import Email

class EmailAdmin(admin.ModelAdmin):
    search_fields = ('to_email', )
    date_hierarchy = 'date_send'
    list_display = ('to_email', 'to_name', 'subject', 'from_email', 'from_name', 'sent', 'template_name', 'date_send')

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distintc = super(EmailAdmin, self).get_search_results(request, queryset, search_term)
        try:
            obj_user = User.objects.get(username=search_term)
            queryset |= self.model.objects.filter(to_email=obj_user.email)
        except:
            pass
        return queryset, use_distintc
admin.site.register(Email, EmailAdmin)
