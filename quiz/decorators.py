from functools import wraps
from classes.models import CourseClass
from courses.models import Course
from quiz.models import Quiz
from quiz.models import Sitting
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from datetime import date
from payments.models import Payment
from payments.models import PaymentOther
from payments.models import PaymentOxxo
from trials.models import TrialOfCourses
from datetime import date
from datetime import datetime
from datetime import timedelta
from accounts.models import UserProfile


def get_date_end_of_subscription(user):
    try:
        obj_payment = Payment.objects.get(user=user)
        date_end = obj_payment.date_end_of_subscription
    except:
        try:
            obj_payment = PaymentOther.objects.get(user=user)
            date_end = obj_payment.date_end_of_subscription
        except:
            try:
                obj_payment = PaymentOxxo.objects.get(user=user)
                date_end = obj_payment.date_end_of_subscription
            except:
                date_end = False
    return date_end

def validate_user_can_access(view_func):
    def __response_decoractor(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        try:
            kwargs_name = kwargs["quiz_name"]
        except:
            try:
                kwargs_name = kwargs["slug"]
            except:
                return redirect("courses")
        quiz = Quiz.objects.get(url=kwargs_name)
        class_of_quiz = quiz.course_class
        if class_of_quiz.course.available is False:
            if request.user.is_staff:
                return response
            else:
                return redirect("courses")
        user_profile = UserProfile.objects.get(user=request.user)
        if not user_profile.paid_user:
            obj_trial = TrialOfCourses.objects.filter(
                user=request.user,
                date_end__gt=datetime.today().date()
            ).first()
            if obj_trial:
                course = class_of_quiz.course
                obj_class = class_of_quiz
                objs_class_basic = course.courseclass_set.filter(level='basic').order_by('id')
                if len(objs_class_basic) <= 4:
                    objs_class_intermediate = course.courseclass_set.filter(
                        level='intermediate'
                    ).order_by('id')[:4-len(objs_class_basic)]
                    len_class = len(objs_class_basic) + len(objs_class_intermediate)
                    if len_class <= 4:
                        objs_class_advanced = course.courseclass_set.filter(
                            level='advanced'
                        ).order_by('id')[:4-len_class]
                if obj_class in objs_class_basic or obj_class in objs_class_intermediate or obj_class in objs_class_advanced:
                    return response
                else:
                    return redirect('courses')
        if class_of_quiz.course.course_type == "paid" and not request.user.get_profile().paid_user:
                return redirect("pricing")
        if class_of_quiz.course.course_type == 'paid' and request.user.get_profile().paid_user:
            user_date = get_date_end_of_subscription(request.user)
            if user_date:
                if date.today() > user_date:
                    return redirect("pricing")
            else:
                return redirect("pricing")
        last_sitting_of_user = quiz.sitting_set.filter(user=request.user, complete=True).last()
        if last_sitting_of_user is None:
            return response
        try:
            request.GET['exam_result']
            exam_result = True
        except:
            exam_result = False
        if last_sitting_of_user.check_if_passed and exam_result is False:
            return redirect(reverse("clase", kwargs={
                "course_slug": class_of_quiz.course.slug,
                "class_slug": class_of_quiz.slug}))
        else:
            return response
    return wraps(view_func)(__response_decoractor)


def validate_user_can_access_to_exam(view_func):
    def __response_decoractor(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        try:
            kwargs_name = kwargs["quiz_name"]
        except:
            try:
                kwargs_name = kwargs["slug"]
            except:
                return redirect("courses")
        quiz = Quiz.objects.get(url=kwargs_name)
        class_of_quiz = quiz.course_class
        try:
            all_sittings_approved = Sitting.objects.filter(
                user=request.user.id,
                complete=True
            ).order_by("-id")
            approved = []
            validate_sitting = []
            for sitting in all_sittings_approved:
                if not sitting.quiz.id in validate_sitting:
                    if sitting.check_if_passed:
                        approved.append(sitting.quiz.id)
                    validate_sitting.append(sitting.quiz.id)
            all_quiz_of_course_of_user = Quiz.objects.filter(
                id__in=approved
            )
            course = Course.objects.get(id=class_of_quiz.course_id)
            available_classes = CourseClass.objects.filter(
                course=class_of_quiz.course_id,
                quiz__in=all_quiz_of_course_of_user,
                start_date__lt=date.today()
            ).order_by("id")
            try:
                disabled_classes = course.courseclass_set.exclude(
                    start_date__gt=date.today()
                ).exclude(
                    id__in=available_classes
                ).order_by("id")[0]
            except:
                disabled_classes = False
            if class_of_quiz in available_classes:
                return response
            elif class_of_quiz == disabled_classes:
                return response
            else:
                return redirect(reverse("clase", kwargs={
                    "course_slug": class_of_quiz.course.slug,
                    "class_slug": class_of_quiz.slug}))
        except Exception:
            return response
    return wraps(view_func)(__response_decoractor)
