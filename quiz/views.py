import random

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, TemplateView, FormView

from .forms import QuestionForm, EssayForm
from .models import Quiz, Category, Progress, Sitting, Question
from essay.models import Essay_Question
from courses.models import Course
from classes.models import CourseClass
from celery import task
from award.type_award import CourseClassAward
from loggings.models import LoggingError
from mixpanel import Mixpanel
from django.conf import settings
from attempts.models import Attempts
from datetime import datetime
mp = settings.MIXPANEL

class QuizMarkerMixin(object):
    @method_decorator(login_required)
    @method_decorator(permission_required('quiz.view_sittings'))
    def dispatch(self, *args, **kwargs):
        return super(QuizMarkerMixin, self).dispatch(*args, **kwargs)


class SittingFilterTitleMixin(object):
    def get_queryset(self):
        queryset = super(SittingFilterTitleMixin, self).get_queryset()
        quiz_filter = self.request.GET.get('quiz_filter')
        if quiz_filter:
            queryset = queryset.filter(quiz__title__icontains=quiz_filter)

        return queryset

class CourseQuizzesProgressDetailView(DetailView):
    model = Course
    template_name = "progress.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CourseQuizzesProgressDetailView, self).get_context_data(*args, **kwargs)
        course = context["course"]
        classes = course.courseclass_set.all()
        quizzes = list()
        for x in classes:
            if len(x.quiz_set.all()) < 1:
                pass
            else:
                quiz=x.quiz_set.all()[0]
                quizzes.append(quiz)
        sittings = list()
        for quiz in quizzes:
            if len(quiz.sitting_set.all().filter(user=self.request.user)) < 1:
                pass
            else:
                sitting = quiz.sitting_set.all().filter(user=self.request.user)[0]
                sittings.append(sitting)
        context["sittings"] = sittings
        return context

class QuizListView(ListView):
    model = Quiz

    def get_queryset(self):
        queryset = Quiz.objects.all().order_by("id")
        return queryset

    def get_context_data(self, **kwargs):
        context = super(QuizListView, self).get_context_data(**kwargs)
        all_sitting_by_user = Sitting.objects.filter(user=self.request.user.id, complete=True).order_by("-id")
        approved = []
        validate_sitting = []
        sitting_user = []
        for classes in all_sitting_by_user:
            if not classes.quiz.id in validate_sitting:
                if classes.check_if_passed:
                    approved.append(classes.quiz.id)
                    sitting_user.append(classes.id)
                validate_sitting.append(classes.quiz.id)
        context["all_course"] = Course.objects.filter(available=True)
        context["all_quiz_of_course_of_user"] = self.model.objects.filter(
                                                    id__in=approved)
        context["all_sittings_of_user"] = Sitting.objects.filter(
                                                    id__in=sitting_user)
        context['exam_icon'] = True
        return context

class QuizDetailView(DetailView):
    model = Quiz
    slug_field = 'url'

    def get_object_course_class(self):
        slug = self.kwargs['slug']
        obj_quiz = self.model.objects.get(url=slug)
        obj = obj_quiz.course_class
        return obj

    def get_quiz(self):
        obj = Quiz.objects.get(course_class=self.get_object_course_class())
        return obj

    def get(self, request, *args, **kwargs):
        obj_user = request.user
        obj_course_class = self.get_object_course_class()
        obj_attempts = Attempts.objects.filter(user=obj_user, date=datetime.today(), quiz=self.get_quiz())
        if len(obj_attempts) >= 2:
            url = '/cursos/' + obj_course_class.course.slug + '/clases/' + obj_course_class.slug
            return redirect(url)
        return super(QuizDetailView, self).get(request, *args, **kwargs)

class CategoriesListView(ListView):
    model = Category


class ViewQuizListByCategory(ListView):
    model = Quiz
    template_name = 'view_quiz_category.html'

    def dispatch(self, request, *args, **kwargs):
        self.category = get_object_or_404(
            Category,
            category=self.kwargs['category_name']
        )

        return super(ViewQuizListByCategory, self).\
            dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ViewQuizListByCategory, self)\
            .get_context_data(**kwargs)

        context['category'] = self.category
        context['exam_icon'] = True
        return context

    def get_queryset(self):
        queryset = super(ViewQuizListByCategory, self).get_queryset()
        return queryset.filter(category=self.category)


class QuizUserProgressView(TemplateView):
    template_name = 'progress.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(QuizUserProgressView, self)\
            .dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(QuizUserProgressView, self).get_context_data(**kwargs)
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        context['cat_scores'] = progress.list_all_cat_scores
        context['exams'] = progress.show_exams()
        context['exam_icon'] = True
        return context


class QuizMarkingList(QuizMarkerMixin, SittingFilterTitleMixin, ListView):
    model = Sitting

    def get_queryset(self):
        queryset = super(QuizMarkingList, self).get_queryset()\
                                               .filter(complete=True)

        user_filter = self.request.GET.get('user_filter')
        if user_filter:
            queryset = queryset.filter(user__username__icontains=user_filter)

        return queryset


class QuizMarkingDetail(QuizMarkerMixin, DetailView):
    model = Sitting

    def post(self, request, *args, **kwargs):
        sitting = self.get_object()

        q_to_toggle = request.POST.get('qid', None)
        if q_to_toggle:
            q = Question.objects.get_subclass(id=int(q_to_toggle))
            if int(q_to_toggle) in sitting.get_incorrect_questions:
                sitting.remove_incorrect_question(q)
            else:
                sitting.add_incorrect_question(q)

        return self.get(request)

    def get_context_data(self, **kwargs):
        context = super(QuizMarkingDetail, self).get_context_data(**kwargs)
        context['questions'] =\
            context['sitting'].get_questions(with_answers=True)
        context['exam_icon'] = True
        return context


class QuizTake(FormView):
    form_class = QuestionForm
    template_name = 'question.html'

    def get_object_course_class(self):
        slug = self.kwargs['quiz_name']
        obj_quiz = Quiz.objects.get(url=slug)
        obj = obj_quiz.course_class
        return obj

    def get_quiz(self):
        obj = Quiz.objects.get(course_class=self.get_object_course_class())
        return obj

    def get(self, request, *args, **kwargs):
        obj_user = request.user
        obj_course_class = self.get_object_course_class()
        obj_attempts = Attempts.objects.filter(user=obj_user, date=datetime.today(), quiz=self.get_quiz())
        if len(obj_attempts) >= 2:
            url = '/cursos/' + obj_course_class.course.slug + '/clases/' + obj_course_class.slug
            return redirect(url)
        return super(QuizTake, self).get(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.quiz = get_object_or_404(Quiz, url=self.kwargs['quiz_name'])
        self.logged_in_user = self.request.user.is_authenticated()

        if self.logged_in_user:
            self.sitting = Sitting.objects.user_sitting(request.user,
                                                        self.quiz)
        else:
            self.sitting = self.anon_load_sitting()

        if self.sitting is False:
            return render(request, 'single_complete.html')
        return super(QuizTake, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class):
        if self.logged_in_user:
            self.question = self.sitting.get_first_question()
            self.progress = self.sitting.progress()
        else:
            self.question = self.anon_next_question()
            self.progress = self.anon_sitting_progress()

        if self.question.__class__ is Essay_Question:
            form_class = EssayForm

        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(QuizTake, self).get_form_kwargs()

        return dict(kwargs, question=self.question)

    def form_valid(self, form):
        if self.logged_in_user:
            self.form_valid_user(form)
            if self.sitting.get_first_question() is False:
                self.register_award(self)
                try:
                    obj_attempts = Attempts.objects.create(
                        user=self.request.user,
                        quiz=self.quiz
                    )
                    obj_attempts.save()
                except:
                    pass
                return self.final_result_user()
        else:
            self.form_valid_anon(form)
            if not self.request.session[self.quiz.anon_q_list()]:
                self.register_award(self)
                return self.final_result_anon()

        self.request.POST = {}
        return super(QuizTake, self).get(self, self.request)

    @task
    def register_award(datos):
        CourseClassAward(datos)

    def get_context_data(self, **kwargs):
        context = super(QuizTake, self).get_context_data(**kwargs)
        course_class = CourseClass.objects.get(id=self.quiz.course_class_id)
        context['level'] = course_class.level
        context['question'] = self.question
        context['quiz'] = self.quiz
        context['exam_icon'] = True
        if hasattr(self, 'previous'):
            context['previous'] = self.previous
        if hasattr(self, 'progress'):
            context['progress'] = self.progress
        return context

    def form_valid_user(self, form):
        self.request.GET = {'exam_result': True}
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct is True:
            self.sitting.add_to_score(1)
            progress.update_score(self.question, 1, 1)
        else:
            self.sitting.add_incorrect_question(self.question)
            progress.update_score(self.question, 0, 1)

        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question
                                               .__class__.__name__: True}}
        else:
            self.previous = {}

        self.sitting.add_user_answer(self.question, guess)
        self.sitting.remove_first_question()

    def final_result_user(self):
        course_class = CourseClass.objects.get(id=self.quiz.course_class_id)
        results = {
            'quiz': self.quiz,
            'score': self.sitting.get_current_score,
            'max_score': self.sitting.get_max_score,
            'percent': self.sitting.get_percent_correct,
            'sitting': self.sitting,
            'previous': self.previous,
            'level': course_class.level,
        }
        self.sitting.mark_quiz_complete()
        if self.sitting.check_if_passed:
            from award.models import Award
            try:
                award = Award.objects.get(
                    name_reference=self.quiz.course_class.slug
                )
                results['award_image'] = award.image
                results['award_point'] = award.point
            except Exception, msg_error:
                try:
                    obj_logging = LoggingError.objects.create(
                        user=self.request.user,
                        name='quiz',
                        error=msg_error
                    )
                    obj_logging.save()
                except:
                    pass
            try:
                last_class = CourseClass.objects.filter(
                    course=self.quiz.course_class.course,
                    id__gt=self.quiz.course_class.id
                ).first()
                results['last_class'] = last_class
            except Exception, msg_error:
                try:
                    obj_logging = LoggingError.objects.create(
                        user=self.request.user,
                        name='quiz',
                        error=msg_error
                    )
                    obj_logging.save()
                except:
                    pass
            try:
                identify = self.request.user.id
                mp.track(identify, "Exam: " + str(self.sitting.quiz.course_class) + " approved" )
            except Exception, msg_error:
                try:
                    obj_logging = LoggingError.objects.create(
                        user=self.request.user,
                        name='quiz',
                        error=msg_error
                    )
                    obj_logging.save()
                except:
                    pass
        else:
            try:
                identify = self.request.user.id
                mp.track(identify, "Exam: " + str(self.sitting.quiz.course_class) + " reproved")
            except Exception, msg_error:
                try:
                    obj_logging = LoggingError.objects.create(
                        user=self.request.user,
                        name='quiz',
                        error=msg_error
                    )
                    obj_logging.save()
                except:
                    pass
        if self.quiz.answers_at_end:
            results['questions'] =\
                self.sitting.get_questions(with_answers=True)
            results['incorrect_questions'] =\
                self.sitting.get_incorrect_questions
            results['incorrect_content'] = Question.objects.filter(id__in=results['incorrect_questions'])
        if self.quiz.exam_paper is False:
            self.sitting.delete()

        return render(self.request, 'result.html', results)

    def anon_load_sitting(self):
        if self.quiz.single_attempt is True:
            return False

        if self.quiz.anon_q_list() in self.request.session:
            return self.request.session[self.quiz.anon_q_list()]
        else:
            return self.new_anon_quiz_session()

    def new_anon_quiz_session(self):
        """
        Sets the session variables when starting a quiz for the first time
        as a non signed-in user
        """
        self.request.session.set_expiry(259200)  # expires after 3 days
        questions = self.quiz.get_questions()
        question_list = [question.id for question in questions]

        if self.quiz.random_order is True:
            random.shuffle(question_list)

        if all([self.quiz.max_questions,
               self.quiz.max_questions < len(question_list)]):
            question_list = question_list[:self.quiz.max_questions]

        # session score for anon users
        self.request.session[self.quiz.anon_score_id()] = 0

        # session list of questions
        self.request.session[self.quiz.anon_q_list()] = question_list

        # session list of question order and incorrect questions
        self.request.session[self.quiz.anon_q_data()] = dict(
            incorrect_questions=[],
            order=question_list,
        )

        return self.request.session[self.quiz.anon_q_list()]

    def anon_next_question(self):
        next_question_id = self.request.session[self.quiz.anon_q_list()][0]
        return Question.objects.get_subclass(id=next_question_id)

    def anon_sitting_progress(self):
        total = len(self.request.session[self.quiz.anon_q_data()]['order'])
        answered = total - len(self.request.session[self.quiz.anon_q_list()])
        return (answered, total)

    def form_valid_anon(self, form):
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct:
            self.request.session[self.quiz.anon_score_id()] += 1
            anon_session_score(self.request.session, 1, 1)
        else:
            anon_session_score(self.request.session, 0, 1)
            self.request\
                .session[self.quiz.anon_q_data()]['incorrect_questions']\
                .append(self.question.id)

        self.previous = {}
        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question
                                               .__class__.__name__: True}}

        self.request.session[self.quiz.anon_q_list()] =\
            self.request.session[self.quiz.anon_q_list()][1:]

    def final_result_anon(self):
        score = self.request.session[self.quiz.anon_score_id()]
        q_order = self.request.session[self.quiz.anon_q_data()]['order']
        max_score = len(q_order)
        percent = int(round((float(score) / max_score) * 100))
        session, session_possible = anon_session_score(self.request.session)
        if score is 0:
            score = "0"

        results = {
            'score': score,
            'max_score': max_score,
            'percent': percent,
            'session': session,
            'possible': session_possible
        }

        del self.request.session[self.quiz.anon_q_list()]

        if self.quiz.answers_at_end:
            results['questions'] = sorted(
                self.quiz.question_set.filter(id__in=q_order)
                                      .select_subclasses(),
                key=lambda q: q_order.index(q.id))

            results['incorrect_questions'] = (
                self.request
                    .session[self.quiz.anon_q_data()]['incorrect_questions'])

        else:
            results['previous'] = self.previous

        del self.request.session[self.quiz.anon_q_data()]

        return render(self.request, 'result.html', results)


def anon_session_score(session, to_add=0, possible=0):
    """
    Returns the session score for non-signed in users.
    If number passed in then add this to the running total and
    return session score.

    examples:
        anon_session_score(1, 1) will add 1 out of a possible 1
        anon_session_score(0, 2) will add 0 out of a possible 2
        x, y = anon_session_score() will return the session score
                                    without modification

    Left this as an individual function for unit testing
    """
    if "session_score" not in session:
        session["session_score"], session["session_score_possible"] = 0, 0

    if possible > 0:
        session["session_score"] += to_add
        session["session_score_possible"] += possible

    return session["session_score"], session["session_score_possible"]
