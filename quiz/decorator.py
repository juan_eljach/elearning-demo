from functools import wraps
from classes.models import CourseClass
from courses.models import Course
from quiz.models import Quiz
from quiz.models import Sitting
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

def validate_user_can_access(view_func):
    def __response_decoractor(request, *args, **kwargs):
        reponse = view_func(request, *args, **kwargs)
        quiz = Quiz.objects.get(url=kwargs["quiz_name"]) 
        class_of_quiz = quiz.course_class
        if class_of_quiz.course.available == False:
            return redirect("courses")
        last_sitting_of_user = quiz.sitting_set.filter(user=request.user, complete=True).last()
        if last_sitting_of_user == None:
            return reponse
        if last_sitting_of_user.check_if_passed:
            return redirect(reverse("clase", kwargs={"course_slug":class_of_quiz.course.slug, "class_slug": class_of_quiz.slug}))
        else:
            return reponse
    return wraps(view_func)(__response_decoractor)
