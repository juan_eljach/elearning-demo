# -*- coding: utf-8 -*-
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.views.generic import DetailView
from mixpanel import Mixpanel
from datetime import date
from datetime import datetime
from quiz.models import Quiz, Sitting
from exploiter.decorators import user_can_access
from users.models import UserProgressMaterial
from materials.models import Material
from .models import CourseClass, Course
from attempts.models import Attempts
from django.conf import settings
mp = settings.MIXPANEL
if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
    production = False
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
    production = True
from labs_containers.models import ApiExploiterPrivateBetaUser


class ClassDetailView(DetailView):
    model = CourseClass
    model_quiz = Quiz
    model_sitting = Sitting
    template_name = "classes/class_detail.html"
    context_object_name = "courseclass"

    def get_object(self):
        slug = self.kwargs['class_slug']
        obj_course = Course.objects.get(slug=self.kwargs['course_slug'])
        obj = self.model.objects.get(slug=slug, course=obj_course)
        return obj

    @method_decorator(user_can_access)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        obj_api = ApiExploiterPrivateBetaUser.objects.filter(user=request.user,
            status=True)
        if obj_api and self.object.course.course_type == 'free':
            obj = self.get_object()
            materials = Material.objects.filter(course_class=self.get_object())
            first_material = materials.first()
            if first_material.type_material == 'video':
                try:
                    obj = obj.video_set.get(id=first_material.id_data_material)
                    type_material = '/video'
                except:
                    pass
            elif first_material.type_material == 'article':
                try:
                    obj = obj.article_set.get(id=first_material.id_data_material)
                    type_material = ''
                except:
                    pass
            url = '/cursos/{0}/clases/{1}{2}/{3}/'.format(
                self.kwargs['course_slug'],
                self.kwargs['class_slug'],
                type_material,
                obj.slug)
            return HttpResponseRedirect(url)
        try:
            all_sittings_approved = self.model_sitting.objects.filter(
                user=self.request.user.id,
                complete=True
            ).order_by("-id")
            approved = []
            validate_sitting = []
            for sitting in all_sittings_approved:
                if not sitting.quiz.id in validate_sitting:
                    if sitting.check_if_passed:
                        approved.append(sitting.quiz.id)
                    validate_sitting.append(sitting.quiz.id)
            all_quiz_of_course_of_user = self.model_quiz.objects.filter(
                id__in=approved
            )
            course = Course.objects.get(id=self.get_object().course_id)
            available_classes = self.model.objects.filter(
                course=self.get_object().course_id,
                quiz__in=all_quiz_of_course_of_user,
                start_date__lt=date.today()
            ).order_by("id")
            try:
                disabled_classes = course.courseclass_set.exclude(
                    start_date__gt=date.today()
                ).exclude(
                    id__in=available_classes
                ).order_by("id")[0]
            except:
                disabled_classes = False
#            context = self.get_context_data(object=self.object)
            if self.get_object() in available_classes:
                context = self.get_context_data(object=self.object)
            elif self.get_object() == disabled_classes:
                context = self.get_context_data(object=self.object)
            else:
                return HttpResponseRedirect(
                    "/cursos/%s" % self.kwargs["course_slug"]
                )
        except:
            list_course_class_by_course = self.model.objects.filter(
                course=self.get_object().course_id
            )
            list_quiz_of_course_class = self.model_quiz.objects.filter(
                course_class__in=list_course_class_by_course
            ).first()
            available_classes = self.model.objects.filter(
                quiz__lte=list_quiz_of_course_class.id
            )
#            context = self.get_context_data(object=self.object)
            if self.get_object() in available_classes:
                context = self.get_context_data(object=self.object)
            else:
                return HttpResponseRedirect(
                    "/cursos/%s" % self.kwargs["course_slug"]
                )
        try:
            identify = self.request.user.id
            course_class = str(self.object)
            mp.track(identify, "Class: "
                + course_class + " of the course: " + str(course) + " viewed")
        except:
            pass
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(ClassDetailView, self).get_context_data(**kwargs)
        obj = self.get_object()
        context['production'] = production
        if len(obj.quiz_set.all()) < 1:
            pass
        else:
            quiz = obj.quiz_set.all()[0]
            context["quiz"] = quiz
            if len(quiz.sitting_set.all().filter(user=self.request.user)) < 1:
                pass
            else:
                sitting = quiz.sitting_set.all().filter(
                    user=self.request.user,
                    complete=True
                ).last()
                context["sitting"] = sitting
        context["books"] = obj.book_set.all()
        context["articles"] = obj.article_set.all().order_by("id")
        context["labs"] = obj.lab_set.all()
        context["comments"] = obj.comment_set.all()
        context["videos"] = obj.video_set.all().order_by("id")
        viewed_material_articles = UserProgressMaterial.objects.filter(
            user=self.request.user,
            type_material='articles',
        )
        materials = Material.objects.filter(
            course_class=self.get_object()
        )
        context['materials'] = []
        for material in materials:
            if material.type_material == 'video':
                try:
                    context['materials'].append(
                        obj.video_set.get(id=material.id_data_material)
                    )
                except:
                    pass
            elif material.type_material == 'article':
                try:
                    context['materials'].append(
                        obj.article_set.get(id=material.id_data_material)
                    )
                except:
                    pass
        obj_viewed_material_articles = []
        for material_articles in viewed_material_articles:
            obj_viewed_material_articles.append(
                material_articles.id_data_material
            )
        viewed_material_videos = UserProgressMaterial.objects.filter(
            user=self.request.user,
            type_material='videos'
        )
        obj_viewed_material_videos = []
        for material_videos in viewed_material_videos:
            obj_viewed_material_videos.append(
                material_videos.id_data_material
            )
        context['viewed_material_videos'] = obj.video_set.filter(
            id__in=obj_viewed_material_videos
        ).order_by("id")
        context["viewed_material_articles"] = obj.article_set.filter(
            id__in=obj_viewed_material_articles
        ).order_by("id")
        context['course_icon'] = True
        obj_course = Course.objects.get(slug=self.kwargs['course_slug'])
        obj_course_class = CourseClass.objects.get(slug=self.kwargs['class_slug'], course=obj_course)
        try:
            context['alert'] = True
            obj_quiz = Quiz.objects.get(course_class=obj_course_class)
        except:
            context['alert'] = False
            obj_quiz = False
        obj_attempts = Attempts.objects.filter(
            user=self.request.user,
            date=datetime.today(),
            quiz=obj_quiz
        )
        context['attempts'] = False
        if len(obj_attempts) >= 2:
            context['attempts'] = True
        return context


#class ClassVideoDetailView(DetailView):
#        model = CourseClass
#        template_name = "classes/videoclass.html"
#        context_object_name = "video_class"

#        def get_object(self):
#                slug = self.kwargs['class_slug']
 #               obj = self.model.objects.get(slug__iexact=slug)
 #               return obj

#	def get_context_data(self, **kwargs):
#		context = super(ClassVideoDetailView, self).get_context_data(**kwargs)
#		obj = self.get_object()
#               context["comments"] = obj.comment_set.all()
#                return context

def guardar_pregunta(request):
	if request.is_ajax():

		if request.POST['pregunta']:
			pregunta = Comment(comment=request.POST['pregunta'])
			pregunta.save()

		preguntas = Comment.objects.all().order_by('-id')

		data = list()
		for pregunta in preguntas:
			data.append({ 'id': pregunta.pk, 'titulo': pregunta.titulo })

		return HttpResponse(
			json.dumps({ 'preguntas': data }),
			content_type="application/json; charset=uft8"
			)
	else:
		raise Http404

def cargar_respuestas(request, id):
	if request.is_ajax():
		respuestas = Answer.objects.filter(comment__id=id).order_by('-id')

		data = list()
		for respuesta in respuestas:
			data.append(respuesta.answer)

		return HttpResponse(
			json.dumps({'respuestas': data, 'pregunta': id}),
			content_type="application/json; charset=uft8"
			)
	else:
		raise Http404

def guardar_respuesta(request):
	if request.is_ajax():

		if request.POST['respuesta']:
			respuesta = Respuestas(titulo=request.POST['respuesta'], pregunta_id=request.POST['pregunta'])
			respuesta.save()

		return cargar_respuestas(request, request.POST['pregunta'])
