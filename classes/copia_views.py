from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.views.generic import DetailView
from .models import CourseClass, Course
from quiz.models import Quiz, Sitting
from exploiter.decorators import user_can_access
from datetime import date
from mixpanel import Mixpanel

mp = Mixpanel("92623cef2230cef5fb3c7e0748877016")


class ClassDetailView(DetailView):
    model = CourseClass
    model_quiz = Quiz
    model_sitting = Sitting
    template_name = "classes/class_detail.html"
    context_object_name = "courseclass"

    def get_object(self):
        slug = self.kwargs['class_slug']
        obj = self.model.objects.get(slug__iexact=slug)
        return obj

    @method_decorator(user_can_access)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        try:
            all_sittings_approved = self.model_sitting.objects.filter(
                user=self.request.user.id, complete=True).order_by("-id")
            approved = []
            validate_sitting = []
            for sitting in all_sittings_approved:
                if not sitting.quiz.id in validate_sitting:
                    if sitting.check_if_passed is True:
                        approved.append(sitting.quiz.id)
                    validate_sitting.append(sitting.quiz.id)
            all_quiz_of_course_of_user = self.model_quiz.objects.filter(
                id__in=approved)
            course = Course.objects.get(id=self.get_object().course_id)
            available_classes = self.model.objects.filter(
                course=self.get_object().course_id,
                quiz__in=all_quiz_of_course_of_user,
                start_date__lt=date.today()).order_by("id")
            try:
                disabled_classes = course.courseclass_set.exclude(
                    start_date__gt=date.today()
                ).exclude(
                    id__in=available_classes
                ).order_by("id")[0]
            except:
                disabled_classes = False
            if self.get_object() in available_classes or self.get_object(
                ) == disabled_classes:
                context = self.get_context_data(object=self.object)
            else:
                return HttpResponseRedirect("/cursos/{0}".format(
                    self.kwargs["course_slug"]))
        except:
            list_course_class_by_course = self.model.objects.filter(
                course=self.get_object().course_id)
            list_quiz_of_course_class = self.model_quiz.objects.filter(
                course_class__in=list_course_class_by_course).first()
            available_classes = self.model.objects.filter(
                quiz__lte=list_quiz_of_course_class.id)
            if self.get_object() in available_classes:
                context = self.get_context_data(object=self.object)
            else:
                return HttpResponseRedirect(
                    "/cursos/%s" % self.kwargs["course_slug"])
        try:
            course_class = str(self.object)
            identify = self.request.user.id
            track_doc = "Class: " + course_class + " of the course: "
            mp.track(identify,
                track_doc + str(course) + " viewed")
        except:
            pass
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(ClassDetailView, self).get_context_data(**kwargs)
        obj = self.get_object()
        if len(obj.quiz_set.all()) < 1:
            context["books"] = obj.book_set.all().only("name", "image")
            context["articles"] = obj.article_set.all().only(
                "title").order_by("id")
            context["labs"] = obj.lab_set.all()
            context["comments"] = obj.comment_set.all()
            context["videos"] = obj.video_set.all().only("title").order_by("id")
        else:
            quiz = obj.quiz_set.all()[0]
            context["quiz"] = quiz
            if len(quiz.sitting_set.all().filter(user=self.request.user)) < 1:
                pass
            else:
                sitting = quiz.sitting_set.all().filter(user=self.request.user, complete=True).last()
                context["sitting"] = sitting
            context["books"] = obj.book_set.all().only("name", "image")
            context["articles"] = obj.article_set.all().only("title").order_by("id")
            context["labs"] = obj.lab_set.all()
            context["comments"] = obj.comment_set.all()
            context["videos"] = obj.video_set.all().only("title").order_by("id")
        return context



#class ClassVideoDetailView(DetailView):
#        model = CourseClass
#        template_name = "classes/videoclass.html"
#        context_object_name = "video_class"

#        def get_object(self):
#                slug = self.kwargs['class_slug']
 #               obj = self.model.objects.get(slug__iexact=slug)
 #               return obj

#	def get_context_data(self, **kwargs):
#		context = super(ClassVideoDetailView, self).get_context_data(**kwargs)
#		obj = self.get_object()
#               context["comments"] = obj.comment_set.all()
#                return context

def guardar_pregunta(request):
	if request.is_ajax():

		if request.POST['pregunta']:
			pregunta = Comment(comment=request.POST['pregunta'])
			pregunta.save()

		preguntas = Comment.objects.all().order_by('-id')

		data = list()
		for pregunta in preguntas:
			data.append({ 'id': pregunta.pk, 'titulo': pregunta.titulo })

		return HttpResponse(
			json.dumps({ 'preguntas': data }),
			content_type="application/json; charset=uft8"
			)
	else:
		raise Http404

def cargar_respuestas(request, id):
	if request.is_ajax():
		respuestas = Answer.objects.filter(comment__id=id).order_by('-id')

		data = list()
		for respuesta in respuestas:
			data.append(respuesta.answer)

		return HttpResponse(
			json.dumps({'respuestas': data, 'pregunta': id}),
			content_type="application/json; charset=uft8"
			)
	else:
		raise Http404

def guardar_respuesta(request):
	if request.is_ajax():

		if request.POST['respuesta']:
			respuesta = Respuestas(titulo=request.POST['respuesta'], pregunta_id=request.POST['pregunta'])
			respuesta.save()

		return cargar_respuestas(request, request.POST['pregunta'])
