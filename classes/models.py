from django.contrib.auth.models import User
from django.db import models
from courses.models import Course
from django.template.defaultfilters import slugify


class CourseClass(models.Model):
    TYPES = (
        ('basic', 'Basico'),
        ('intermediate', 'Intermedio'),
        ('advanced', 'Avanzado'),
    )

    name = models.CharField(max_length=140)
    description = models.TextField()
    image = models.ImageField(upload_to="uploads")
    course = models.ForeignKey(Course)
    level = models.CharField(max_length=20, choices=TYPES, default="basic")
    slug = models.SlugField(max_length=300)
    available = models.BooleanField(default=True)
    start_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(CourseClass, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name


class Comment(models.Model):
    user = models.ForeignKey(User)
    comment = models.TextField(max_length=300)
    course_class = models.ForeignKey(CourseClass)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.comment[:80]

class Answer(models.Model):
	user = models.ForeignKey(User)
	answer = models.TextField(max_length=300)
	comment = models.ForeignKey(Comment)
	timestamp = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.comment[:80]
