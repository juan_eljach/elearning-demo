#-*- coding: utf-8 -*-
from django import template
from discussions.models import AnswerVideo

register = template.Library()

@register.tag
def get_answers(parser, token):
    return get_all_answers()

class get_all_answers(template.Node):
    def render(self, context):
        obj_comment = context['comment']
        answers_comment = AnswerVideo.objects.filter(
            comment=obj_comment
        )
        context['sum_answers'] = len(answers_comment)
        context['answers_comment'] = answers_comment
        return ''
