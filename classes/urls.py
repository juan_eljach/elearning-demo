from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from .views import ClassDetailView#, ClassVideoDetailView
from articles.views import ArticleDetailView

urlpatterns = patterns('',
    url(r'cursos/(?P<course_slug>[\w\d\-]+)/clases/(?P<class_slug>[\w\d\-]+)/$', login_required(ClassDetailView.as_view()), name='clase'),
#    url(r'cursos/(?P<course_slug>[\w\d\-]+)/clases/(?P<class_slug>[\w\d\-]+)/video/$', login_required(ClassVideoDetailView.as_view()), name="video_clase"),
    url(r'cursos/(?P<course_slug>[\w\d\-]+)/clases/(?P<class_slug>[\w\d\-]+)/(?P<article_slug>[\w\d\-]+)/$', login_required(ArticleDetailView.as_view()), name="articulo"),
    url(r'^guardar-pregunta/$', 'classes.views.guardar_pregunta', name='guardar_pregunta'),
    url(r'^cargar-respuestas/(?P<id>\d+)$', 'classes.views.cargar_respuestas', name='cargar_respuestas'),
    url(r'^guardar-respuesta/$', 'classes.views.guardar_respuesta', name='guardar_respuesta'),
)
