from django.contrib import admin
from .models import CourseClass, Comment, Answer
from labs.models import Lab
from books.models import Book
from videos.models import Video


class LabInline(admin.TabularInline):
    model = Lab
    extra = 1


class BookInline(admin.TabularInline):
    model = Book
    extra = 1


class VideoInline(admin.TabularInline):
    model = Video
    extra = 1


class CourseClassAdmin(admin.ModelAdmin):
    exclude = ("slug",)
    list_display = ("name", "course")
    inlines = [LabInline, BookInline, VideoInline]


class CommentAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "course_class",)

admin.site.register(CourseClass, CourseClassAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Answer)
