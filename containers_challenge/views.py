from .models import ContainerImageChallenge
from .models import ContainerUserChallenge
import requests
import httplib
import base64
import urllib
import requests.auth
import json
CONSUMER_KEY = '18b3ccb5f5b544ebd4265f7adca1c5cae44ef3fc661da4524f2a260699f52c25'
CONSUMER_SECRET = 'cf2b9c82831056244ac126b88905531427990321972a4067063f6b4bb51263ba'
host = 'codepicnic.com'


def generate_token():
    encoded_CONSUMER_KEY = urllib.quote(CONSUMER_KEY)
    encoded_CONSUMER_SECRET = urllib.quote(CONSUMER_SECRET)
    concat_consumer_url = encoded_CONSUMER_KEY + ":" + encoded_CONSUMER_SECRET
    url_token = '/oauth/token/'
    params = urllib.urlencode({'grant_type': 'client_credentials'})
    request = httplib.HTTPSConnection(host)
    request.putrequest("POST", url_token)
    request.putheader("Host", host)
    request.putheader("User-Agent: Exploiter.co")
    request.putheader("Authorization", "Basic %s" % base64.b64encode(concat_consumer_url))
    request.putheader("Content-Type" ,"application/x-www-form-urlencoded;charset=UTF-8")
    request.putheader("Content-Length", "29")
    request.putheader("Accept-Encoding", "gzip")
    request.endheaders()
    request.send(params)
    response = request.getresponse()
    token = eval(response.read())['access_token']
    return token


def create_container(user, obj_challenge):
    obj_container_image = ContainerImageChallenge.objects.get(challenge=obj_challenge)
    url_console = '/api/consoles/'
    headers = {"Authorization": "bearer " + generate_token()}
    data = 'bite[custom_image]=%s&bite[container_size]=%s&bite[container_type]=%s&bite[hostname]=%s'%(
        obj_container_image.custom_image,
        obj_container_image.size,
        obj_container_image.container_type,
        obj_container_image.host_name
    )
    response_request = requests.post(
        'https://'+host+url_console,
        headers=headers,
        data=data
    )
    response =  response_request.json()
    create_container_user = ContainerUserChallenge.objects.create(
        user=user,
        image=obj_container_image,
        name=response['container_name']
    )
    create_container_user.save()
    return (response['url'] + '?hide=options')


def start_container(user, obj_challenge):
    try:
        obj_container_image = ContainerImageChallenge.objects.get(challenge=obj_challenge)
        obj_container_user = ContainerUserChallenge.objects.filter(
            user=user,
            image=obj_container_image
        ).last()
        url_console = 'https://codepicnic.com/api/consoles/%s/start' % (
            obj_container_user.name
        )
        headers = {"Authorization": "bearer " + generate_token()}
        response_request = requests.post(
            url_console,
            headers=headers
        )
        response = response_request.json()
    except Exception, msg_error:
        print "Error: ", msg_error
    return (response['url'] + '?hide=options')


def stop_container(user, obj_challenge):
    obj_container_image = ContainerImageChallenge.objects.get(challenge=obj_challenge)
    obj_container_user = ContainerUserChallenge.objects.filter(
        user=user,
        image=obj_container_image
    ).last()
    url_console = 'https://codepicnic.com/api/consoles/%s/stop' % (
        obj_container_user.name
    )
    headers = {"Authorization": "bearer " + generate_token()}
    response_request = requests.post(
        url_console,
        headers=headers
    )
    response = response_request.json()
    return (response['url'] + '?hide=options')


def restart_container(user, obj_challenge):
    obj_container_image = ContainerImageChallenge.objects.get(challenge=obj_challenge)
    obj_container_user = ContainerUserChallenge.objects.last(
        user=user,
        image=obj_container_image
    ).last()
    url_console = 'https://codepicnic.com/api/consoles/%s/restart' % (
        obj_container_user.name
    )
    headers = {"Authorization": "bearer " + generate_token()}
    response_request = requests.post(
        url_console,
        headers=headers
    )
    response = response_request.json()
    return (response['url'] + '?hide=options')
