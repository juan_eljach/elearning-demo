from django.db import models
from django.contrib.auth.models import User
from challenges.models import Challenges

container_size = (
    ("medium", "256MB"),
    ("large", "512MB"),
    ("xlarge", "1GB"),
    ("xxlarge", "2GB")
)

container_type = (
    ("python", "Python"),
    ("ruby", "Ruby"),
    ("bash", "Bash"),
    ("php", "Php"),
    ("nodejs", "NodeJS"),
    ("js", "JS"),
    ("rails", "Rails")
)


class ContainerImageChallenge(models.Model):
    custom_image = models.CharField(max_length=254, blank=True, null=True)
    host_name = models.CharField(max_length=254)
    size = models.CharField(max_length=7, choices=container_size)
    container_type = models.CharField(max_length=6, choices=container_type)
    challenge = models.ForeignKey(Challenges)

    def __unicode__(self):
        return self.host_name


class ContainerUserChallenge(models.Model):
    user = models.ForeignKey(User)
    image = models.ForeignKey(ContainerImageChallenge)
    name = models.CharField(max_length=254)

    def __unicode__(self):
        return self.user.username
