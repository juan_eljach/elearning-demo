from django.contrib import admin
from .models import ContainerImageChallenge
from .models import ContainerUserChallenge

admin.site.register(ContainerImageChallenge)
admin.site.register(ContainerUserChallenge)
