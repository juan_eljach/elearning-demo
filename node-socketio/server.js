var fs = require('fs');

var options = {
  key: fs.readFileSync("/etc/nginx/ssl/certificate.key"),
  cert: fs.readFileSync("/etc/nginx/ssl/public.crt")
};


var app = require('https').createServer(options, handler),
	io = require('socket.io').listen(app),
	qs = require('querystring')

io.sockets.on('connection', function(socket) {
	console.log('client connected');

	socket.on('disconnect', function() {
		console.log('client disconnected');
	});

});

function handler(req, res) {
	var buffer = '';
	req.on('data', function(data) {
		buffer += data;
	});
	req.on('end', function() {
		var message = qs.parse(buffer);
		console.log(message.event);
		try {
			data = JSON.parse(message.data);
		} catch(e) {
			data = message.data;
		}
		io.sockets.emit(message.event, data);
		res.writeHead(200);
		res.end('ok');
	});
}

app.listen(8001, '0.0.0.0');
