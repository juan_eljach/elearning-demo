from django.contrib import admin
from .models import Teams
from .models import TeamsUser


class TeamsAdmin(admin.ModelAdmin):
    search_fields = ('name', )


admin.site.register(Teams, TeamsAdmin)
admin.site.register(TeamsUser)
