#-*- encoding: UTF-8 -*-
from django.contrib.auth.models import User
from django.utils import simplejson
from django.utils import timezone
from datetime import timedelta
from django import template
from award.models import RegisterAward
from teams.models import Teams
from teams.models import TeamsUser
import time
from award.models import RegisterAward

register = template.Library()


@register.tag
def get_day(parser, token):
    return get_points_for_day()


class get_points_for_day(template.Node):
    def get_num_day(self):
        num_day = time.strftime("%w")
        return num_day

    def get_date(self):
        num_day = int(self.get_num_day())
        date = []
        for i in range(0, num_day):
            days = int(num_day - (i) - 1)
            date.append(timezone.datetime.today().date() + timedelta(days=days))
        return date

    def render(self, context):
        return ''

@register.tag
def get_data(parser, token):
    return get_data_dict()

class data:
    def __init__(self, dia, obj_user):
        self.dias = [
            'lunes',
            'martes',
            'miercoles',
            'jueves',
            'viernes',
            'sabado',
            'domingo'
        ]
        self.dia = dia
        self.obj_user = obj_user
        self.dict_lunes = {}
        self.dict_martes = {}
        self.dict_miercoles = {}
        self.dict_jueves = {}
        self.dict_viernes = {}
        self.dict_sabado = {}
        self.dict_domingo  = {}
        self.diccionario = {}
        self.get_teams_users()

    def get_teams_users(self):
        try:
            obj_team_user = TeamsUser.objects.get(
                user=self.obj_user
            )
            list_status = ['leader', 'accepted']
            obj_teams_users = TeamsUser.objects.filter(
                teams=obj_team_user.teams,
                status__in=list_status
            )
            return obj_teams_users
        except:
            return False

    def get_num_day(self):
        num_day = time.strftime("%w")
        return num_day

    def get_date(self, dia):
        days = int(dia)
        date = (timezone.datetime.today().date() - timedelta(days=days))
        return date

    def get_point_of_user(self, obj_user):
        get_num_day = int(self.get_num_day()) - 1
        if get_num_day >= int(self.dia):
            num_dia = get_num_day - self.dia
            date = self.get_date(num_dia)
            awards = RegisterAward.objects.filter(
                user=obj_user,
                date=date
            )
            point = 0
            for award in awards:
                point += award.award.point
            sum_point = point
        else:
            sum_point = 'null'
        return sum_point

    def get_datos(self):
        teams_users = self.get_teams_users()
        lista = []
        if teams_users:
            for user in teams_users:
                dict_user = {}
                dict_user['user'] = str(user.user.username)
                dict_user['point'] = self.get_point_of_user(user.user)
                lista.append(dict_user)
        else:
            return False
        self.diccionario = {
            'dia': self.dias[self.dia],
            'datos': lista
        }
        return self.diccionario


class get_data_dict(template.Node):
    def render(self, context):
        lista_data = []
        for i in range(0, 7):
            obj = data(i, context['user'])
            lista_data.append(obj.get_datos())
        context['data'] = lista_data
        return ''
