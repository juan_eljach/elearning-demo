from django import template
from django.contrib.auth.models import User
from award.models import RegisterAward

register = template.Library()


@register.tag
def get_user_point(parser, token):
    return get_sum_point()

class get_sum_point(template.Node):
    def render(self, context):
        obj_user = User.objects.get(username=context["user"])
        awards = RegisterAward.objects.filter(user=obj_user)
        point = 0
        for award in awards:
            point += award.award.point
        context['point'] = point
        return ''
