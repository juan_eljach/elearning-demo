from django import template
from django.contrib.auth.models import User
from award.models import RegisterAward
from courses.models import Course
from additional.percentages.percentages_of_courses import percentage_by_course
register = template.Library()


@register.tag
def get_user_percentage(parser, token):
    return get_sum_percentage()

class get_sum_percentage(template.Node):
    def render(self, context):
        obj_course = context['course']
        obj_user = User.objects.get(username=context["user"])
        context['completed'] = percentage_by_course(obj_user, obj_course)
        context['missing'] = 100 - int(context['completed'])
        return ''
