#-*-coding:UTF-8 -*-
from django.http import HttpResponse
from django.views import generic
import json

from django.http.response import HttpResponseRedirect
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render

from notifications_user.models import NotificationTeam
from award.models import RegisterAward
from courses.models import Course
from .models import TeamsUser
from .models import Teams
from additional.percentages.percentages_of_courses import percentage_by_course

class TeamUserCoursePercentage(TemplateView):
    model = Teams

    def get(self, request, *args, **kwars):
        obj_team_user = TeamsUser.objects.get(
            user=request.user
        )
        list_status = ['leader', 'accepted']
        obj_teams_users = TeamsUser.objects.filter(
            teams=obj_team_user.teams,
            status__in=list_status
        )
        username = self.kwargs['username_slug']
        try:
            obj_user_consults = User.objects.get(
                username=username
            )
            obj_team_user_consults = TeamsUser.objects.get(
                user=obj_user_consults
            )
        except:
            return HttpResponse(json.dumps({
                'completed': 'Te gusta hacer trampa',
                }),
                mimetype='application/javascript')
        if obj_team_user_consults in obj_teams_users:
            all_courses = Course.objects.filter(
                available=True
            )
            data = {}
            for course in all_courses:
                completed = percentage_by_course(obj_user_consults, course)
                missing = 100 - int(completed)
                data[course.slug] = {
                    'id_course': course.id,
                    'completed': completed,
                    'missing': missing
                }
            return HttpResponse(json.dumps({
                'data': data,
                }),
                mimetype="application/json")
        else:
            return HttpResponse(json.dumps({
                'completed': 'Te gusta hacer trampa',
                }),
                mimetype='application/javascript')


class LeaveTheTeam(TemplateView):
    model = TeamsUser

    def get(self, request, *args, **kwargs):
        try:
            obj_team_user = TeamsUser.objects.get(
                user=request.user
            )
            obj_team_user.delete()
        except:
            pass
        return redirect('/cursos/')


class DeleteOfTeam(TemplateView):
    model = TeamsUser

    def get_object(self):
        username = self.kwargs['username']
        try:
            obj = User.objects.get(
                username=username
            )
            return obj
        except:
            return redirect('/team/')

    def get(self, request, *args, **kwargs):
        obj_user = request.user
        try:
            TeamsUser.objects.get(
                user=obj_user,
                status='leader'
            )
        except:
            return redirect('/team/')
        self.object = self.get_object()
        try:
            obj_team_user = TeamsUser.objects.get(
                user=self.object
            )
            obj_team_user.delete()
        except:
            pass
        return redirect('/team/')

class TeamView(TemplateView):
    model = Teams
    models_team_user = TeamsUser
    template_name = "teams/dashboard.html"

    def get_leader(self):
        try:
            self.models_team_user.objects.get(
                user=self.request.user,
                status='leader'
            )
            return True
        except:
            return False

    def get_teams(self):
        try:
            obj_team_user = self.models_team_user.objects.filter(
                user=self.request.user,
            ).first()
            return obj_team_user.teams
        except:
            pass
        return False

    def get_context_data(self, **kwargs):
        context = super(TeamView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        self.object = None
        context = self.get_context_data(object=self.object)
        try:
            obj_team_user = TeamsUser.objects.get(
                user=request.user
            )
            list_status = ['leader', 'accepted']
            context['obj_teams_users'] = TeamsUser.objects.filter(
                teams=obj_team_user.teams,
                status__in=list_status
            )
            context['obj_teams_pending'] = TeamsUser.objects.filter(
                teams=obj_team_user.teams,
                status='invited'
            )
            context['get_leader'] = self.get_leader()
            context['team_icon'] = True
            context['courses'] = Course.objects.filter(
                available=True
            )
        except:
            return redirect('/new-team/')
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        team=self.get_teams()
        nick_name = request.POST['nick_name']
        if nick_name:
            try:
                obj_user = User.objects.get(username=nick_name)
            except:
                return HttpResponseRedirect("/team/")
            try:
                self.models_team_user.objects.get(
                    user=obj_user
                )
            except:
                if obj_user != self.request.user:
                    obj_team_user = self.models_team_user.objects.create(
                        teams=team,
                        user=obj_user,
                        status='invited'
                    )
                    obj_team_user.save()
                    obj_notification = NotificationTeam.objects.create(
                        team=obj_team_user,
                        user=obj_user
                    )
                    obj_notification.save()
        return HttpResponseRedirect("/team/")

class NewteamView(TemplateView):
    model = Teams
    models_team_user = TeamsUser
    template_name = "teams/team_form.html"

    def get_team(self):
        try:
            obj_teams = self.model.objects.get(
                leader=self.request.user
            )
        except:
            obj_teams = self.model.objects.create(
                name=self.request.user.username,
                leader=self.request.user
            )
            obj_teams.save()
            obj_teams_user = self.models_team_user.objects.create(
                teams=obj_teams,
                user=self.request.user,
                status='leader',
            )
            obj_teams_user.save()
        finally:
            return obj_teams

    def get_context_data(self, **kwargs):
        context = super(NewteamView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        self.object = None
        context = self.get_context_data(object=self.object)
        context['team_icon'] = True
        try:
            obj_user = request.user
            self.models_team_user.objects.get(
                user=obj_user
            )
            return HttpResponseRedirect("/team/")
        except:
            return self.render_to_response(context)


    def post(self, request, *args, **kwargs):
        team=self.get_team()
        for i in range(1, 6):
            nick_name = request.POST['nick_' + str(i)]
            if nick_name:
                try:
                    obj_user = User.objects.get(username=nick_name)
                except:
                    continue
                try:
                    self.models_team_user.objects.get(
                        user=obj_user
                    )
                except:
                    if obj_user != self.request.user:
                        obj_team_user = self.models_team_user.objects.create(
                            teams=team,
                            user=obj_user,
                            status='invited'
                        )
                        obj_team_user.save()
                        obj_notification = NotificationTeam.objects.create(
                            team=obj_team_user,
                            user=obj_user
                        )
                        obj_notification.save()
        return HttpResponseRedirect("/team/")


class NotificationAccept(generic.View):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            obj_team_user = TeamsUser.objects.filter(
                user=request.user
            ).last()
            if obj_team_user.status == 'invited':
                try:
                    obj_notification_team = NotificationTeam.objects.filter(
                        user=request.user,
                        viewed=False
                    ).last()
                    if obj_notification_team:
                        obj_notification_team.viewed = True
                        obj_notification_team.date = obj_notification_team.date
                        obj_notification_team.save()
                        obj_team_user.status = 'accepted'
                        obj_team_user.save()
                except:
                    pass
            else:
                pass
        return HttpResponse(json.dumps({'status': 'ok'}),
            mimetype='application/javascript')


class NotificationCanceled(generic.View):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            obj_team_user = TeamsUser.objects.filter(
                user=request.user
            ).last()
            if obj_team_user.status == 'invited':
                try:
                    obj_notification_team = NotificationTeam.objects.filter(
                        user=request.user,
                        viewed=False
                    ).last()
                    if obj_notification_team:
                        obj_notification_team.viewed = True
                        obj_notification_team.date = obj_notification_team.date
                        obj_notification_team.save()
                        obj_team_user.status = 'canceled'
                        obj_team_user.save()
                except:
                    pass
            else:
                pass
        return HttpResponse(json.dumps({'status': 'ok'}),
            mimetype='application/javascript')
