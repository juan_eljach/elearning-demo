from django.contrib.auth.models import User
from django.db import models



class Teams(models.Model):
    name = models.CharField(max_length=254)
    leader = models.ForeignKey(User)
    image = models.ImageField(
        upload_to="uploads",
        default='static/img/cara.png'
    )
    date_of_creation = models.DateField(auto_now=True)

    def __unicode__(self):
        return self.name


class TeamsUser(models.Model):
    status_choices = (
        ('accepted', 'accepted'),
        ('invited', 'invited'),
        ('removed', 'removed'),
        ('canceled', 'canceled'),
        ('leader', 'leader')
    )
    teams = models.ForeignKey(Teams)
    user = models.ForeignKey(User)
    status = models.CharField(max_length=254, choices=status_choices)

    class Meta:
        ordering = ['id', ]
        
    def __unicode__(self):
        return self.user.username
