from django.conf.urls import patterns, url
from .views import TeamView
from .views import NewteamView
from .views import TeamUserCoursePercentage
from .views import LeaveTheTeam
from .views import DeleteOfTeam
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^team/$', login_required(TeamView.as_view()), name='team'),
    url(r'^new-team/$', login_required(NewteamView.as_view()), name='new-team'),
    url(r'^team/salir-del-equipo/$', login_required(LeaveTheTeam.as_view()), name='leave-team'),
    url(r'^team/eliminar-del-equipo/(?P<username>[\w\d\-]+)/$', login_required(DeleteOfTeam.as_view()), name='delete_user_of_team'),
    url(r'^team/consults/(?P<username_slug>[\w\d\-]+)/$', login_required(TeamUserCoursePercentage.as_view()), name='team_consults'),
)
