from django.conf.urls import patterns, include, url
from .views import BooksList

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'exploiter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^libros/', BooksList.as_view(), name='books'),
)
