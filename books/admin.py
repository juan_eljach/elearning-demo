from django.contrib import admin
from .models import Book


class BookAdmin(admin.ModelAdmin):
    exclude = ("slug",)

admin.site.register(Book, BookAdmin)
