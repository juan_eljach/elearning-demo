from django.contrib import admin
from .models import Video, Concept
from materials.models import Material
from materials.notifications import register_of_notification

class VideoAdmin(admin.ModelAdmin):
    exclude=('slug',)
    search_fields = ('title', )
    class Media:
        js = ('/static/tiny_mce/tiny_mce.js',
              '/static/tiny_mce/textarea.js',)

    def save_model(self, request, form, formset, change):
        instances = formset.save(commit=False)
        instances.save()
        try:
            Material.objects.get(
                course_class=instances.course_class,
                id_data_material=instances.id,
                type_material='video'
            )
        except:
            register_material = Material.objects.create(
                course_class=instances.course_class,
                id_data_material=instances.id,
                type_material='video'
            )
            register_material.save()
            obj_material = Material.objects.get(id=register_material.id)
            register_of_notification(
                obj_material,
                obj_material.course_class,
                obj_material.id_data_material,
                obj_material.type_material
            )

class ConceptAdmin(admin.ModelAdmin):
	class Media():
	    js = ('/static/tiny_mce/tiny_mce.js',
              '/static/tiny_mce/textarea.js',)

admin.site.register(Video, VideoAdmin)
admin.site.register(Concept, ConceptAdmin)
