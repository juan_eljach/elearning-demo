from django.db import models
from classes.models import CourseClass
from django.template.defaultfilters import slugify
from materials.models import Material


class Video(models.Model):
    title = models.CharField(max_length=150)
    url = models.URLField(blank=True)
    iframe = models.TextField()
    course_class = models.ForeignKey(CourseClass)
    slug = models.SlugField(max_length=300)
    glosary = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Video, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Video, self).save(*args, **kwargs)
        try:
            material = Material.objects.get(
                course_class=self.course_class,
                id_data_material=self.pk,
                type_material='video'
            )
            material.delete()
        except:
            pass

    def __unicode__(self):
        return self.title


class Concept(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()
    video = models.ForeignKey(Video)

    def __unicode__(self):
        return self.title
