from django.conf.urls import patterns, include, url
from .views import VideoDetailView

urlpatterns = patterns('',
    url(r'cursos/(?P<course_slug>[\w\d\-]+)/clases/(?P<class_slug>[\w\d\-]+)/video/(?P<video_slug>[\w\d\-]+)/$', VideoDetailView.as_view(), name="video"),
)

