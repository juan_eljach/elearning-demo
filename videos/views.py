from django.views.generic import ListView, DetailView
from django.views.generic import View
from django.utils.decorators import method_decorator

from users.previous_next_classes import url_previous_classes
from users.previous_next_classes import if_first_classes
from users.previous_next_classes import url_next_classes
from users.previous_next_classes import if_last_classes
from exploiter.decorators import user_can_access
from users.models import UserProgressMaterial
from classes.models import CourseClass
from courses.models import Course
from .models import Video
from django.shortcuts import redirect
#container
from labs_containers.models import ContainerImage
from labs_containers.models import ContainerUser
from labs_containers.views import create_container
from labs_containers.views import start_container
from labs_containers.models import ApiExploiterPrivateBetaUser
#from labs_containers.views import stop_container
#from labs_containers.views import restart_container
from labs_containers.models import ApiExploiterUser
from additional.can_access_material.access_material import can_access_material
from loggings.models import LoggingError
from django.http import HttpResponse
from discussions.models import CommentVideo
from discussions.models import AnswerVideo
import json
from django.utils.safestring import mark_safe
from notifications_user.models import NotificationComment
from django.conf import settings
if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
    production = False
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
    production = True


class VideoListView(ListView):
    model = Video
    template_name = "classes/videos.html"
    context_object_name = "videos"


class VideoDetailView(DetailView):
    model = Video
    template_name = "classes/videoclass.html"
    context_object_name = "video"

    def get_object_course_class(self):
        obj_course = Course.objects.get(slug=self.kwargs['course_slug'])
        obj_course_class = CourseClass.objects.get(
            course=obj_course,
            slug=self.kwargs['class_slug']
        )
        return obj_course_class

    def get_object(self):
        slug = self.kwargs['video_slug']
        obj = self.model.objects.get(
            slug__iexact=slug,
            course_class=self.get_object_course_class()
        )
        return obj

    def get_comments_video(self):
        obj_comment = CommentVideo.objects.filter(
            video=self.get_object()
        ).order_by("-id")[:5]
        return obj_comment

    def get_object_course(self):
        slug = self.kwargs['course_slug']
        obj = Course.objects.get(slug__iexact=slug)
        return obj

    def get_context_data(self, **kwargs):
        try:
            UserProgressMaterial.objects.get(
                user=self.request.user,
                id_data_material=self.get_object().id,
                type_material='videos'
            )
        except:
            UserProgressMaterial.objects.create(
                user=self.request.user,
                id_data_material=self.get_object().id,
                type_material='videos'
            )
        try:
            obj_container_image = ContainerImage.objects.get(
                course=self.get_object_course()
            )
            try:
                ContainerUser.objects.get(
                    user=self.request.user,
                    image=obj_container_image
                )
                iframe_container = start_container(
                    self.request.user,
                    self.get_object_course())
            except:
                iframe_container = create_container(
                    self.request.user,
                    self.get_object_course())
        except Exception, msg_error:
            try:
                obj_logging = LoggingError.objects.create(
                    user=self.request.user,
                    name='container',
                    error=msg_error
                )
                obj_logging.save()
            except:
                pass
            iframe_container = False
        context = super(VideoDetailView, self).get_context_data(**kwargs)
        context['production'] = production
        self.object = self.get_object()
        context["concepts"] = self.object.concept_set.all()
        context['url_next_class'] = url_next_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'video',
            self.request.user
        )
        context['url_previous_class'] = url_previous_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'video'
        )
        context['if_first_classes'] = if_first_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'video'
        )
        context['if_last_classes'] = if_last_classes(
            self.get_object_course_class(),
            self.get_object().id,
            'video',
            self.request.user,
        )
        context['comment_video'] = self.get_comments_video()
        context['iframe_container'] = iframe_container
        context['course_icon'] = True
        obj_api = ApiExploiterPrivateBetaUser.objects.filter(
            user=self.request.user,
            status=True)
        if obj_api:
            self.object_course = self.get_object_course_class().course
            if self.object_course.course_type == 'free':
                self.template_name = 'classes/videonuevo.html'
                try:
                    api_user = ApiExploiterUser.objects.get(
                        user=self.request.user,
                        status=True)
                    context['iframe_container_exploiter'] = "http://{0}:8080".format(
                        api_user.ip_publica
                    )
                except:
                    pass
        return context

    @method_decorator(user_can_access)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        can_access = can_access_material(
            self.request.user, self.object.course_class)
        if can_access:
            return redirect("course", course_slug=kwargs["course_slug"])
        return self.render_to_response(context)


class RegisterDiscussions(View):

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            comment = request.POST['comment']
            video_slug = request.POST['video_slug']
            obj_video = Video.objects.get(
                slug=video_slug
            )
            obj_comment = CommentVideo.objects.create(
                user_comment=request.user,
                comment=comment,
                video=obj_video
            )
            obj_comment.save()
        return HttpResponse(json.dumps({
                'status': 'ok',
                'username': request.user.username,
                'image_user': request.user.get_profile().get_mugshot_url(),
                'get_short_name': mark_safe(request.user.get_short_name()),
                'comment': mark_safe(obj_comment.comment.format())
            }),
            mimetype='application/json')


class RegisterAnswerOfComment(View):

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            answer = request.POST['answer']
            id_comment = request.POST['id_comment']
            obj_comment = CommentVideo.objects.get(
                id=id_comment
            )
            obj_answer = AnswerVideo.objects.create(
                user_answer=request.user,
                answer=answer,
                comment=obj_comment
            )
            obj_answer.save()
            try:
                obj_notification = NotificationComment.objects.create(
                    answer=obj_answer
                )
                obj_notification.save()
            except Exception, msg_error:
                print "Error: ", msg_error
        return HttpResponse(json.dumps({
                'status': 'ok',
                'username': request.user.username,
                'image_user': request.user.get_profile().get_mugshot_url(),
                'get_short_name': mark_safe(request.user.get_short_name()),
                'comment': mark_safe(obj_answer.answer)
            }),
            mimetype='application/json')
