#-*- coding: UTF-8 -*-
from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from django.contrib.auth import authenticate, login, logout
from .forms import UserCreation
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse
from userena.decorators import secure_required
from userena.forms import SignupFormOnlyEmail
from users.forms import EditProfileForm
from .forms import SignupForm
from userena import settings as userena_settings
from emails.utils import SendEmail
from userena.models import UserenaSignup
from userena import signals as userena_signals
from django.contrib.auth.models import User
from django.contrib import messages
from django.utils.translation import ugettext as _
from accounts.models import UserProfile
from functools import wraps
from guardian.shortcuts import assign_perm
from django.conf import settings
mp = settings.MIXPANEL


class UserCreationView(FormView):
    template_name = "users/register.html"
    form_class = UserCreation
    success_url = "/cursos/"

    def get(self, request):
        if request.user.is_authenticated():
            return redirect("/cursos/")
        else:
            form = self.form_class
            return render(request, self.template_name, {"form": form})

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(self.request, user)
        return super(UserCreationView, self).form_valid(form)


def my_decorator(view_func):
    def __decorator(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        if request.method == "POST":
            request.POST["email"]
        return response
    return wraps(view_func)(__decorator)


class ExtraContextTemplateView(TemplateView):
    """ Add extra context to a simple template view """
    extra_context = None

    def get_context_data(self, *args, **kwargs):
        context = super(
            ExtraContextTemplateView, self).get_context_data(*args, **kwargs)
        if self.extra_context:
            context.update(self.extra_context)
        return context

    post = TemplateView.get


@secure_required
def signup(request, signup_form=SignupForm,
           template_name='userena/signup_form.html', success_url=None,
           extra_context=None):
    """
    Signup of an account.

    Signup requiring a username, email and password. After signup a user gets
    an email with an activation link used to activate their account. After
    successful signup redirects to ``success_url``.

    :param signup_form:
        Form that will be used to sign a user. Defaults to userena's
        :class:`SignupForm`.

    :param template_name:
        String containing the template name that will be used to display the
        signup form. Defaults to ``userena/signup_form.html``.

    :param success_url:
        String containing the URI which should be redirected to after a
        successful signup. If not supplied will redirect to
        ``userena_signup_complete`` view.

    :param extra_context:
        Dictionary containing variables which are added to the template
        context. Defaults to a dictionary with a ``form`` key containing the
        ``signup_form``.

    **Context**

    ``form``
        Form supplied by ``signup_form``.

    """
    # If signup is disabled, return 403
    if userena_settings.USERENA_DISABLE_SIGNUP:
        raise PermissionDenied

    # If no usernames are wanted and the default form is used, fallback to the
    # default form that doesn't display to enter the username.
    if userena_settings.USERENA_WITHOUT_USERNAMES and (signup_form == SignupForm):
        signup_form = SignupFormOnlyEmail
    logout(request)
    form = signup_form()
    if request.method == 'POST':
        form = signup_form(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.first_name = request.POST["first_name"]
            user.last_name = request.POST["last_name"]
            user.save()
            userena = UserenaSignup.objects.get(user_id=user)
            to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                request.POST["email"],
                request.POST["first_name"],
                'e@exploiter.co',
                'Cursos de Exploiter.co',
                request.POST["first_name"] + ", Bienvenido a Exploiter.co. Activa tu cuenta",
                '8c18b6f8-f070-4973-acdd-c2b8288b9e9f',
                'http://exploiter.co/accounts/activate/%s/' % userena.activation_key,
                )
            SendEmail(to_email, to_name, from_email, from_name, subject, template_name, html_text)
            l = request.POST["email"]# Send the signup complete signal
            userena_signals.signup_complete.send(sender=None,
                                                 user=user)


            if success_url: redirect_to = success_url
            else: redirect_to = reverse('userena_signup_complete',
                                        kwargs={'username': user.username})

            # A new signed user should logout the old one.
            if request.user.is_authenticated():
                logout(request)

            if (userena_settings.USERENA_SIGNIN_AFTER_SIGNUP and
                not userena_settings.USERENA_ACTIVATION_REQUIRED):
                user = authenticate(identification=user.email, check_password=False)
                login(request, user)

            return redirect(redirect_to)

    if not extra_context: extra_context = dict()
    extra_context['form'] = form
    return ExtraContextTemplateView.as_view(template_name=template_name,
                                            extra_context=extra_context)(request)


@secure_required
def activate(request, activation_key,
             template_name='userena/activate_fail.html',
             retry_template_name='userena/activate_retry.html',
             success_url=None, extra_context=None):
    """
    Activate a user with an activation key.

    The key is a SHA1 string. When the SHA1 is found with an
    :class:`UserenaSignup`, the :class:`User` of that account will be
    activated.  After a successful activation the view will redirect to
    ``success_url``.  If the SHA1 is not found, the user will be shown the
    ``template_name`` template displaying a fail message.
    If the SHA1 is found but expired, ``retry_template_name`` is used instead,
    so the user can proceed to :func:`activate_retry` to get a new actvation key.

    :param activation_key:
        String of a SHA1 string of 40 characters long. A SHA1 is always 160bit
        long, with 4 bits per character this makes it --160/4-- 40 characters
        long.

    :param template_name:
        String containing the template name that is used when the
        ``activation_key`` is invalid and the activation fails. Defaults to
        ``userena/activate_fail.html``.

    :param retry_template_name:
        String containing the template name that is used when the
        ``activation_key`` is expired. Defaults to
        ``userena/activate_retry.html``.

    :param success_url:
        String containing the URL where the user should be redirected to after
        a successful activation. Will replace ``%(username)s`` with string
        formatting if supplied. If ``success_url`` is left empty, will direct
        to ``userena_profile_detail`` view.

    :param extra_context:
        Dictionary containing variables which could be added to the template
        context. Default to an empty dictionary.

    """
    try:
        if (not UserenaSignup.objects.check_expired_activation(activation_key)
            or not userena_settings.USERENA_ACTIVATION_RETRY):
            user = UserenaSignup.objects.activate_user(activation_key)
            if user:
                # Sign the user in.
                auth_user = authenticate(identification=user.email,
                                         check_password=False)
                login(request, auth_user)

                if userena_settings.USERENA_USE_MESSAGES:
                    messages.success(request, _('Your account has been activated and you have been signed in.'),
                                     fail_silently=True)
                try:
                    identify = user.id
                    mp.people_set(identify, {
                        '$first_name'    : user.first_name,
                        '$last_name'     : user.last_name,
                        '$email'         : user.email,
                        'type': 'free'
                    })
                    mp.track(identify, "Account activated")
                except:
                    pass
                if success_url:
                    redirect_to = success_url % {'username': user.username }
                else:
                    redirect_to = reverse('trial_poll')
                return redirect(redirect_to)
            else:
                if not extra_context: extra_context = dict()
                return ExtraContextTemplateView.as_view(template_name=template_name,
                                                        extra_context=extra_context)(
                                        request)
        else:
            if not extra_context: extra_context = dict()
            extra_context['activation_key'] = activation_key
            return ExtraContextTemplateView.as_view(template_name=retry_template_name,
                                                extra_context=extra_context)(request)
    except UserenaSignup.DoesNotExist:
        if not extra_context: extra_context = dict()
        return ExtraContextTemplateView.as_view(template_name=template_name,
                                                extra_context=extra_context)(request)


def save_profile(backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        try:
            profile = user.get_profile()
        except:
            profile = UserProfile.objects.create(
                user_id=user.id,
                privacy='registered',
                paid_user=False
            )
            profile.save()

def associate_by_email(**kwargs):
    try:
        email = kwargs['details']['email']
        kwargs['user'] = User.objects.get(email=email)
    except:
        pass
    return kwargs

USER_FIELDS = ['username', 'email']
ASSIGNED_PERMISSIONS = {
    'profile':
        (('view_profile', 'Can view profile'),
         ('change_profile', 'Can change profile'),
         ('delete_profile', 'Can delete profile')),
    'user':
        (('change_user', 'Can change user'),
         ('delete_user', 'Can delete user'))
}


def create_user(strategy, details, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name) or details.get(name))
                        for name in strategy.setting('USER_FIELDS',
                                                      USER_FIELDS))
    if not fields:
        return
    user = strategy.create_user(**fields)
    obj_user = User.objects.get(username=user)
    userena_signup = UserenaSignup.objects.create(
        user_id=obj_user.id,
    )
    userena_signup.save()
    profile = UserProfile.objects.create(
        user=obj_user,
        privacy='open',
    )
    profile.save()
    new_profile = obj_user.get_profile()
    for perm in ASSIGNED_PERMISSIONS['profile']:
        assign_perm(perm[0], obj_user, new_profile)

    # Give permissions to view and change itself
    for perm in ASSIGNED_PERMISSIONS['user']:
        assign_perm(perm[0], obj_user, obj_user)
    try:
        obj_user = User.objects.get(username=user)
        identify = user.id
        mp.people_set(identify, {
            '$first_name'    : kwargs['response']['first_name'],
            '$last_name'     : kwargs['response']['last_name'],
            '$email'         : obj_user.email,
            'type': 'free'
        })
        mp.track(identify, "Account activated")
    except:
        pass
    return {
        'is_new': True,
        'user': user
    }


from guardian.decorators import permission_required_or_403
from userena.utils import get_profile_model, get_user_model
from userena.decorators import secure_required
from django.shortcuts import get_object_or_404

@secure_required
@permission_required_or_403('change_profile', (get_profile_model(), 'user__username', 'username'))
def profile_edit(request, username, edit_profile_form=EditProfileForm,
                 template_name='userena/profile_form.html', success_url=None,
                 extra_context=None, **kwargs):
    """
    Edit profile.

    Edits a profile selected by the supplied username. First checks
    permissions if the user is allowed to edit this profile, if denied will
    show a 404. When the profile is successfully edited will redirect to
    ``success_url``.

    :param username:
        Username of the user which profile should be edited.

    :param edit_profile_form:

        Form that is used to edit the profile. The :func:`EditProfileForm.save`
        method of this form will be called when the form
        :func:`EditProfileForm.is_valid`.  Defaults to :class:`EditProfileForm`
        from userena.

    :param template_name:
        String of the template that is used to render this view. Defaults to
        ``userena/edit_profile_form.html``.

    :param success_url:
        Named URL which will be passed on to a django ``reverse`` function after
        the form is successfully saved. Defaults to the ``userena_detail`` url.

    :param extra_context:
        Dictionary containing variables that are passed on to the
        ``template_name`` template.  ``form`` key will always be the form used
        to edit the profile, and the ``profile`` key is always the edited
        profile.

    **Context**

    ``form``
        Form that is used to alter the profile.

    ``profile``
        Instance of the ``Profile`` that is edited.

    """
    user = get_object_or_404(get_user_model(),
                             username__iexact=username)

    profile = user.get_profile()
    paid_user = profile.paid_user
    type_of_payment = profile.type_of_payment
    user_initial = {'first_name': user.first_name,
                    'last_name': user.last_name,
                    'paid_user': user.profile.paid_user}
    form = edit_profile_form(instance=profile, initial=user_initial)

    if request.method == 'POST':
        form = edit_profile_form(request.POST, request.FILES, instance=profile,
                                 initial=user_initial)

        if form.is_valid():
            profile = form.save()
            profile.paid_user = paid_user
            profile.type_of_payment = type_of_payment
            profile.save()

            if userena_settings.USERENA_USE_MESSAGES:
                messages.success(request, _('Your profile has been updated.'),
                                 fail_silently=True)

            if success_url:
                # Send a signal that the profile has changed
                userena_signals.profile_change.send(sender=None,
                                                    user=user)
                redirect_to = success_url
            else: redirect_to = reverse('userena_profile_detail', kwargs={'username': username})
            return redirect(redirect_to)

    if not extra_context: extra_context = dict()
    extra_context['form'] = form
    extra_context['profile'] = profile
    return ExtraContextTemplateView.as_view(template_name=template_name,
                                            extra_context=extra_context)(request)
