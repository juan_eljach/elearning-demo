# -*- coding: utf-8 -*-
from classes.models import CourseClass
from courses.models import Course
from articles.models import Article
from materials.models import Material
from videos.models import Video
from challenges.models import VideoChallenge
from labs_containers.models import ApiExploiterPrivateBetaUser

def url_next_classes(slug_class, id_material, type_material, obj_user,
    lab=False):
    obj_course_class = slug_class
    obj_next_class = Material.objects.get(
        course_class=obj_course_class,
        id_data_material=id_material,
        type_material=type_material
    )
    obj_api = ApiExploiterPrivateBetaUser.objects.filter(user=obj_user,
        status=True)
    if type_material == 'video' and not lab and obj_api:
        try:
            next_class = Material.objects.get(
                course_class=obj_course_class,
                id_data_material=id_material,
                type_material='lab'
            )
        except:
            next_class = Material.objects.filter(
                course_class=obj_course_class,
                pk__gt=obj_next_class.pk
            ).order_by('id').first()
    else:
        next_class = Material.objects.filter(
            course_class=obj_course_class,
            pk__gt=obj_next_class.pk
        ).order_by('id').exclude(type_material='lab').first()
    if next_class:
        if next_class.type_material == 'video':
            try:
                data_material = Video.objects.get(
                    id=next_class.id_data_material
                )
            except:
                return False
            if next_class.course_class == obj_course_class:
                url_next_class = '/cursos/%s/clases/%s/video/' % (
                    next_class.course_class.course.slug,
                    next_class.course_class.slug,
                )
                url_next_class += data_material.slug
        elif next_class.type_material == 'lab':
            if next_class.course_class == obj_course_class:
                url_next_class = '/cursos/%s/clases/%s/lab/' % (
                    next_class.course_class.course.slug,
                    next_class.course_class.slug,
                )
                obj_video_challenge = VideoChallenge.objects.get(
                    video=next_class.id_data_material
                )
                url_next_class += obj_video_challenge.challenge.slug
        elif next_class.type_material == 'article':
            try:
                data_material = Article.objects.get(
                    id=next_class.id_data_material
                )
            except:
                return False
            if next_class.course_class == obj_course_class:
                url_next_class = '/cursos/%s/clases/%s/' % (
                    next_class.course_class.course.slug,
                    next_class.course_class.slug,
                )
                url_next_class += data_material.slug
        return url_next_class
    else:
        return False


def url_previous_classes(slug_class, id_material, type_material):
    obj_course_class = slug_class
    obj_previous_class = Material.objects.get(
        course_class=obj_course_class,
        id_data_material=id_material,
        type_material=type_material
    )
    previous_class = Material.objects.filter(
        course_class=obj_course_class,
        pk__lt=obj_previous_class.pk
    ).order_by('id').last()
    if previous_class:
        if previous_class.type_material == 'video':
            try:
                data_material = Video.objects.get(
                    id=previous_class.id_data_material
                )
            except:
                return False
            if previous_class.course_class == obj_course_class:
                url_previous_class = '/cursos/%s/clases/%s/video/' % (
                    previous_class.course_class.course.slug,
                    previous_class.course_class.slug,
                )
                url_previous_class += data_material.slug
        elif previous_class.type_material == 'article':
            try:
                data_material = Article.objects.get(
                    id=previous_class.id_data_material
                )
            except:
                return False
            if previous_class.course_class == obj_course_class:
                url_previous_class = '/cursos/%s/clases/%s/' % (
                    previous_class.course_class.course.slug,
                    previous_class.course_class.slug,
                )
                url_previous_class += data_material.slug
        return url_previous_class
    else:
        return False


def if_first_classes(slug_class, id_material, type_material):
    obj_course_class = slug_class
    obj_material = Material.objects.get(
        course_class=obj_course_class,
        id_data_material=id_material,
        type_material=type_material
    )
    obj_of_first_material = Material.objects.filter(
        course_class=obj_course_class
    ).first()
    if obj_of_first_material == obj_material:
        return 1
    else:
        return False


def if_last_classes(slug_class, id_material, type_material, obj_user):
    obj_course_class = slug_class
    obj_material = Material.objects.get(
        course_class=obj_course_class,
        id_data_material=id_material,
        type_material=type_material
    )
    obj_api = ApiExploiterPrivateBetaUser.objects.filter(user=obj_user,
        status=True)
    if type_material == 'video' and obj_api:
        try:
            obj_of_last_material = Material.objects.filter(
                course_class=obj_course_class,
                type_material='lab'
            ).last()
        except:
            obj_of_last_material = Material.objects.filter(
                course_class=obj_course_class,
            ).last()
    else:
        obj_of_last_material = Material.objects.filter(
            course_class=obj_course_class
        ).exclude(type_material='lab').last()
    if obj_of_last_material == obj_material:
        obj_course = Course.objects.get(id=obj_material.course_class.course.id)
        obj_course_class = CourseClass.objects.filter(
            course=obj_course,
            id__gt=obj_material.course_class.id
        ).first()
        next_class = Material.objects.filter(
            course_class=obj_course_class,
        ).order_by('id').first()
        if next_class:
            if next_class.type_material == 'video':
                try:
                    data_material = Video.objects.get(
                        id=next_class.id_data_material
                    )
                except:
                    return False
                if next_class.course_class == obj_course_class:
                    url_next_class = '/cursos/%s/clases/%s/video/' % (
                        next_class.course_class.course.slug,
                        next_class.course_class.slug,
                    )
                    url_next_class += data_material.slug
            elif next_class.type_material == 'article':
                try:
                    data_material = Article.objects.get(
                        id=next_class.id_data_material
                    )
                except:
                    return False
                if next_class.course_class == obj_course_class:
                    url_next_class = '/cursos/%s/clases/%s/' % (
                        next_class.course_class.course.slug,
                        next_class.course_class.slug,
                    )
                    url_next_class += data_material.slug
            return url_next_class
        return 1
    else:
        return False
