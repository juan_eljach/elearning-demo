from userena import views as views_userena
from threading import Thread
import threading
from functools import wraps
from mailin import Mailin
from celery import task
from emails.utils import SendEmail
from userena.models import UserenaSignup
from django.contrib.auth.models import User


@task
def proceso_mensajes(email, username):
    m = Mailin("https://api.sendinblue.com/v1.0","Q2W0OH7RkjYzrwA1","UNQt7vLyDmhZW8Aa")
    attributes = {"name":username}
    blacklisted = 0
    listid = ("4",)
    listid_unlink = list()
    blacklisted_sms = 0
    m.create_update_user(email, attributes, blacklisted, listid, listid_unlink, blacklisted_sms)
    return

def signup_decorator(view_func):
    def __response_decoractor(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        if request.method == "POST":
            #user = User.objects.get(username=request.POST["username"])
            #userena = UserenaSignup.objects.get(user_id=user)
            proceso_mensajes(request.POST["email"], request.POST["username"])
            ##Enviar Mensajes
            to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                request.POST["email"],
                request.POST["username"],
                'e@exploiter.co',
                'Cursos de Exploiter.co',
                request.POST["username"] + ", Bienvenido a Exploiter.co. Activa tu cuenta",
                '8c18b6f8-f070-4973-acdd-c2b8288b9e9f',
                'http://exploiter.co/accounts/activate/1/',
                )
            SendEmail(to_email, to_name, from_email, from_name, subject, template_name, html_text)
        return response
    return wraps(view_func)(__response_decoractor)
