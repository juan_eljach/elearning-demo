from django import template
from django.contrib.auth.models import User
from award.models import RegisterAward, Award
from courses.models import Course

register = template.Library()


@register.tag
def get_user_award(parser, token):
    return user_award()


def get_sum_point(user):
    awards = RegisterAward.objects.filter(user=user)
    point = 0
    for award in awards:
        point += award.award.point
    return point


class user_award(template.Node):
    def render(self, context):
        u = User.objects.get(username=context["profile"])
        context['courses'] = Course.objects.filter(available=True)
        context['points'] = get_sum_point(u)
        return ''


@register.tag
def get_award_of_user(parse, token):
    return award_of_user()

class award_of_user(template.Node):
    def render(self, context):
        try:
            obj_user = User.objects.get(username=context['profile'])
            obj_award = Award.objects.get(name_reference=context['class'].slug)
            award_of_user = RegisterAward.objects.filter(
                user=obj_user,
                award=obj_award.id
            ).last()
            if award_of_user:
                date = award_of_user.date
            else:
                date = False
            context['award_name'] = obj_award.name_show
            context['award_image'] = obj_award.image
            context['award_date'] = date
        except:
            context['award_name'] = False
            context['award_image'] = False
            context['award_date'] = False
        return ''

@register.tag
def get_award_special_of_user(parse, token):
    return award_special_of_user()

class award_special_of_user(template.Node):
    def render(self, context):
        try:
            obj_user = User.objects.get(username=context['profile'])
            obj_awards = Award.objects.filter(name_reference__startswith='challenges-')
            list_id_award = []
            for award in obj_awards:
                list_id_award.append(award.id)
            award_of_user = RegisterAward.objects.filter(
                user=obj_user,
                award__in=list_id_award
            )
            context['awards_specials_of_user'] = award_of_user
        except:
            pass
        return ''
