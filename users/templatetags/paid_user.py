from django import template
from django.contrib.auth.models import User
from payments.models import Payment
from payments.models import PaymentOther
from payments.models import PaymentOxxo
from datetime import date

register = template.Library()

@register.tag
def get_status_paid(parser, token):
    return status_paid()


class status_paid(template.Node):

    def get_date_end_of_subscription(self, user):
        try:
            obj_payment = Payment.objects.get(user=user)
            date_end = obj_payment.date_end_of_subscription
        except:
            try:
                obj_payment = PaymentOther.objects.get(user=user)
                date_end = obj_payment.date_end_of_subscription
            except:
                try:
                    obj_payment = PaymentOxxo.objects.get(user=user)
                    date_end = obj_payment.date_end_of_subscription
                except:
                    date_end = False
        return date_end

    def render(self, context):
        obj_user = User.objects.get(username=context['profile'])
        context['status_paid'] = obj_user.get_profile().paid_user
        if context['status_paid']:
            user_date = self.get_date_end_of_subscription(obj_user)
            if user_date:
                if date.today() > user_date:
                    context['end_of_subscription'] = False
                else:
                    context['end_of_subscription'] = user_date
        return ''
