from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from functools import wraps
from django.shortcuts import redirect


def signin_decorator(view_func):
    def __response_decoractor(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        if request.method == "POST":
            if '@' in request.POST['identification']:
                try:
                    obj_user = User.objects.get(
                        email=request.POST['identification']
                    )
                    if obj_user.check_password(request.POST['password']):
                        new_user = authenticate(
                            username=obj_user,
                            password=request.POST['password']
                        )
                        login(request, new_user)
                        return redirect("/cursos/")
                except:
                    pass
            else:
                try:
                    obj_user = User.objects.get(
                        username=request.POST['identification']
                    )
                    if obj_user.check_password(request.POST["password"]):
                        new_user = authenticate(
                            username=request.POST['identification'],
                            password=request.POST['password']
                        )
                        login(request, new_user)
                        return redirect("/cursos/")
                except:
                    pass
        return response
    return wraps(view_func)(__response_decoractor)
