from django.db import models
from django.contrib.auth.models import User
from courses.models import Course

TYPE_OF_PAYMENT = (
    ('credit_card', 'credit_card'),
    ('paypal', 'paypal'),
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="profile")
    image = models.ImageField(upload_to="uploads")
    subscription = models.BooleanField(default=False)
    type_of_payment = models.CharField(max_length=20, choices=TYPE_OF_PAYMENT)
    date_of_subscription = models.DateField()
    date_of_last_payment = models.DateField()


class UserInitiationCourse(models.Model):
    user = models.ForeignKey(User)
    obj_course = models.ForeignKey(Course)
    date_of_viewed = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.user.username


class UserProgressMaterial(models.Model):
    user = models.ForeignKey(User)
    id_data_material = models.IntegerField()
    type_material = models.CharField(max_length=255)
    date_of_consumption = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.user.username

class IPUser(models.Model):
    user = models.ForeignKey(User)
    ip = models.IPAddressField()
    city = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return '%s' % self.user.username
