from django.conf.urls import patterns, url
from userena.views import signin
from .views import signup
from .decorator_login import signin_decorator

urlpatterns = patterns('',
    url(r'^login/$', signin_decorator(signin), {
        "template_name": "users/login.html", }, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login',
        name="logout"),
    url(r'^signup/$', signup, {"template_name": "users/register.html", },
        name="signup"),
)
