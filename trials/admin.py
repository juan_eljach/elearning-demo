from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.auth.models import User
from django.contrib import admin
from django import forms
from .models import TrialOfCourses
from .models import Answer
from .models import Question
from .models import AnswerQuestion
from .models import Poll
import csv
from datetime import timedelta
from datetime import datetime
from .forms import AnswerModelForm


class AnswerQuestionAdmin(admin.ModelAdmin):
    form = AnswerModelForm
    list_display = ('question', 'answer')
    exclude = ('answer', )

    def save_model(self, request, form, formset, change):
        for answer in formset.cleaned_data['answer_form']:
            obj = AnswerQuestion.objects.create(
                question=formset.cleaned_data['question'],
                answer=answer
            )
            obj.save()


class TrialOfCoursesAdmin(admin.ModelAdmin):
    list_display = ('user', 'date_initiation', 'date_end')
    search_fields = ('user__username', )


class PollAdmin(admin.ModelAdmin):
    list_filter = ('dedicate_to_the_hacking', 'level_of_study')
    list_display = ('user', 'dedicate_to_the_hacking', 'level_of_study', 'courses')


admin.site.register(TrialOfCourses, TrialOfCoursesAdmin)
admin.site.register(Answer)
admin.site.register(Question)
admin.site.register(AnswerQuestion, AnswerQuestionAdmin)
admin.site.register(Poll, PollAdmin)
