from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import View
from django.contrib.auth.models import User
from django.shortcuts import render
from django.shortcuts import redirect
from userena.forms import SignupForm
from .models import Poll
from .models import TrialOfCourses
from accounts.models import UserProfile

#Celery
from accounts.tasks import features_of_plataform


class SigninTrial(TemplateView):
    template_name = 'trials/signin.html'

    def get(self, request, *args, **kwargs):
        obj_user = request.user
        if obj_user.is_authenticated():
            return redirect('/trial/login')
        else:
            return super(SigninTrial, self).get(request, *args, **kwargs)

class TrialPollView(CreateView):
    model = Poll
    form_signup = SignupForm
    template_name = 'trials/free-trial.html'
    template_signup = 'trials/free-trial-registro.html'

    def get_context_data(self, **kwargs):
        context = super(TrialPollView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        obj_user = request.user
        if obj_user.is_authenticated():
            try:
                obj_poll = self.model.objects.get(user=request.user)
                return redirect('/cursos/')
            except:
                obj_user_profile = UserProfile.objects.get(user=request.user)
                if not obj_user_profile.paid_user:
                    return super(TrialPollView, self).get(request, *args, **kwargs)
                else:
                    return redirect('/cursos/')
        else:
            return render(request, self.template_signup)

    def post(self, request, *args, **kwargs):
        obj_user = request.user
        if obj_user.is_authenticated():
            try:
                obj_poll = self.model.objects.get(user=request.user)
                return redirect('/cursos/')
            except:
                obj_user_profile = UserProfile.objects.get(user=request.user)
                if not obj_user_profile.paid_user:
                    obj_poll = self.model.objects.create(
                        user=request.user,
                        dedicate_to_the_hacking=request.POST['dedicate_to_the_hacking'],
                        level_of_study=request.POST['level_of_study'],
                        courses=request.POST['courses']
                    )
                    obj_poll.save()
                    obj_trial = TrialOfCourses.objects.create(
                        user=request.user
                    )
                    obj_trial.save()
                    features_of_plataform.apply_async(args=[obj_user.username], countdown=86400)
                    return redirect('/trial/gracias/')
                else:
                    return redirect('/cursos/')
        else:
            username = request.POST['username']
            password = request.POST['password1']
            if '@' in username:
                try:
                    obj = User.objects.get(email=username)
                    if obj.check_password(password):
                        user = authenticate(
                            username=obj,
                            password=password
                        )
                        login(request, user)
                except:
                    pass
            else:
                try:
                    obj = User.objects.get(username=username)
                    if obj.check_password(password):
                        user = authenticate(
                            username=username,
                            password=password
                        )
                        login(request, user)
                except:
                    pass
        return redirect('/trial/login/')

class TrialThanks(TemplateView):
    template_name = 'trials/free-trial-thanks.html'

    @method_decorator(login_required(login_url='/trial/login/'))
    def dispatch(self, *args, **kwargs):
        return super(TrialThanks, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        try:
            obj_poll = Poll.objects.get(user=request.user)
            return super(TrialThanks, self).get(request, *args, **kwargs)
        except:
            return redirect('/trial/login/')
