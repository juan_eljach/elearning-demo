#-*- coding:utf-8 -*-
from django import forms
from .models import AnswerQuestion
from .models import Answer

class AnswerModelForm(forms.ModelForm):
    answer_form = forms.ModelMultipleChoiceField(
        queryset=Answer.objects.all(),
        required=False
    )

    class Meta:
        model = AnswerQuestion
        fields = ["question", "answer"]

    def __init__(self, *args, **kwargs):
        super(AnswerModelForm, self).__init__(*args, **kwargs)
