#-*- coding: UTF-8 -*-
from django.contrib.auth.models import User
from django.db import models
from datetime import datetime
from datetime import timedelta

class TrialOfCourses(models.Model):
    user = models.ForeignKey(User, unique=True)
    date_initiation = models.DateField(
        default=datetime.today().now()
    )
    date_end = models.DateField(
        default=datetime.today().date() + timedelta(days=7)
    )

    def __unicode__(self):
        return self.user.username


class Answer(models.Model):
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return '%s' % self.title


class Question(models.Model):
    name = models.CharField(max_length=250)

    def __unicode__(self):
        return '%s' % self.name


class AnswerQuestion(models.Model):
    question = models.ForeignKey(Question)
    answer = models.ForeignKey(Answer)

    def __unicode__(self):
        return '%s' % self.question.name


class PollOther(models.Model):
    user = models.ForeignKey(User, unique=True)
    answer_question = models.ForeignKey(AnswerQuestion)

    def __unicode__(self):
        return '%s' % self.user.username

class Poll(models.Model):
    yes_no = (
        ('si', 'Si'),
        ('no', 'No')
    )

    level_study = (
        ('Bachillerato', 'Bachillerato'),
        ('Tecnología/Tecnólogo', 'Tecnología/Tecnólogo'),
        ('Universidad', 'Universidad'),
        ('Especialización', 'Especialización'),
        ('Maestría', 'Maestría'),
        ('Doctorado', 'Doctorado')
    )

    user = models.ForeignKey(User)
    dedicate_to_the_hacking = models.CharField(max_length=30, choices=yes_no)
    level_of_study = models.CharField(max_length=240, choices=level_study)
    courses = models.TextField()

    def __unicode__(self):
        return self.user.username
