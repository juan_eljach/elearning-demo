#-*- coding:utf-8 -*-
from django.conf.urls import url, include
from .views import TrialPollView
from .views import TrialThanks
from .views import SigninTrial

urlpatterns = [
    url(r'^trial/login/$', TrialPollView.as_view(), name='trial_poll'),
    url(r'^trial/$', SigninTrial.as_view(), name='trial_sign-in'),
    url(r'^trial/gracias/$', TrialThanks.as_view(), name='trial_thanks'),
]
