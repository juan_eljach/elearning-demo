from django.contrib import admin
from .models import UserProfile
from users.models import IPUser


class UserProfileAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )


class IPUserAdmin(admin.ModelAdmin):
    search_fields = ('user__username', )
    list_display = ('user', 'ip', 'city', 'country')

admin.site.unregister(UserProfile)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(IPUser, IPUserAdmin)
