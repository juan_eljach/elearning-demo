from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from userena.models import UserenaBaseProfile

TYPE_OF_PAYMENT = (
    ('credit_card', 'credit_card'),
    ('paypal', 'paypal'),
)


class UserProfile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='my_profile')
    paid_user = models.BooleanField(default=False)
    type_of_payment = models.CharField(
        max_length=20, choices=TYPE_OF_PAYMENT, blank=True)

    def __unicode__(self):
        return self.user.get_username()
