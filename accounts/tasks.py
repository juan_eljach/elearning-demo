#-*- coding: UTF-8 -*-
from celery import shared_task
from django.contrib.auth.models import User
from award.models import RegisterAward, Award
from emails.utils import SendEmail
from accounts.models import UserProfile
from payments.models import UserTracking
from django.utils.crypto import get_random_string
import stripe
from discounts.models import DiscountPercentage
from discounts.models import DiscountUser
from django.conf import settings
from courses.models import Course
from classes.models import CourseClass
from loggings.models import LoggingError
if settings.SETTINGS_MODULE == 'exploiter.settings.local':
    from exploiter.settings.local import STRIPE_PUBLISHABLE
    from exploiter.settings.local import GOOGLE_ANALYTICS
else:
    from exploiter.settings.production import STRIPE_PUBLISHABLE
    from exploiter.settings.production import GOOGLE_ANALYTICS
from emails.models import Email
from trials.models import TrialOfCourses
from discounts.models import DiscountCodeURL
import time


class InvitationOfDiscount():
    def __init__(self, email, short_name=None):
        self.email = email
        self.short_name = short_name

    def send_email(self):
        try:
            Email.objects.get(
                to_email=self.email,
                template_name='cf726b02-99cc-463e-8b3a-14dac649d1b6')
        except:
            try:
                try:
                    titulo = "{0} 20% de descuento en todos los cursos por nuestro cumpleaños".format(str(self.short_name))
                except:
                    titulo = "20% de descuento en todos los cursos por nuestro cumpleaños"
                    self.shohort_name = ""
                to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                    self.email,
                    False,
                    'juan@exploiter.co',
                    'Juan Eljach',
                    titulo,
                    'cf726b02-99cc-463e-8b3a-14dac649d1b6',
                    self.short_name
                )
                SendEmail(to_email, to_name, from_email, from_name, subject,
                    template_name, html_text)
            except:
                pass

        return to_email


@shared_task
def invitation_of_discount():
    objs_users = User.objects.filter(is_active=True)
    #objs_users = User.objects.filter(email="danilovargas.01@gmail.com")
    DiscountCodeURL.objects.get_or_create(
        code="lc77JmygeI6GKtKHsFPVBSaehfbRKiry0sf49j5x3MHr54HXPpJR2ExmsiQdCzAX5mqCcF",
        discount="exploiterhb"
    )
    for obj_user in objs_users:
        try:
            obj_profile = UserProfile.objects.get(user=obj_user)
            if not obj_profile.paid_user:
                obj_invitation = InvitationOfDiscount(
                    obj_user.email, obj_user.username)
                obj_invitation.send_email()
                time.sleep(5)
        except:
            pass
    return ''


def prueba():
    invitation_of_discount.apply_async(countdown=10)


class InvitationFreeTrial():
    def __init__(self, email, short_name):
        self.email = email
        self.short_name = short_name

    def send_email(self):
        try:
            Email.objects.get(
                to_email=self.email,
                template_name='e9476549-fe48-4d6e-83b7-f37655966d3c')
        except:
            to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                self.email,
                False,
                'juan@exploiter.co',
                'Juan Eljach',
                'Todos los cursos de Exploiter gratis, para ti',
                'e9476549-fe48-4d6e-83b7-f37655966d3c',
                self.short_name
            )
            SendEmail(to_email, to_name, from_email, from_name, subject,
                template_name, html_text)
        return to_email


@shared_task
def invitation_of_free_trial():
    objs_users = User.objects.filter(is_active=True)
    for obj_user in objs_users:
        try:
            obj_profile = UserProfile.objects.get(user=obj_user)
            obj_trial = TrialOfCourses.objects.filter(user=obj_user)
            if not obj_profile.paid_user and not obj_trial:
                obj_invitation = InvitationFreeTrial(obj_user.email,
                    obj_user.get_short_name())
                obj_invitation.send_email()
        except:
            pass
    return ''


class feedback:
    def __init__(self, username):
        self.username = username

    def get_obj_user(self):
        try:
            obj_user = User.objects.get(username=str(self.username))
        except:
            obj_user = False
        return obj_user

    def text_body(self):
        body = """
            Hola, ¿Cómo estás?<br>
            <p align="justify">
            En Exploiter nos preocupamos todos por darte la mejor experiencia en educación online, por eso queremos construirla junto a ti Soy Juan el CTO de Exploiter y me encantaría recibir tu opinión sobre los cursos y la plataforma y que me dijeras que crees que podríamos cambiar para que te sintieras mejor en tu proceso educativo.<br>
            </p>
            Así puedo empezar a trabajar junto el equipo en ello de forma inmediata.<br><br>
            ¿Te gustaría que agregáramos una nueva funcionalidad?<br><br>
            ¿Hay algo que no te guste y que crees que deberíamos cambiar?<br><br>
            Agradezco de antemano tu opinión, será muy valiosa para construir un producto cada día mejor para ti.<br><br><br>
            Saludos,<br><br>
        """
        return body

    def send_email(self):
        obj_user = self.get_obj_user()
        obj_user_profile = UserProfile.objects.get(
            user=obj_user
        )
        if obj_user and obj_user_profile.paid_user:
            try:
                Email.objects.get(to_email=obj_user.email,
                    template_name='c4188857-a8a6-45cb-aa66-8450b51609e0')
            except:
                try:
                    to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                        obj_user.email,
                        False,
                        'juan@exploiter.co',
                        'Juan Eljach',
                        "Me encantaría saber tu opinión",
                        'c4188857-a8a6-45cb-aa66-8450b51609e0',
                        self.text_body()
                    )
                    SendEmail(to_email, to_name, from_email, from_name,
                        subject, template_name, html_text)
                except Exception as msg_error:
                    obj_logging = LoggingError.objects.create(
                        user=obj_user,
                        name='email',
                        error=msg_error
                    )
                    obj_logging.save()
        return ''


@shared_task
def question_by_feedback(username):
    obj_id = feedback(username)
    obj_id.send_email()
    return ''


class discout_30_days_for_user:

    def __init__(self, username, code=False, point=False):
        self.username = username
        self.code = code
        self.point = point

    def set_cupon(self):
        obj_discount_percentage = DiscountPercentage.objects.filter(
            apply_discount='discount_mail_tracking'
        ).first()
        if obj_discount_percentage:
            self.code = get_random_string(20)
            stripe.Coupon.create(
                percent_off=obj_discount_percentage.percentage,
                duration='once',
                id=self.code
            )
        return obj_discount_percentage

    def get_cupon(self):
        return self.code

    def get_obj_user(self):
        try:
            obj_user = User.objects.get(username=str(self.username))
        except:
            obj_user = False
        return obj_user

    def create_discount(self):
        cupon = self.set_cupon()
        if cupon:
            type_of_user = self.get_obj_user()
            if type_of_user:
                obj_discount = DiscountUser.objects.create(
                    code=self.get_cupon(),
                    user=type_of_user
                )
                obj_discount.save()
                return obj_discount
            else:
                return False
        else:
            return False

    def get_awards_of_user(self):
        obj_user = User.objects.get(username=self.username)
        obj_awards = RegisterAward.objects.filter(user=obj_user)
        for award in obj_awards:
            self.point += award.award.point
        return obj_awards

    def send_email(self):
        obj_user = self.get_obj_user()
        obj_user_tracking = UserTracking.objects.filter(
            user=obj_user,
            used=True
        ).first()
        obj_user_profile = UserProfile.objects.get(
            user=obj_user
        )
        self.get_awards_of_user()
        if obj_user_tracking and not obj_user_profile.paid_user and self.point >= 50:
            if obj_user:
                try:
                    Email.objects.get(to_email=obj_user.email,
                        template_name='4c8f005f-1af2-4c68-b9a8-6530c009b75e')
                except:
                    try:
                        get_discount = self.create_discount()
                        if get_discount:
                            to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                                obj_user.email,
                                False,
                                'juan@exploiter.co',
                                'Juan Eljach',
                                "%s, una oferta exclusiva para ti" % str(
                                    obj_user.get_short_name()),
                                '4c8f005f-1af2-4c68-b9a8-6530c009b75e',
                                str(self.get_cupon())
                            )
                            SendEmail(to_email, to_name, from_email,
                                from_name, subject, template_name, html_text)
                        question_by_feedback.apply_async(
                            args=[obj_user.username], countdown=1555200)
                    except Exception as msg_error:
                        obj_logging = LoggingError.objects.create(
                            user=obj_user,
                            name='email',
                            error=msg_error
                        )
                        obj_logging.save()
        return ''


@shared_task
def discount_30_days(username):
    obj = discout_30_days_for_user(username)
    obj.send_email()
    return ''


class buy_of_courses_of_users:

    def __init__(self, username):
        self.username = username

    def text_body(self):
        get_courses = self.get_courses()
        tracking = 'https://exploiter.co/tracking/' + str(
            self.create_token().token)
        body = """
            <table border="1" cellpadding="1" cellspacing="1" style="width: 100%; border: none;">
			<tbody style="margin: 0 0 25px;" >
            {0}
			</tbody>
			</table>
			<div style="width: 60%;margin: 20px auto 0; text-align: center;-webkit-border-radius: 5; -moz-border-radius: 5; border-radius: 5px; font-family: Arial; color: #ffffff; font-size: 16px; background: #0f76ac; padding: 10px 2% 10px 2%; text-decoration: none;"><a href="{1}" style="font-family: Arial; color: #ffffff; font-size: 16px;text-decoration: none;"><b>SUSCR&Iacute;BETE POR SOLO $25 </b><small>USD</small> </a></div>
        """.format(str(get_courses), str(tracking))
        return body

    def get_courses(self):
        text_of_courses = ''
        objs_courses = Course.objects.filter(
            available=True,
            course_type='paid'
        )
        for course in objs_courses:
            image = "https://exploiter.co/media/" + str(course.image)
            url_of_course = "https://exploiter.co/cursos/" + str(course.slug)
            name = course.name.encode('utf-8')
            name = name.replace("á", "&aacute;")
            name = name.replace("é", "&eacute;")
            name = name.replace("í", "&iacute;")
            name = name.replace("ó", "&oacute;")
            name = name.replace("ú", "&uacute;")
            name = name.replace("ñ", "&ntilde;")
            text = """
<tr style="margin: 0 0 25px;">
									<td style="height:auto;border-radius: 4px;background: #fff;border:none;display:inline-block;float: left;margin-left: 2%;margin-top: 25px;margin-right: 0.5%;">
									<div style="text-align: left;display: flex;"><span class="sg-image" style="float: none; display: block; text-align: center;"><img height="120" src="{0}" style="border: 0px solid transparent;" width="120" /></span>
									<div style="margin-left: 35px;" width="300px">
									<p style="text-align: left;"><span style="color:#666666;"><span style="font-size:16px;"><strong>{1}</strong></span></span></p>
									<div style="width:auto;display: inline-block;font-weight:bold;margin:0px auto;text-align:center;border-radius:5px;font-family:Arial;color:#fff !important;font-size:16px;background:#0f76ac;padding:10px 25px;text-decoration:none"><a class="link" href="{2}" style="color: #fff;text-decoration: none;">Ver curso</a></div>
									</div>
									</div>
									</td>
								</tr>
            """.format(str(image), str(name), str(url_of_course))
            text_of_courses = text_of_courses + str(text)
        return text_of_courses

    def get_obj_user(self):
        try:
            obj_user = User.objects.get(username=str(self.username))
        except:
            obj_user = False
        return obj_user

    def create_token(self):
        obj_user = self.get_obj_user()
        obj_tracking = False
        while True:
            try:
                obj_tracking = UserTracking.objects.create(
                    user=obj_user,
                    token=get_random_string(35),
                )
                obj_tracking.save()
                break
            except:
                pass
        return obj_tracking

    def send_email(self):
        obj_user = self.get_obj_user()
        user_profile = UserProfile.objects.get(user=obj_user)
        if obj_user and not user_profile.paid_user:
            obj_user_tracking = UserTracking.objects.filter(
                user=obj_user
            ).first()
            if not obj_user_tracking:
                try:
                    Email.objects.get(to_email=obj_user.email,
                        template_name='e60e443c-5b24-423a-814d-3d78d07a8439')
                except:
                    try:
                        to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                            obj_user.email,
                            False,
                            'juan@exploiter.co',
                            'Juan Eljach',
                            "%s Inicia tu carrera profesional en Hacking ético hoy" % str(obj_user.get_short_name()),
                            'e60e443c-5b24-423a-814d-3d78d07a8439',
                            self.text_body()
                        )
                        SendEmail(to_email, to_name, from_email, from_name,
                            subject, template_name, html_text)
                        discount_30_days.apply_async(
                            args=[obj_user.username], countdown=1036800)
                    except Exception as msg_error:
                        obj_logging = LoggingError.objects.create(
                            user=obj_user,
                            name='email',
                            error=msg_error
                        )
                        obj_logging.save()
        return ''


@shared_task
def buy_of_courses(username):
    obj_id = buy_of_courses_of_users(username)
    obj_id.send_email()
    return ''


class value_add_for_user:
    def __init__(self, username):
        self.username = username

    def get_obj_user(self):
        try:
            obj_user = User.objects.get(username=str(self.username))
        except:
            obj_user = False
        return obj_user

    def send_email(self):
        obj_user = self.get_obj_user()
        if obj_user:
            try:
                Email.objects.get(to_email=obj_user.email, template_name='96356cfc-23cb-4c78-a49e-30e9dc8dd549')
            except:
                try:
                    to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                        obj_user.email,
                        False,
                        'juan@exploiter.co',
                        'Juan Eljach',
                        "Queremos que seas un Hacker ético profesional, por eso creamos esto",
                        '96356cfc-23cb-4c78-a49e-30e9dc8dd549',
                        obj_user.get_short_name()
                    )
                    SendEmail(to_email, to_name, from_email, from_name,
                        subject, template_name, html_text)
                    buy_of_courses.apply_async(
                        args=[obj_user.username], countdown=691200)
                except Exception as msg_error:
                    obj_logging = LoggingError.objects.create(
                        user=obj_user,
                        name='email',
                        error=msg_error
                    )
                    obj_logging.save()
        return ''


@shared_task
def value_add(username):
    obj_id = value_add_for_user(username)
    obj_id.send_email()
    return ''


class advanced_of_user:

    def __init__(self, username, point=0, obj_awards=0, slug_course=0):
        self.username = username
        self.point = point
        self.obj_awards = obj_awards
        self.slug_course = slug_course

    def text_body(self, short_name, points, design_of_awards, username):
        body = """
            <p><span style="font-size:18px;">¡Felicitaciones {0}!</span></p>
			<p style="text-align: start;"><span style="font-size: 16px; line-height: 25.6000003814697px;">Estás haciendo un excelente avance en el curso y estamos seguros que puedes mantener ese nivel.<br />
			Hasta hoy has ganado {1} puntos y  estas medallas</span></p>
			<table border="1" cellpadding="1" cellspacing="1" style="border-collapse:collapse;width: 100%; border: none;">
			<tbody><tr colspan="1" >
            {2}
			</tr>
			</tbody>
			</table>
			<hr style="border: 1px solid #f9f9f9;border-left: none;border-top: none;margin-bottom: 15px;" />
			<p><span style="font-size: 16px; line-height: 25.6000003814697px;">Recuerda que la practica hace al maestro y que cuando termines el curso tendrás un regalo especial de nuestra parte :)</span></p>
			<p>&nbsp;</p>
			<div style="margin: 0 auto;width: 50%; border-radius: 8px; font-family: Arial; color: rgb(255, 255, 255); font-size: 18px; font-weight: bold; padding: 12px 35px; text-decoration: none; text-align: center; background: rgb(0, 140, 186);"><a href="https://exploiter.co/cursos/{3}/" style="-webkit-border-radius: 8;-moz-border-radius: 8;border-radius: 8px;font-family: Arial;color: #ffffff;font-size: 18px;background: #008cba;font-weight: bold;text-decoration: none;">Continuar con el curso</a></div>
        """.format(str(short_name), int(points),
            str(design_of_awards), str(self.slug_course))
        return body

    def get_course(self):
        user = User.objects.get(username=self.username)
        awards = RegisterAward.objects.filter(user=user)
        courses = {}
        clases = []
        for award in awards:
            clases.append(award.award.name_reference)
        for slug in clases:
            obj_class = CourseClass.objects.get(slug=slug)
            try:
                courses[str(obj_class.course.slug)] += 1
            except:
                courses[str(obj_class.course.slug)] = 0
        obj_course = False
        if courses:
            list_courses = courses.items()
            list_courses.sort(key=lambda x: x[1])
            obj_course = Course.objects.get(
                slug=list_courses[len(list_courses) - 1][0])
            self.slug_course = obj_course.slug
        return obj_course

    def get_course_class(self, course):
        objs_course_class = CourseClass.objects.filter(course=course)
        list_slug_course_class = []
        for course_class in objs_course_class:
            list_slug_course_class.append(str(course_class.slug))
        return list_slug_course_class

    def get_awards(self, list_course_class):
        objs_awards = Award.objects.filter(name_reference__in=list_course_class)
        return objs_awards

    def get_awards_of_user(self, list_awards):
        obj_user = User.objects.get(username=self.username)
        self.obj_awards = RegisterAward.objects.filter(
            user=obj_user, award=list_awards)
        for award in self.obj_awards:
            self.point += award.award.point
        return self.obj_awards

    def get_design_of_awards(self, list_awards):
        text_of_awards = ''
        for award in list_awards:
            image = "https://exploiter.co/media/" + str(award['image'])
            name = award['name'].encode('utf-8')
            name = name.replace("á", "&aacute;")
            name = name.replace("é", "&eacute;")
            name = name.replace("í", "&iacute;")
            name = name.replace("ó", "&oacute;")
            name = name.replace("ú", "&uacute;")
            name = name.replace("ñ", "&ntilde;")
            text = """
                <td style="word-wrap:break-word;height:235px;border-radius: 4px;background: #fff;width:160px;border:none;display:inline-block;float: left;margin-left: 2%;margin-top: 25px;margin-right: 0.5%;"><span contenteditable="false" style="font-size: 0px; float: none; display: block; text-align: center;" tabindex="-1"><span class="sg-image" data-widget="sgimage" style="float: none; display: block; text-align: center;"><img height="120" src="{0}" style="border: 0px solid transparent;" width="120" /></span></span>
            	<p style="text-align: center;padding:0 5px;">{1}</p>
            	</td>
            """.format(image, name)
            text_of_awards = text_of_awards + text
        return text_of_awards

    def get_obj_user(self):
        try:
            obj_user = User.objects.get(username=str(self.username))
        except:
            obj_user = False
        return obj_user

    def send_email(self):
        obj_user = self.get_obj_user()
        user_profile = UserProfile.objects.get(user=obj_user)
        if obj_user and not user_profile.paid_user:
            short_name = obj_user.get_short_name()
            if not short_name:
                short_name = obj_user.username
            list_awards = []
            awards = self.obj_awards
            for award in awards:
                dict_awards = {
                    'name': award.award.name_show,
                    'image': award.award.image
                }
                list_awards.append(dict_awards)
            if list_awards:
                try:
                    Email.objects.get(
                        to_email=obj_user.email,
                        template_name='6d0705b3-99ea-4ca0-b314-b659f0ea1d22')
                except:
                    try:
                        to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                            obj_user.email,
                            False,
                            'juan@exploiter.co',
                            'Juan Eljach',
                            "Vas por muy buen camino %s" % str(short_name),
                            '6d0705b3-99ea-4ca0-b314-b659f0ea1d22',
                            self.text_body(
                                short_name,
                                self.point,
                                self.get_design_of_awards(list_awards),
                                obj_user.username
                            )
                        )
                        SendEmail(
                            to_email,
                            to_name,
                            from_email,
                            from_name, subject, template_name, html_text)
                        value_add.apply_async(
                            args=[obj_user.username], countdown=432000)
                    except Exception as msg_error:
                        obj_logging = LoggingError.objects.create(
                            user=obj_user,
                            name='email',
                            error=msg_error
                        )
                        obj_logging.save()
            return ''
        else:
            return obj_user


@shared_task
def advanced_of_5_days(username):
    obj_id = advanced_of_user(username)
    obj_course = obj_id.get_course()
    if obj_course:
        list_course_class = obj_id.get_course_class(obj_course)
        list_get_awards = obj_id.get_awards(list_course_class)
        obj_id.get_awards_of_user(list_get_awards)
        obj_id.send_email()
    return username


class features_of_user:
    def __init__(self, username):
        self.username = username

    def get_obj_user(self):
        try:
            obj_user = User.objects.get(username=str(self.username))
        except:
            obj_user = False
        return obj_user

    def send_email(self):
        obj_user = self.get_obj_user()
        if obj_user:
            try:
                Email.objects.get(
                    to_email=obj_user.email,
                    template_name='1fe4d3ae-dd0e-417e-bb37-f606bccfca77')
            except:
                try:
                    to_email, to_name, from_email, from_name, subject, template_name, html_text = (
                        obj_user.email,
                        False,
                        'juan@exploiter.co',
                        'Juan Eljach',
                        "¡Que bueno tenerte con nosotros!",
                        '1fe4d3ae-dd0e-417e-bb37-f606bccfca77',
                        obj_user.get_short_name()
                    )
                    SendEmail(
                        to_email,
                        to_name,
                        from_email,
                        from_name,
                        subject, template_name, html_text)
                    advanced_of_5_days.apply_async(
                        args=[obj_user.username], countdown=259200)
                except Exception as msg_error:
                    obj_logging = LoggingError.objects.create(
                        user=obj_user,
                        name='email',
                        error=msg_error
                    )
                    obj_logging.save()
        return obj_user


@shared_task
def features_of_plataform(username):
    obj_user = features_of_user(username)
    e = obj_user.send_email()
    return e
