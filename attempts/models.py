from django.contrib.auth.models import User
from quiz.models import Quiz
from django.db import models


class Attempts(models.Model):
    user = models.ForeignKey(User)
    quiz = models.ForeignKey(Quiz)
    date = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return '{0}'.format(self.user.username)
