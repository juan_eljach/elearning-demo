from django.contrib import admin
from .models import Attempts


class AttemptsAdmin(admin.ModelAdmin):
    list_display = ('user', 'quiz', 'date')

admin.site.register(Attempts, AttemptsAdmin)
