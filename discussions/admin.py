from django.contrib import admin
from .models import CommentVideo
from .models import AnswerVideo

class CommentVideoAdmin(admin.ModelAdmin):
    list_display = ('user_comment', 'comment', 'video')

class AnswerVideoAdmin(admin.ModelAdmin):
    list_display = ('user_answer', 'answer', 'comment')

admin.site.register(CommentVideo, CommentVideoAdmin)
admin.site.register(AnswerVideo, AnswerVideoAdmin)
