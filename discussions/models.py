from django.contrib.auth.models import User
from django.db import models

from videos.models import Video

class CommentVideo(models.Model):
    user_comment = models.ForeignKey(User, related_name='comment_video')
    comment = models.TextField(max_length=300)
    video = models.ForeignKey(Video)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-id' ,)

    def __unicode__(self):
        return self.video.title

class AnswerVideo(models.Model):
	user_answer = models.ForeignKey(User)
	answer = models.TextField(max_length=300)
	comment = models.ForeignKey(CommentVideo)
	timestamp = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.answer[:80]
