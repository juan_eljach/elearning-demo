from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from videos.views import RegisterDiscussions
from videos.views import RegisterAnswerOfComment

urlpatterns = patterns('',
    url(r'^registrar-pregunta/$', RegisterDiscussions.as_view(), name='register_discussion'),
    url(r'^registrar-respuesta/$', RegisterAnswerOfComment.as_view(), name='register_answer'),
)
